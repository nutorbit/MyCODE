#include<cstdio>

using namespace std;

int D[1000001]={};
int order[1000001]={};
int stock[1000001]={};

int main(){
    
    int N,M;
    scanf("%d %d",&N,&M);
    for (int i=1; i<=N; i++) {
        scanf("%d",&D[i]);
        stock[i] = D[i];
    }
    for (int i=1; i<=M; i++) {
        scanf("%d",&order[i]);
    }
    int c = 0;
    for (int i=1; i<=M; i++) {
        if(stock[order[i]] <= i){
            c++;
            //printf("%d before %d after %d\n",order[i],stock[order[i]].d,stock[order[i]].d+D[order[i]]);
            stock[order[i]] = i+D[order[i]];
        }
    }
    printf("%d\n",c);
    /*
    for (int i=0; i<M; i++) {
        printf("%d ",stock[i]);
    }
     */
}