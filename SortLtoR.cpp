#include<bits/stdc++.h>

using namespace std;

int main(){
	int N, Q; cin >> N >> Q;
	vector<int> A(N);
	for(int i=0; i<N; i++)
		cin >> A[i];
	for(int i=0; i<Q; i++){
		int from, to; cin >> from >> to;
		priority_queue<int, vector<int>, greater<int> > ans;
		for(int k=from; k<=to; k++)
			ans.push(A[k]);
		while(!ans.empty()){
			cout << ans.top() << " ";
			ans.pop();
		}
	}
}