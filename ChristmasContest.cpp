#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

int N,M;
ll A1[65]={},A0[65]={},B1[65]={},B0[65]={};
ll in,ans = 0;

int main(){
    scanf("%d",&N);
    FOR(i,0,N){
        scanf("%lld",&in);
        FOR(j,0,63){
            if(in & (1LL << j))A1[j]++;
            else A0[j]++;
        }
    }
    scanf("%d",&M);
    FOR(i,0,M){
        scanf("%lld",&in);
        FOR(j,0,63){
            if(in & (1LL << j))B1[j]++;
            else B0[j]++;
        }
    }
    FOR(i,0,63){
        ans += (1LL << i) * (A0[i]*B1[i] + A1[i]*B0[i]);
    }
    printf("%lld\n",ans);
}
