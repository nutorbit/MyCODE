#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll ans;

ll i,j,k,count1[500005][2],count2[500005][2],n,f,arr[1000005],pos,qsum1,qsum2,maxx;
bool c = false;

int main(){
    
    scanf("%lld %lld",&n,&f);
    FOR(i,0,n){
        scanf("%lld",&arr[i]);
        if(!c){
            if(arr[i]==f){
                pos = i;
                c=true;
                for(j=pos-1;j>=0;j--){
                    if(arr[j]>f)qsum1++;
                    else qsum1--;
                    if(qsum1>maxx)maxx = qsum1;
                    if (qsum1>=0)count1[qsum1][0]++;
                    else count1[qsum1*(-1)][1]++;
                }
            }
        }
        else{
            if(arr[i]>f)qsum2++;
            else qsum2--;
            if(qsum2>maxx)maxx = qsum2;
            if(qsum2>=0)count2[qsum2][0]++;
            else count2[qsum2*(-1)][1]++;
        }
    }
    FOR(i,1,maxx+1){
        ans+=(count1[i][1]*count2[i][0]);
        ans+=(count2[i][1]*count1[i][0]);
    }
    ans = ans+count1[0][0]+count2[0][0]+1+(ll)(count1[0][0]*count2[0][0]);
    printf("%lld\n",ans);
    return 0;
    
}
