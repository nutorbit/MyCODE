'''primeNum'''
def findfactor(limit):
    '''find prime factor'''
    number = [[] for _ in range(limit*5)]
    for i in range(2, limit+1):
        if len(number[i]) == 0:
            for j in range(i, limit*5, i):
                number[j] += [i]
    return number

def main():
    '''main'''
    first, second, limit = [int(v) for v in input().split()]
    num = findfactor(limit)
    setn = set([first, second])
    for i in range(limit, -1, -1):
        if sorted(list(setn)) == num[i] and len(num[i]) == len(setn):
            print(i)
            return
    print('0')
main()
