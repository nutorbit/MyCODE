#include<bits/stdc++.h>

using namespace std;

int main(){
    int N; cin >> N;
    deque<int> ans;
    if(N == 1){
        cout << "1" << endl;
        return 0;
    }
    if(N <= 0){
        cout << "-1" << endl;
        return 0;
    }
    for(int i=9; i>=2; i--){
        if(N%i == 0){
            while(N%i == 0){
                ans.push_front(i);
                N /= i;
            }
        }
    }
    if(ans.empty() or N != 1){
        cout << "-1" << endl;
        return 0;
    }
    for(auto v: ans)
        cout << v << "";
    cout << endl;
}
