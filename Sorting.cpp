#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef struct info{
    int val,before,after;
}INFO;
INFO from[200001]={};

bool comp(const INFO &a,const INFO &b){
    return a.before < b.before;
}

int main(){
    int N,tmp; scanf("%d",&N);
    FOR(i,0,N){
        scanf("%d",&tmp);
        from[tmp].before = i+1;
        from[tmp].val = tmp;
    }
    FOR(i,0,N){
        scanf("%d",&tmp);
        from[tmp].after = i+1;
        //cout << "x";
    }
    from[0].before = INT_MAX;
    sort(from,from+N+1,comp);
    
    FOR(i,1,N){
        if(from[i].after  < from[i-1].after ){
            printf("%d\n",N-i);
            return 0;
        }
        //cout << from[i].after << " " ;
    }
    printf("0\n");
}
