'''double'''
def main():
    '''main'''
    rate = 1 + float(input())
    amount = rate
    year = 0
    while amount < 2:
        year += 1
        amount *= rate
    print(year+1)
main()
