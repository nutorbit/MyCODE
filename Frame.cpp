#include<cstdio>
#include<algorithm>

using namespace std;

#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

typedef struct node{
    int x1,x2,y1,y2;
}NODE;

NODE arr[1001]={};

struct comp{
    bool operator ()(const NODE &a,const NODE &b){
        if(a.x1 == b.x1)return a.y1 > b.y1;
        return a.x1 < b.x1;
    }
}comp;

int main(){

    int n,m,t1,t2,t3,t4;
    scanf("%d %d",&n,&m);
    FOR(i,0,n){
        scanf("%d %d %d %d",&t1,&t2,&t3,&t4);
        arr[i].x1 = t1,arr[i].x2 = t3,arr[i].y1 = t2,arr[i].y2 = t4;
    }
    sort(arr,arr+n,comp);
    FOR(i,0,m){
        scanf("%d %d %d %d",&t1,&t2,&t3,&t4);
        int c = 0;
        FOR(j,0,n){
            if(arr[j].x1 < t3 && arr[j].y1 > t4 && arr[j].x2 > t1 && arr[j].y2 < t2){
                c++;
            }
        }
        printf("%d\n",c);
    }
    
}
