#include <cstdio>
#include <algorithm>

using namespace std;

double Work[22][22];
double DP[1<<21];

int Count(int A){
    int C=0;
    for(int i=0;i<21;i++){
        if(1<<i&A)C++;
    }
    return C;
}

int main(){
    int N;
    scanf("%d",&N);
    // cin>>N;
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            scanf("%lf",&Work[i][j]);
      //    cin>>Work[i][j];
            Work[i][j] = Work[i][j]/100;
        }
    }
    int T = (1<<N);
    //printf("%d\n",T);
    for(int i=0;i<T;i++){
        int Ans = 0;
        if(Count(i)==1){
            for(int j=0;j<21;j++){
                if((1<<j)&i){
                    DP[i] = Work[0][j];
                }
            }
        }else{
            int T = Count(i);
            for(int j=0;j<21;j++){
                if(((1<<j)&i)){
                    DP[i] = max(DP[i],DP[(1<<j)^i]*Work[T-1][j]);
                }
            }
        }
    }
    printf("%lf",100*DP[(1<<N)-1]);
    //cout << DP[(1<<N)-1];
 }
