#include<bits/stdc++.h>

using namespace std;

#define val first
#define idx second

vector<int> tree[250005]{};
pair<int, int> A[50005]={};

inline int mid(int l, int r){ return (l+r)/2;}
inline int L(int node){ return node<<1;}
inline int R(int node){ return (node<<1)+1;}

void build(int node, int l, int r){
    if(l == r){
        tree[node].push_back(A[l].val); 
    }else{
        build(L(node), l, mid(l, r));
        build(R(node), mid(l, r)+1, r);
        merge(tree[L(node)].begin(), tree[L(node)].end(),
                tree[R(node)].begin(), tree[R(node)].end(), back_inserter(tree[node]));
    }
}

int query(int node, int from, int to, int l, int r, int k){
	if(l > to || r < from) return 0;
	if(l > from and r < to){
		return lower_bound(tree[node].begin(), tree[node].end(), k) - tree[node].begin();
	}
	return query(L(node), from, to, l, mid(l, r), k) + query(R(node), from, to, mid(l, r)+1, r, k);
}


int main(){
    ios_base::sync_with_stdio(0);
    int N, Q; cin >> N >> Q;
    for(int i=0; i<N; i++){
        cin >> A[i].val;
        A[i].idx = i;
    }

    sort(A, A+N);
    build(1, 0, N);
    for(int i=0; i<Q; i++){
        int l, r; cin >> l >> r;
        int interval = query(1, l, r, 0, N, 1);
        cout << interval << endl;

    }
}
