import math
cal_area = lambda radius: math.acos(-1)*radius**2

cal_circumference = lambda radius: 2*math.acos(-1)*radius

def main():
    input_str = list(map(float, input('Please insert sequence of radius, seperated by comma: ').split(',')))
    areas = list(map(cal_area, input_str))
    circumferences = list(map(cal_circumference, input_str))

    for i in range(len(input_str)):
        print('radius: {0:.2f}, area: {1:.2f}, circumference: {2:.2f}'.format(input_str[i], areas[i], circumferences[i]))

if __name__ == '__main__':
    main()
