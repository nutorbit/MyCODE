#include<bits/stdc++.h>

using namespace std;

template<class T>
class LinkedList{
    private:
    struct node{
        int32_t val;
        node *next;
    };
    node *now, *front;
    public:
        LinkedList(){
            now = NULL;
            front = NULL;
        }
        void appendLast(int32_t data){
            if(now == NULL){
                node *tmp = new node();
                tmp->val = data;
                tmp->next = NULL;
                now = tmp;
                front = now;
            }else{
                node *tmp = new node();
                tmp->val = data;
                now->next = tmp;
                now = tmp;
            }
        }
        void Pr(){
            node *runner = new node();
            runner = front;
            cout << runner->val << endl;
            while(runner->next != NULL){
                runner = runner->next;
                cout << runner->val << endl;
            }
        }
};

template<class T>
class SegmentTree{
    SegmentTree(){
        
    }
}


int main(){
    LinkedList<int32_t> LL;
    LL.appendLast(10);
    LL.appendLast(20);
    LL.appendLast(30);
    LL.appendLast(20);
    LL.appendLast(10);
    LL.Pr();

}