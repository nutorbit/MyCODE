#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;
typedef struct info{
    ll s ;
    int idx;
}INFO;
bool comp(const INFO &a,const INFO &b){
    return a.s > b.s;
}
vector<INFO> arr;

int main(){
    int N; scanf("%d",&N);
    ll sumAll = 0;
    FOR(i,0,2*N-1){
        int a,b; scanf("%d %d",&a,&b);
        INFO u; u.s = a+b; u.idx = i+1;
        arr.push_back(u);
        sumAll += u.s;
    }
    sort(arr.begin(),arr.end(),comp);
    ll sum = 0;
    FOR(i,0,N){
        sum += arr[i].s;
    }
    if(sum >= sumAll/2){
        FOR(i,0,N){
            printf("%d ",arr[i].idx);
        }
    }else{
        cout << "-1" << endl;
    }
}



