#include<iostream>
#include<vector>
#include<cctype>

using namespace std;

int main(){
    vector<char> v;
    char in;
    char tmp;
    int t=0;
    while (true) {
        scanf("%c%c",&in,&tmp);
        v.push_back(in);
        if (tmp=='\n') {
            break;
        }
    }
    for (int i=0; i<v.size(); i++) {
        if (v[i-2] !='A' && v[i-1]=='C' && v[i]=='V') {
            if (v[i-2]==0) {
                cout << "No data in clipboard!!" << endl;
                t++;
            }else{
                v.erase(v.begin()+i-1,v.begin()+i+1);
                cout << v[i-2];
                t++;
                cout << endl;
            }
        }
        if (v[i-2]=='A' && v[i-1]=='C' && v[i]=='V') {
            v.erase(v.begin()+i-2,v.begin()+i+1);
            for (int j=0; j<i-2; j++) {
                cout << v[j] << " ";
            }
            t++;
            cout << endl;
        }
    }
    if (t==0) {
        cout << "No data in clipboard!!" << endl;
    }
}