#include <bits/stdc++.h>
#define rep(i,j,k) for(i=j;(j<=k)? i<=k : i>=k ;(j<=k)? i++ : i--)
#define LSONE(S) ((-S)&S)
#define ls1(i) for(;i;i-=LSONE(i))
#define ls2(i,n) for(;i<=n;i+=LSONE(i))
#define fi first
#define se second
#define all(x) x.begin() , x.end()
#define eb emplace_back

using namespace std;

typedef pair<int,int> pii;
vector<pii> List;
vector<int> idx , val;

class Fenwick {
    int ft[100002];
    int i;
public:
    int S;
    Fenwick(){
        fill(ft,ft+100002,0);
    }
    int rmq(int a){
        int ans = 0;
        ls1(a){
            ans += ft[a];
        }
        return ans;
    }
    int sum(int a,int b){
        return rmq(b) - rmq(a-1);
    }
    void update(int a,int x){
        ls2(a,S){
            ft[a] += x;
        }
    }
};
int main(){
    int n,m;
    cin >> n >> m;
    int i,j,k;
    rep(i,0,m-1){
        int a,b;
        cin >> a >> b;
        List.eb(a,b+1);
        idx.eb(a) , idx.eb(b+1);
//      cout << a  << " | " << b << '\n';
    }
//    cout << endl;
    string c;
    cin >> c;
    int color = (c[0] == 'R')? 0 : (c[0]=='G')? 1 : 2;
    idx.eb(1);
    idx.eb(n+1);
//    for(auto i : idx){
//
//        cout << i << " " ;
//    }
    sort(all(idx));
    vector<int>::iterator it  = unique(all(idx));
    idx.resize(distance(idx.begin(),it));
//for(auto i : idx){
//    cout << i << " ";
//}
//cout << endl;
    Fenwick FT;
    int S = idx.size();
    FT.S = S;
    rep(i,0,m-1){
        int l = lower_bound(all(idx),List[i].fi) - idx.begin();
        int r = lower_bound(all(idx),List[i].se) - idx.begin();
        FT.update(l+1,1);
        FT.update(r+1,-1);
//  rep(j,1,4){
//   cout << FT.rmq(j) << " " ;
//  }
//  cout <<endl;
    }
    int Ans = 0;
    rep(i,0,S-2){
        if((S-2) < 0) break;
        Ans += (idx[i+1] - idx[i] ) *  ((color-(FT.rmq(i+1)%3)+3)%3);
//   cout << idx[i] << " " << idx[i+1] << " " << ((color-(FT.rmq(i+1)%3)+3)%3) << '\n';
    }
    cout << Ans;
}