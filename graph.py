from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x =[-10,4.4,9.8,-10]
y =[-2.3,20.3,-10,-2.3]
z =[0,9.5,0,0]

x2=[0,0.68,0]
y2=[0,-1.14,0]
z2=[0,1.82,0]

px=[0.67,0]
py=[-1.12,0]
pz=[1.79,0]

ax.plot_wireframe(x, y, z, color="red")
ax.plot_wireframe(x2, y2, z2 ,color="blue")
ax.plot_wireframe(px, py, pz, color="green")
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
