'''daysCount'''
def invalid(day, month, year):
    '''checkInvalid'''
    listmonth = [-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if year%4 == 0:
        listmonth[2] = 29
    if 1 <= day <= listmonth[month]:
        return True
    return False
def main():
    '''main'''
    date = input()
    month, day, year = date.split('/')
    month = int(month)
    day = int(day)
    year = int(year)
    daynum = (31 * (month - 1)) + day
    if month > 2:
        daynum = daynum - ((4 * month + 23)//10)
        if ((year % 4 == 0 or year % 400 == 0 and (year % 100 != 0))) and (month > 2):
            daynum = daynum + 1
    if invalid(day, month, year):
        state = daynum%10
        print(date, 'is the {}'.format(daynum), end='')
        if state == 1:
            print('st', end=' ')
        if state == 2:
            print('nd', end=' ')
        if state > 2 or state == 0:
            print('th', end=' ')
        print('days of the year')
    else:
        print(date, 'is invalid.')
main()
