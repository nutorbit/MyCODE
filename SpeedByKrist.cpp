#include <bits/stdc++.h>
#include <iostream>
#include <cstring>
#define input() (*std::istream_iterator<int>(cin))
#define rep(i,k,n) for(int i=(k);i!=int(n);++i)
#define loop(i,n) for(int i = n; i--;)
#define all(x) (x).begin(),(x).end()
#define sz(x) (x).size()
#define fi first
#define se second
using namespace std;
typedef long long lli;
typedef short sht;
typedef std::pair<int,int> pii;

const int inf = 1e9 + 10;
const int mxn = 8e4 + 10;
const int mxk = 10;
bool bott[mxn];
lli dp[mxn][mxk];
std::vector<pii> edge[mxn];

struct Data {
    int nd;
    lli wei;
    sht cnt;
    int bit;
    Data (int _a, lli _b, sht _c, int _d): nd(_a), wei(_b), cnt(_c), bit(_d) {}
    bool operator < (const Data& rhs) const { return wei > rhs.wei; }
};

int main(){
    cin.sync_with_stdio(0);
    int n, m, l, q;
    cin >> n >> m >> l >> q;
    loop(_,m){
        int u, v, w;
        cin >> u >> v >> w;
        edge[u].emplace_back(v, w);
    }
    rep(i,0,l){
        bott[input()] = true;
    }
    std::fill_n (*dp, (n+1) * mxk, inf);
    std::priority_queue<Data> pq;
    pq.emplace(1, dp[1][0] = 0, 0, 0);
    while (!pq.empty()){
        Data d = pq.top();  
        pq.pop();
        if (d.nd == n) return !printf("%lld", d.wei);
        if (dp[d.nd][d.cnt] == d.wei){
            if (bott[d.nd] and d.cnt != q and d.bit != d.nd){
                d.cnt += 1;
                d.bit = d.nd;
            }
            for (pii nx : edge[d.nd]){
                lli wei = d.wei + (nx.se >> d.cnt);
                if (dp[nx.fi][d.cnt] > wei){
                    dp[nx.fi][d.cnt] = wei;
                    pq.emplace (nx.fi, wei, d.cnt, d.bit);
            }}
    }}
    std::terminate();
}
