#include <iostream>
#include <vector>

using namespace std;

void print(vector<int> v){
    for(int i = 0; i < v.size(); i++) cout << v[i] << " ";
    cout << endl;
}

vector<int> merge(vector<int> left, vector<int> right){
    vector<int> result;
    while ((int)left.size() > 0 || (int)right.size() > 0) {
        if ((int)left.size() > 0 && (int)right.size() > 0) {
            if ((int)left.front() <= (int)right.front()) {
                result.push_back((int)left.front());
                left.erase(left.begin());
            }
            else {
                result.push_back((int)right.front());
                right.erase(right.begin());
            }
        }  else if ((int)left.size() > 0) {
            for (int i = 0; i < (int)left.size(); i++)
                result.push_back(left[i]);
            break;
        }  else if ((int)right.size() > 0) {
            for (int i = 0; i < (int)right.size(); i++)
                result.push_back(right[i]);
            break;
        }
    }
    return result;
}

vector<int> mergeSort(vector<int> m){
    if (m.size() <= 1)
        return m;
    
    vector<int> left, right, result;
    int middle = ((int)m.size()+ 1) / 2;
    
    for (int i = 0; i < middle; i++) {
        left.push_back(m[i]);
    }
    
    for (int i = middle; i < (int)m.size(); i++) {
        right.push_back(m[i]);
    }
    
    left = mergeSort(left);
    right = mergeSort(right);
    result = merge(left, right);
    
    return result;
}

int main(){
    vector<int> v;
    
    v.push_back(38);
    v.push_back(27);
    v.push_back(43);
    v.push_back(3);
    v.push_back(9);
    v.push_back(82);
    v.push_back(10);
    
    print(v);
    cout << "------------------" << endl;
    
    v = mergeSort(v);
    
    print(v);
}
/*

#include <iostream>

// a: original, b: helper array
void merge(int a[], int b[],  int low, int mid, int high)
{
    for(int i = low; i <= high; i++) {
        b[i] = a[i];
    }
    int left = low;
    int right = mid+1;
    int index = low;
    while(left <= mid && right <= high) {
        if(b[left] <= b[right])
            a[index++] = b[left++];
        else
            a[index++] = b[right++];
    }
    
    // copy remainder of the left side
    int remainder = mid - left +1;
    for(int i = 0; i < remainder; i++) {
        a[index+i] = b[left+i];
    }
}

// merge sort starts here
void mergeSort(int a[], int b[], int low, int high)
{
    if(low >= high) return;
    int mid = (low+high)/2;
    mergeSort(a, b, low, mid);
    mergeSort(a, b, mid+1, high);
    merge(a, b, low, mid, high);
}

// prepare for real mergesort()
void mergeSort(int a[], int len)
{
    int *b = new int[len];
    mergeSort(a, b, 0, len-1);
    delete[] b;
}

int main()
{
    int a[] = {9,8,7,6,5,4,3,2,1,0};
    int size = sizeof(a)/sizeof(int);
    mergeSort(a, size); 
    
    return 0;
}

*/
