#include<cstdio>

using namespace std;
/* 
 https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes#/media/File:Sieve_of_Eratosthenes_animation.gif
 เป็นรูปแบบ algorithm ที่เร็วกว่าการไล่ลูปธรรมดาหลายเท่าตัว
 
*/
bool pr[10000001]={};

void Prime(){
    for(int i=2;i<1000001;i++){
        if(pr[i] == true){
            continue;
        }
        for(int j=i*2;j<1000001;j+=i){
            pr[j] = true;
        }
    }
}

int main(){
    int ch;
    Prime();
    /*
     ถ้าเราอยากเช็คว่าจำนวนที่เราใส่ เป็นหรือไม่เป็นทำได้ดังนี้
     
     scanf("%d",&ch);
     if(pr[ch] == false){
        printf("Prime\n");
     }else{
        printf("Not Prime\n");
     }
     
     */
    
    for(int i = 2;i<100;i++){//การแสดงผล จำนวนเฉพาะ ระหว่าง 2 - 100
        if(pr[i] == false){
            printf("%d ",i);
        }
    }
    printf("\n");
}

//วิธีแบบทั่วๆไปที่ช้ามาก และถ้าจะให้หาหลายๆตัวแบบอันแรก ยิ่งจะช้ากว่าเดิม แค่หาว่าตัวนั้นเป็นรึปล่าวก็ใช้เวลาพอสมควรแล้ว
/*
int main(){
    int ch;
    bool Prime = true;//เป็นการเริ่มให้เรา ให้ตัวที่หาเป็น prime ไว้ก่อน ถ้า false = ไม่เป็น
    scanf("%d",&ch);
    for(int i=2;i<ch;i++){
        if(ch%i == 0){//ถ้ามีตัวหารลงตัวระหว่างนี้ แปลว่าไม่เป็น
            Prime = false;
            break;
        }
    }
    if(ch <= 1){
        Prime = false;
    }
    if(Prime == true){
        printf("Prime\n");
    }else{
        printf("Not Prime\n");
    }
}
 */
