#include<cstdio>
#include<algorithm>

using namespace std;

typedef long long int ll;

typedef struct INFO{
    ll x;
    ll h;
}INFO;

bool comp(const INFO &a,const INFO &b){
    return a.x < b.x;
}

INFO arr[100001]={};

int minR = -1 ,minL = -1;
int siR,siL;
int n;
int maxx = -1,maxx2 = -1;
ll x,h;
int tmp = 0;

void recurR(int i,int val){
    tmp = 0;
    if(minR < val){
        minR = val;
        printf("%d\n",minR);
    }
    if(i >= n ){
        return ;
    }
    else if(arr[i].h + arr[i].x > arr[i+1].x && i+1 < n){
        for(int k=i+1;;k++){
            if(arr[i].h + arr[i].x > arr[k].x && k < n){
                //printf("o ");
                tmp++;
                //printf("asd %d\n",tmp);
            }else{
                //printf("%d :%d\n",i+tmp,val+tmp);
                recurR(i+tmp,val+tmp);
                break;
            }
        }
        
    }else{
        return;
    }
}

void recurL(int i,int val){
    //printf("%lld\n",arr[i].h - arr[i].x);
    tmp = 0;
    if(minL < val){
        minL = val;
    }
    if(i < 0 ){
        return ;
    }
    else if(arr[i].x - arr[i].h < arr[i-1].x && i-1 >= 0){
        for (int k=i-1; ; k--) {
            if(arr[i].x - arr[i].h < arr[k].x && k >= 0){
                tmp++;
            }else{
                recurL(i-tmp,val+tmp);
                break;
            }
        }
        recurL(i-1,val+1);
    }else{
        return ;
    }
}

int main(){
    
    
    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%lld %lld",&x,&h);
        arr[i].x = x;
        arr[i].h = h;
    }
    sort(arr,arr+n,comp);
    
    for(int i=0;i<n;i++){
        recurR(i,1);
        if(minR > maxx){
            maxx = minR;
            siR = i+1;
        }
    }
    
    for(int i=n-1;i>=0;i--){
        recurL(i,1);
        if(minL > maxx2){
            maxx2 = minL;
            siL = i+1;
        }
    }
    
    /*
    minR = -1;
    recurR(1,0);
    printf("%d",minR);
    */
    //printf("R:%d i:%d \n",maxx,siR);
    //printf("L:%d i:%d \n",maxx2,siL);
    
    if(maxx == maxx2){
        if(siR < siL){
            printf("%d R\n",siR);
        }else{
            printf("%d L\n",siL);
        }
    }else{
        if(maxx > maxx2){
            printf("%d R\n",siR);
        }else{
            printf("%d L\n",siL);
        }
    }
    

}
