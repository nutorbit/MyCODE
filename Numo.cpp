#include<cstdio>

using namespace std;

bool num[101]={};

int main(){
    int N,X;
    scanf("%d %d",&N,&X);
    if(X>N){
        printf("IMPOSSIBLE\n");
    }else{
        if(X == 1){
            if(N == 2){
                printf("2-1\n");
            }else if(N == 3){
                printf("3-2*1\n");
            }else if(N == 4){
                printf("4-3*2-1\n");
            }else if(N >= 5){
                printf("5-4+2-3");
                num[5] = true,num[4] = true,num[2] = true,num[3] = true;
                for (int i=2; i<=N; i++) {
                    if(!num[i]){
                        printf("*%d",i);
                    }
                }
                printf("+1\n");
            }else{
                printf("IMPOSSIBLE\n");
            }
        }
        else if(X == 2){
            if(N == 2){
                printf("2*1\n");
            }else if(N == 3){
                printf("3-2+1\n");
            }else if(N >= 4){
                printf("4-3-1");
                num[4]=true,num[3]=true,num[1]=true;
                for (int i=3; i<=N; i++) {
                    if(!num[i]){
                        printf("*%d",i);
                    }
                }
                printf("+2\n");
            }else{
                printf("IMPOSSIBLE\n");
            }
        }else if(X == 3){
            if(N == 3){
                printf("2-1*3\n");
            }else if(N == 4){
                printf("4-2-1*3\n");
            }else if(N >= 5){
                printf("5+1-4-2");
                num[5]=true,num[1]=true,num[4]=true,num[2]=true;
                for (int i=4; i<=N; i++) {
                    if(!num[i])
                        printf("*%d",i);
                }
                printf("+3\n");
            }else{
                printf("IMPOSSIBLE\n");
            }
        }else if(X >= 4){
            printf("3-2-1");
            num[3]=true,num[2]=true,num[1]=true,num[X]=true;
            for (int i=1; i<=N; i++) {
                if(!num[i])
                    printf("*%d",i);
            }
            printf("+%d\n",X);
        }
    }
}
