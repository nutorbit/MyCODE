#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cmath>

using namespace std;

int arr[1<<11]= {};
int MAX[1<<11][1<<11]= {};
int MIN[1<<11][1<<11]= {};

int main(){
    
    int n,q;
    char c;

    scanf("%d %d",&n,&q);
    for(int i=0; i<n; i++){
            scanf("%d",&arr[i]);
    }

    for(int i=0; i<n; i++){
        for(int j=0; i+j<n; j++){
            int k = i+j;
            if(j==k){
                MAX[j][k] = arr[j];
                MIN[j][k] = arr[j];
            }else{
                MAX[j][k] = max(arr[k],MAX[j][k-1]);
                MIN[j][k] = min(arr[k],MIN[j][k-1]);
            }
        }
    }
    int a,b;
    int d;
    for(int i=0; i<q; i++){
        if(c=='P'){
            scanf("%d %d",&a,&b);
            if(a>b)swap(a,b);
            printf("%d\n",MAX[a][b]);
        }else{
            scanf("%d %d",&a,&b);
            if(a>b)swap(a,b);
            printf("%d\n",MIN[a][b]);
        }

    }

}

