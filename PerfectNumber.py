'''Perfect Number'''
def main():
    '''ohhoo'''
    num = int(input())
    factor = []
    for i in range(1, num):
        if not num%i:
            factor += [i]
    print(' + '.join(str(i) for i in factor) + " = " + str(sum(factor)))
    if sum(factor) == num:
        print('Yes!!! This is a perfect number.')
    else:
        print('Oops!!! This is not a perfect number.')
main()
