#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll arr[12]={};

ll GCD(ll a,ll b){
    while((a%=b) && (b%=a));
    return a+b;
}

int main(){
    ll tmp;
    int n;
    scanf("%d ",&n);
    FOR(i,0,n){
        scanf("%lld",&tmp);
        i==0?arr[i] = tmp:arr[i] = GCD(tmp,arr[i-1]);
    }
    //cout << arr[n-1] << endl;
    ll ans = 0;
    int x = sqrt(arr[n-1]);
    FOR(i,1,x+1){
        if(arr[n-1] % i == 0){
            ans +=2;
            if(i == arr[n-1]/i)ans--;
        }
    }
    printf("%lld\n",ans);
    
}
