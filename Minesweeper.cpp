#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int ROW,COL;
char field[102][102]={};
int num[102][102]={};
char sen[102][102]={};

void clear(int row,int col){
	FOR(i,0,row)
		FOR(j,0,col)
			num[i][j] = 0;
}

void fill(int i,int j){
    FOR(x,-1,2)
        FOR(y,-1,2)
            if(x+i < ROW && x+i >= 0 && y+j < COL && y+j >= 0)
                num[x+i][y+j]++;
}

int main(){
    int t;
    scanf("%d ",&t);
	while(t--){
        scanf("%d",&ROW);
        COL = ROW;
		FOR(i,0,ROW){
			FOR(j,0,COL){
				scanf(" %c",&field[i][j]);
				if(field[i][j] == '*')fill(i,j);
			}
		}
        
        FOR(i,0,ROW)
            FOR(j,0,COL)
                scanf(" %c",&sen[i][j]);
        
        FOR(i,0,ROW){
			FOR(j,0,COL){
				if(sen[i][j] == 'x')printf("%d",num[i][j]);
				else printf(".");
			}
			printf("\n");
		}
		cout << endl;
		clear(ROW,COL);
	}

}

 
 
