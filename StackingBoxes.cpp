#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<stack>

using namespace std;

typedef struct node{
//    vector<int> arr;
    int arr[11];
    int idx;
}NODE;

vector<NODE> vec;

bool comp(const int &a,const int &b){
    return a<b;
}
bool comp2(const NODE &a,const NODE &b){
    for (int i=0; i<11; i++) {
        if(a.arr[i] != b.arr[i])return a.arr[i] < b.arr[i];
    }
    return a.arr[0] < b.arr[0];
}
/*
bool cmp(int a,int b){
    for(int i=0;i<=vec[a].arr.size();i++)
        if(vec[a].arr[i]<=vec[b].arr[i])
            return false;
    return true;
}*/

int DP[2][40]={};
int k,n,tmp;
stack<int> st;

bool fun(int x,int y){
    for (int i=0; i<n; i++) {
        if(vec[x].arr[i] <= vec[y].arr[i]){
            return false;
        }
    }
    return true;
}


vector<int> path;
vector<int>::iterator it;
void backtract(int x)
{
    if ( x == DP[1][x] )
    {
        path.push_back(vec[x].idx);
        return ;
    }
    
    backtract(DP[1][x]);
    
    path.push_back(vec[x].idx);
}

int main(){
   
    while(cin>>k>>n){
        
        for (int i=0; i<k; i++) {
            NODE a;
//            a.arr.clear();
            for (int j=0; j<n; j++) {
//                scanf("%d",&tmp);
                cin>>tmp;
                a.arr[j] = tmp;
            }
            a.idx = i+1;
            vec.push_back(a);
            sort(vec[i].arr,vec[i].arr+n);
            DP[0][i] = 1;
            DP[1][i] = i;
        }
        
            sort(vec.begin(),vec.end(),comp2);
            /*for ( int i = 0; i < k; i++ ){
                for ( int j = i + 1; j < k; j++ ){
                    if ( fun (i, j) )
                        swap (vec[i], vec[j]);
                }
            }*/
            int maxx = 1,lst = 0;
            for (int i=1; i<k; i++) {
                for (int j=0; j<i; j++) {
                    if(fun(i,j) && DP[0][i] < DP[0][j]+1){
                        DP[0][i] = DP[0][j]+1;
                        DP[1][i] = j;
                        if(maxx < DP[0][i]){
                            maxx = DP[0][i];
                            lst = i;
                        }
                    }
                }
            }
            printf("%d\n",maxx);
            backtract(lst);
            for(it=path.begin();it!=path.end();it++)
            {
                if(it!=path.begin())
                    cout<<" ";
                cout<<*it;
            }
            
            cout<<endl;
        
        path.clear();
        vec.clear();
        memset(DP[0],0,sizeof(DP[0]));
        memset(DP[1],0,sizeof(DP[1]));
    }
    
    
}
