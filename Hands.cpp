#include<cstdio>
#include<algorithm>
#include<functional>

using namespace std;

int arr[1000001]={};

int main(){
    int N,K;
    scanf("%d %d",&N,&K);
    for (int i=0; i<N; i++) {
        scanf("%d",&arr[i]);
    }
    sort(arr,arr+N,greater<int>());
    int ans = 0;
    int count = 0;
    while(true){
        ans+=arr[count];
        count+=K;
        if (count >= N) {
            break;
        }
    }
    printf("%d\n",ans);
}
