#include <iostream>

using namespace std;

long long find(int a,int b){
    int c;
    while ( a != 0 ) {
        c = a; a = b%a;  b = c;
    }
    return b;
}

int main(){
    long long count,in[1000001];
    cin >> count;
    for (int i=0; i<count; i++) {
        cin >> in[i];
    }
    for (int i=0; i<count-1; i++) {
        in[i+1] = (in[i]*in[i+1])/(find(in[i],in[i+1]));
    }
    cout << in[count-1];
}
