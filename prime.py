'''prime'''
def sieve():
    '''
        sieve of eratosthenes complexity time O(n)
        ถ้าใช้ normal loop complexity time O(N^2)
    '''
    prime = []
    notprime = set()
    for i in range(2, 1001):
        if i not in notprime:
            prime += [i]
            j = i
            while j < 1001:
                notprime.add(j)
                j += i
    return prime

def main():
    '''main'''
    listprime = sieve()
    val = int(input())
    ans = []
    for i in range(10001):
        if val >= listprime[i]:
            ans += [listprime[i]]
        else:
            break
    print(*ans)


main()
