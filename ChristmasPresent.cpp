#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

bool comp(const int &a,const int &b){
    return a < b;
}

int N,M,in;
vector<int> A,B;
int MIN = INT_MAX,total = 0;

int main(){

    scanf("%d %d",&N,&M);
    FOR(i,0,N)
        scanf("%d",&in),A.push_back(in);
    FOR(i,0,M)
        scanf("%d",&in),B.push_back(in);
    sort(A.begin(),A.end(),comp);
    sort(B.begin(),B.end(),comp);
    total = 0;
    if(A.size() >= 2){
        FOR(i,0,2)
            total += A[i];
        MIN = total;
    }
    total = 0;
    if(B.size() >= 2){
        FOR(i,0,2)
            total += B[i];
        MIN = min(MIN,total);
    }
    total = 0;
    FOR(i,0,A.size()){
        total = A[i];
        FOR(j,0,B.size()){
            total += B[j]+100;
            MIN = min(MIN,total);
            total = A[i];
        }
    }
    printf("%d\n",MIN);
}
