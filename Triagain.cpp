#include <cstdio>
#include <algorithm>

using namespace std;

int data[1<<21];
int n;

bool comp(const int &a,const int &b){
    return a < b;
}

int BS(int x,int a){
    
    int mid=0,front=a,rear=n-1;
    
    if (x>data[rear]) return rear;
    if (x<data[front]) return front;
    
    while (true){
        
        mid=(front+rear)/2;
        
        if (front==rear){
            
            while (data[mid+1]<=x)mid++;
            
            break;
            
        }
        
        if (x>data[mid])front=mid;
        
        if (x==data[mid]){
            
            while (data[mid]==data[mid+1])mid++;
            break;
            
        }
        
        if (front+1==rear){
            mid=front;
            break;
        }
        
        if (x<data[mid])rear=mid;
        
    }
    return mid;
    
}



int main(){
    
    int a,max=-1;
    scanf("%d",&n);
    for (int i=0;i<n;i++){
        scanf("%d",&data[i]);
    }
    if(n < 3){
        printf("%d\n",n);
        return 0;
    }
    
    sort(data,data+n,comp);
    
    for (int i=0;i<n-1;i++){
        
        //printf("%d ",data[i]);
        a=BS(data[i]+data[i+1],i);
        if (a-i+1>max) max=a-i+1;
        
    }
    
    printf("%d\n",max);
    
    return 0;
    
}
