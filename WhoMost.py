'''WhoMost'''
def main():
    '''main'''
    num = int(input())
    hashmap = {}
    maxvote = float('-inf')
    for _ in range(num):
        val = input()
        if val not in hashmap:
            hashmap[val] = 0
        hashmap[val] += 1
        maxvote = max(maxvote, hashmap[val])
    for key in hashmap:
        if hashmap[key] == maxvote:
            print(key)
            print(hashmap[key])
            break
main()
