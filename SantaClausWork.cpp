#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

int n;
ll total = 0,ans = 1;

int main(){
    scanf("%d",&n);
    FOR(i,0,n){
        total = ans;
        total *= 3;
        ans += total;
    }
    printf("%lld\n",ans);
}
