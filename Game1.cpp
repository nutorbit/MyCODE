#include<bits/stdc++.h>

using namespace std;

#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

typedef struct node{
    int x,y;
}NODE;

int H,W,tmp;
int dis[501][501]={};
bool ch[501][501]={};
int MAX = INT_MIN;
int ans = 0;
int point[250002]={};
queue<NODE> q;

void create(){
    NODE u,now;
    u.x = H-1,u.y = 0;
    q.push(u);
    ch[u.x][u.y] = true;
    while(!q.empty()){
        now = q.front();
        q.pop();
        int rx[] = {0,-1};
        int ry[] = {1,0};
        FOR(i,0,2){
            if(!ch[now.x+rx[i]][now.y+ry[i]] && now.x+rx[i] >= 0 && now.y+ry[i] < W){
                dis[now.x+rx[i]][now.y+ry[i]] = dis[now.x][now.y]+1;
                if(MAX<dis[now.x][now.y]+1)MAX=dis[now.x][now.y]+1;
                point[dis[now.x+rx[i]][now.y+ry[i]]]++;
                ch[now.x+rx[i]][now.y+ry[i]] = true;
                u.x = now.x+rx[i],u.y = now.y+ry[i];
                q.push(u);
            }
        }
    }
    
}

int main(){
    scanf("%d %d",&H,&W);
    FOR(i,0,H){
        FOR(j,0,W){
            scanf("%d",&tmp);
            if(tmp == 1)ch[i][j] = true;
        }
    }
    /*
    FOR(i,0,H){
        FOR(j,0,W){
            ch[i][j]?printf("1 "):printf("0 ");
        }
        printf("\n");
    }
    */
    create();
    /*
    FOR(i,0,H){
        FOR(j,0,W){
            if(!board[i][j])point[dis[i][j]]++;
        }
    }
     */
    point[0]=1;
    //FOR(i,0,MAX+1)printf("%d %d \n",i,point[i]);
    FOR(i,0,MAX+1)ans += pow(point[i],2);
    //cout << endl;
    /*
    FOR(i,0,H){
        FOR(j,0,W){
            cout << dis[i][j] << " " ;
        }
        cout << endl;
    }
     */
    //cout << MAX << endl;
    
    cout << ans << endl;

}
