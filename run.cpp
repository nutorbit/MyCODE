#include<cstdio>
#include<string>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;

int arr[100001]={};
int n;
int dis1[100001]={};
int dis2[100001]={};

void LIS1(){
    int *tail = (int*)malloc(sizeof(int)*n);
    int len = 1;
    tail[0] = arr[0];
    dis1[0] = 1;
    for (int i=1; i<n; i++) {
        if(arr[i] > tail[len-1])tail[len++] = arr[i];
        else {tail[lower_bound(tail,tail+len,arr[i])-tail] = arr[i];}
        //else tail[BS(0,len-1,arr[i])] = arr[i];
        dis1[i] = len;
    }
    /*
     for (int i=0; i<n; i++) {
        printf("%d ",dis1[i]);
     }
     printf("\n");
    */
}

void LIS2(){
    int *tail = (int*)malloc(sizeof(int)*n);
    int len = 1;
    dis2[n-1] = 1;
    tail[0] = arr[n-1];
    for (int i=n-2; i>=0; i--) {
        if(arr[i] > tail[len-1])tail[len++] = arr[i];
        else tail[lower_bound(tail,tail+len,arr[i])-tail] = arr[i];
        dis2[i] = len;
    }
    /*
     for (int i=0; i<n; i++) {
         printf("%d ",dis2[i]);
     }
     printf("\n");
    */
}

int main(){
    
    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%d",&arr[i]);
    }
    //printf("L->R\n");
    LIS1();
    //printf("R->L\n");
    LIS2();
    int maxx = -1;
    for (int i=0; i<n; i++) {
        int a = dis1[i] + dis2[i]-1;
        if(maxx < a)maxx = a;
    }
    printf("%d\n",maxx);
}
