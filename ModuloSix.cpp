#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int n,s;
bool printed = false;

void recur(int sum,int tail){
    int average = sum / (n-1);
    if(sum <= 9*(n-1) and sum >= 1){
        int ses = sum % (n-1);
        //cout << ses << endl;
        FOR(i,0,n-1){
            if(average+ses < 10){
                printf("%d",average+ses);
                ses = 0;
            }else{
                printf("%d",9);
                ses = average + ses - 9;
            }
        }
        printf("%d\n",tail);
        printed = true;
    }
}

int main(){
    
    scanf("%d %d",&n,&s);
    if(s%3!=0){
        cout << "-1\n" ;
        return 0;
    }else{
        if(n == 1 and s == 6){
            printf("6\n");
            return 0;
        }
        for(int i=2;i<=8;i+=2){
            recur(s-i,i);
            if(printed)return 0;
        }
        if(!printed)cout << "-1\n";
    }
}

