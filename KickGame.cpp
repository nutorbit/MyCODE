#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int A[300003]={};
int B[300003]={};
int qs[300003]={};
int MAX = 0;
int N;

void BS(int posi,int left,int right){
    while(left < right){
        int mid = (left + right)/2;
        if(qs[posi+posi-mid] - qs[mid-1] - B[posi] <= A[posi] and posi+posi-mid-mid < N){
            /*
            cout << endl;
            for (int i=mid; i<=posi+posi-mid; i++) {
                cout << B[i] << " " ;
            }
            
            cout << endl;
            cout << A[posi] <<" " << qs[posi+posi-mid] - qs[mid-1] - B[posi] << endl;
            cout << endl;
             */
            right = mid;
            MAX = max(MAX,(posi+posi-mid-mid));
        }else{
            left = mid+1;
        }
    }
}

int main(){
    ios :: sync_with_stdio(false);
    cin >> N;
    FOR(i,0,N){
        cin >> A[i];
        A[i+N] = A[i];
        A[i+2*N] = A[i];
    }
    FOR(i,0,N){
        cin >> B[i];
        B[i+N] = B[i];
        B[i+2*N] = B[i];
    }
    FOR(i,0,3*N){
        qs[i] += qs[i-1] + B[i];
        //cout << qs[i] << " ";
    }
    FOR(i,N,2*N){
        MAX = 0;
        BS(i,0,i);
        //BS(i,i,3*N);
        cout << MAX << " " ;
    }
    cout << endl;
    
}
