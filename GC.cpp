#include<bits/stdc++.h>

using namespace std;
#define eb emplace_back

vector<int> adj[51]{};

int main(){
    int N, E; cin >> N >> E;
    int ans = 0;
    for(int i=0; i<E; i++){
        int a, b; cin >> a >> b;
        adj[a].eb(b);
        adj[b].eb(a);
    }
    vector<int> node;
    for(int i=0; i<N; i++)
        node.eb(INT_MAX);
    node[0] = 0;
    int color[N];
    for(int cl=0; cl<N; cl++)
        color[cl] = false;
    for(int i=1; i<N; i++){
        for(auto neig: adj[i]){
            if(node[neig] != INT_MAX){
                color[node[neig]] = true;
            }
        }
        for(int cl=0; cl<N; cl++){
            if(!color[cl]){
                node[i] = cl;
                ans = max(ans, cl);
                break;
            }
        }
        for(auto neig: adj[i]){
            if(node[neig] != INT_MAX){
                color[node[neig]] = false;
            }
        }
    }
    cout << ans+1 << endl;

}