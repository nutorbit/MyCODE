#include <iostream>
#include <vector>
#include <utility>
#include <cstdio>
#include <algorithm>

using namespace std;
int id[1000001], nodes, edges;
pair <long long, pair<int, int> > p[1000001];
pair <int,int> ans[1000001];
int indexx = 0;

bool compare(const pair<int,int> &a,const pair<int,int> &b){
    if(a.first == b.first){
        return a.second < b.second;
    }else{
        return a.first < b.first;
    }
}

void start(){
    for(int i = 0;i < 100001;++i)
        id[i] = i;
}

int root(int x){
    while(id[x] != x){
        id[x] = id[id[x]];
        x = id[x];
    }
    return x;
}

void union1(int x,int y){
    int p = root(x);
    int q = root(y);
    id[p] = id[q];
}

void krus(pair<int,pair<int,int> > p[]){
    int x, y;
    for(int i = 0;i < edges;++i){
        x = p[i].second.first;
        y = p[i].second.second;
        if(root(x) != root(y)){
            ans[indexx].first = x;
            ans[indexx].second = y;
            indexx++;
            union1(x, y);
        }
    }
}

int main(){
    
    int x,y,w;
    start();
    scanf("%d %d",&nodes,&edges);
    for(int i = 0;i < edges;++i){
        scanf("%d %d %d",&x,&y,&w);
        p[i] = make_pair(w, make_pair(x, y));
    }
    sort(p,p+edges);
    krus(p);
    sort(ans,ans+indexx,compare);
    for (int i=0; i<indexx; i++) {
        printf("%d %d\n",ans[i].first,ans[i].second);
    }
}