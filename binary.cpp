#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdio>

using namespace std;

struct node{
      int data;
      node *l;
      node *r;

      node(int n){
            data = n;
            l = NULL;
            r = NULL;
      }
};

typedef struct node NODE;

NODE *head = NULL;
NODE *tmp;

void recurAdd(NODE *tmp,int x){
      if(x < tmp->data){
            if (tmp->l == NULL) {
                  tmp->l = new NODE(x);
            }else{
                  recurAdd(tmp->l,x);
            }
      }else if(x >= tmp->data){
            if (tmp->r == NULL) {
                  tmp->r = new NODE(x);
            }else{
                  recurAdd(tmp->r,x);
            }
      }
}


void add(int x){
      tmp = head;
      if (head == NULL) {
            tmp = new NODE(x);
            head = tmp;
      }else{
            recurAdd(tmp,x);
      }

}

int x ;

void find(NODE *head){
      if (head->data == x) {
            cout << "FOUND" << endl;
            return ;
      }
      if (x < head->data) {
            if (head->l == NULL) {
                  cout << "NONE" << endl;
                  return ;
            }else{
                  find(head->l);
            }
      }
      if (x >= head->data) {
            if (head->r == NULL) {
                  cout << "NONE" << endl;
                  return ;
            }else{
                  find(head->r);
            }
      }

}

void preorder(NODE *check){
      cout << check->data << " ";
      if(check->l != NULL){
            preorder(check->l);
      }
      if(check->r != NULL){
            preorder(check->r);
      }
}

void inorder(NODE *check){
      if(check->l != NULL){
            inorder(check->l);
      }
      cout << check->data << " ";
      if(check->r != NULL){
            inorder(check->r);
      }
}

void postorder(NODE *check){
      if(check->l != NULL){
            postorder(check->l);
      }
      if(check->r != NULL){
            postorder(check->r);
      }
      cout << check->data << " ";
}



int main() {
      int count,in;
      cin >> count;
      for (int i = 0; i < count; i++) {
            cin >> in;
            add(in);
      }
      cin >> x;
      find(head);
      int choice;
      cin >> choice;
      switch (choice) {
            case 1:preorder(head);
            break;
            case 2:inorder(head);
            break;
            case 3:postorder(head);
            break;
      }

      return 0;
}
