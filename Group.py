'''Group'''
from math import ceil
def main(txt, num):
    '''main'''
    if not num:
        return print(txt)
    txt += ' '*(ceil(len(txt)/num)*num-len(txt))
    print('\n'.join([txt[i*num: i*num+num] for i in range(len(txt)) if i*num+num <= len(txt)]))
main(input(), int(input()))
