#include<bits/stdc++.h>

using namespace std;

int fac(int n){
    if(n == 1)
        return 1;
    return n*fac(n-1);
}

int main(){
    int T; cin >> T;
    while(T--){
        int N; cin >> N;
        int num[10]={};
        for(int i=1; i<=N; i++)
            num[i-1] = i;
        int posi = fac(N)/3;
        do{
            if(!posi){
                for(int i=0; i<N; i++){
                    cout << num[i];
                }
                cout << endl;
                break;
            }
            posi --; 
        }while(next_permutation(num, num+N));
    }
}
