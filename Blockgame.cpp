#include<iostream>
#include<cstdio>

using namespace std;

char field[101][101]={};
int r,c;
bool ch = true;
int point = 0;
bool ov = false;

void des(){
    //cout << "X Des" << endl;
    for(int i=0;i<r;i++){
        for (int j=0; j<c; j++) {
            if(field[i][j] != '#' && field[i][j] != '-'){
                char tmp = field[i][j];
                if(field[i+1][j] == tmp){
                    field[i][j] = '-';
                    field[i+1][j] = '-';
                    if(!ch){
                        point += 10;
                    }else{
                        point += 5;
                    }
                    ch = true;
                    ov = true;
                }
                if(field[i-1][j] == tmp){
                    field[i][j] = '-';
                    field[i-1][j] = '-';
                    if(!ch){
                        point += 10;
                    }else{
                        point += 5;
                    }
                    ch = true;
                    ov = true;
                }
                if(field[i][j+1] == tmp){
                    field[i][j] = '-';
                    field[i][j+1] = '-';
                    if(!ch){
                        point += 10;
                    }else{
                        point += 5;
                    }
                    ch = true;
                    ov = true;
                }
                if(field[i][j-1] == tmp){
                    field[i][j] = '-';
                    field[i][j-1] = '-';
                    if(!ch){
                        point += 10;
                    }else{
                        point += 5;
                    }
                    ch = true;
                    ov = true;
                }
            }
        }
    }
    if(!ov){
        point -= 5;
    }
}

void drop(){
    //cout << "X Drop" << endl;
    for (int i=r-1; i>=0; i--) {
        for (int j=c-1; j>=0; j--) {
            if(field[i][j] != '#' && field[i][j] != '-'){
                char tmp = field[i][j];
                field[i][j] = '-';
                int o = i;
                while(true){
                    if (field[o+1][j] == '-') {
                        o = o+1;
                    }else{
                        field[o][j] = tmp;
                        break;
                    }
                }
            }
        }
    }
}

void move(int i,int j,char di){
    char tmp = field[i][j];
    if (di == 'L') {
        if(field[i][j-1] == '-'){
            field[i][j] = '-';
            j = j-1;
            while(true){
                if(field[i+1][j] == '-'){
                    i = i+1;
                }else{
                    field[i][j] = tmp;
                    break;
                }
            }
        }
    }
    if (di == 'R') {
        if(field[i][j+1] == '-'){
            field[i][j] = '-';
            j = j+1;
            while(true){
                if(field[i+1][j] == '-'){
                    i = i+1;
                }else{
                    field[i][j] = tmp;
                    break;
                }
            }
        }
    }
}

int main(){
    
    cin >> r >> c;
    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            scanf(" %c",&field[i][j]);
        }
    }
    int rou;
    cin >> rou;
    for(int i=0;i<rou;i++){
        int a,b;
        char di;
        scanf("%d %d %c",&a,&b,&di);
        move(a,b,di);
        ov = false;
        ch = true;
        while(ch){
            //cout << "x" << endl;
            ch = false;
            drop();
            des();
        }
    }
    cout << point << endl;
    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            cout << field[i][j] << " ";
        }
        cout << endl;
    }
}
/*
5 5
# A - B #
# B - A #
# # - B #
# A B # #
# # # # #
3
0 1 L
0 3 L
0 1 R
*/