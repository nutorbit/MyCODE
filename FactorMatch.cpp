#include<bits/stdc++.h>

using namespace std;

vector<int> A, B;
int table[100001]={};

void init(){
    for(int i=1; i<=100000; i++){
        for(int j=1; i*j<=100000; j++){
            table[i*j] ++;
        }
    }
}

int main(){
    ios_base::sync_with_stdio(0);
    int N, M; cin >> N >> M;
    init();
    for(int i=0; i<N; i++){
        int tmp; cin >> tmp;
        A.push_back(table[tmp]);
    }
    for(int i=0; i<M; i++){
        int tmp; cin >> tmp;
        B.push_back(table[tmp]);
    }
    /*
    for(int i=0; i<20; i++){
        cout << i << " " << table[i] << endl;
    }
    */
    /*
    for(int i=0; i<N; i++){
        cout << A[i] << " " ;
    }
    cout << endl;
    for(int j=0; j<M; j++){
        cout << B[j] << " " ;
    }
    cout << endl;
    */
    int MAX = INT_MIN;
    for(int i=0; i+M-1<N; i++){
        int sum = 0;
        int si = i;
        int ss = -1;
        for(int j=0; j<M; j++){
            if(A[i+j] != B[j]){
                if(ss != -1){
                    i = ss;
                    break;
                }
                si = i+j-1;
                if(i-1 == si)si++;
                i = si;
                break;
            }
            if(B[0] == A[i+j] and j != 0){
                ss = i+j-1;
            }
            sum ++;
        }
        MAX = max(MAX, sum);
        if(MAX == M)break;
    }
    cout << MAX << endl;
}
