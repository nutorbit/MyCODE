#include <cstdio>
#include <string.h>
#include <cstring>
#include <cmath>
#include <iostream>

using namespace std;

char num[1<<21]={};
/*
int main()
{
    
    scanf("%s", num);
    int len = strlen(num), three=0,eleven1=0,eleven2=0;
    for(int i=0; i<len; i++)
    {
        three+=num[i];
        if(i%2==0) eleven1+=num[i];
        else if(i%2!=0) eleven2+=num[i];
    }
    int eleven = eleven1-eleven2;
    if(len%2==0)
    {
        if(eleven<0) eleven = int(fabs(eleven))%11;
        else if(eleven>0) eleven = 11-(eleven%11);}
        else if (len%2!=0)
        {
            if(eleven<0) eleven = eleven+11;
        }
        printf("%d %d\n", three%3, eleven%11);

}

*/


void mod3_11(){
    int mod3 = 0;
    int mod11 = 0;
    char ch;
    while (scanf("%c",&ch) && ch != '\n') {
        int digit = ch - '0';
        mod3 = (digit + mod3)%3;
        mod11 = (digit - mod11 + 11)%11;
    }
    printf("%d ", mod3);
    printf("%d\n", mod11);
}


int main(){
    mod3_11();
}

