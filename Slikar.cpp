#include<cstdio>
#include<queue>
#include<climits>

using namespace std;

typedef struct node{
    int i,j;
    int val;
}NODE;

char field[60][60]={};
int flush[60][60]={};
bool f[60][60]={};
bool ch[60][60]={};
queue<NODE> q;
int r,c;
int stx=999,sty=999;
int endx=999,endy=999;

void BB(int i,int j){
    NODE u,now;
    u.i = i;
    u.j = j;
    u.val = 0;
    q.push(u);
    f[u.i][u.j] = true;
    while(!q.empty()){
        now = q.front();
        q.pop();
        int dirx[9] = {1,0,0,-1,1,-1,-1,1};
        int diry[9] = {0,1,-1,0,1,-1,1, -1};
        for (int i=0; i<4; i++) {
            int tmpx=now.i+dirx[i];
            int tmpy=now.j+diry[i];
            if(tmpx < r && tmpx >= 0 && tmpy >= 0 && tmpy < c && (field[tmpx][tmpy]=='.' || field[tmpx][tmpy]=='S') && !f[tmpx][tmpy]){
                f[tmpx][tmpy] = true;
                u.i = tmpx;
                u.j = tmpy;
                u.val = now.val+1;
                flush[u.i][u.j] = min(u.val,flush[u.i][u.j]);
                q.push(u);
            }
        }
    }
}

int minn = 1000000;

void GO(){
    NODE u,now;
    u.i = stx;
    u.j = sty;
    u.val = 0;
    q.push(u);
    ch[u.i][u.j] = true;
    while (!q.empty()) {
        now = q.front();
        q.pop();
        if(now.i == endx && now.j == endy){
            if(now.val < minn)minn = now.val;
        }
        int dirx[9] = {1,0,0,-1};
        int diry[9] = {0,1,-1,0};
        for (int i=0; i<4; i++) {
            int tmpx=now.i+dirx[i];
            int tmpy=now.j+diry[i];
            if(tmpx < r && tmpx >= 0 && tmpy >= 0 && tmpy < c && (field[tmpx][tmpy]=='.' || field[tmpx][tmpy]=='D') && !ch[tmpx][tmpy] && now.val+1 < flush[tmpx][tmpy]){
                //printf("%d %d : %d\n\n",tmpx,tmpy,now.val);
                ch[tmpx][tmpy] = true;
                u.i = tmpx;
                u.j = tmpy;
                u.val = now.val+1;
                q.push(u);
            }
        }
    }
}

int main(){

    scanf("%d %d",&r,&c);
    for (int i=0; i<r; i++) {
        for (int j=0; j<c; j++) {
            scanf(" %c",&field[i][j]);
            flush[i][j] = 1000000;
            if(field[i][j] == 'S'){
                stx = i;
                sty = j;
            }
            if(field[i][j] == 'D'){
                endx = i;
                endy = j;
            }
        }
    }
    for (int i=0; i<r; i++) {
        for (int j=0; j<c; j++) {
            if(field[i][j] == '*'){
                BB(i,j);
                for (int k=0; k<r; k++) {
                    for (int l=0; l<c; l++) {
                        f[k][l] = false;
                    }
                }
            }
        }
    }
    GO();
    if(minn == 1000000 || minn == 0)printf("KAKTUS\n");
    else printf("%d\n",minn);
}
