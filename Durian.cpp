#include<cstdio>

using namespace std;

int cur[1000001]={};

int main(){

    int N;
    scanf("%d",&N);
    int st,end;
    int sum = 0;
    int maxx = -9999;
    for (int i=0; i<N; i++) {
        scanf("%d %d",&st,&end);
        cur[st]++;
        cur[end]--;
        if (end > maxx) {
            maxx = end;
        }
    }
    int maxx2=-999;
    for (int i=0; i<maxx; i++) {
        sum+=cur[i];
        if (maxx2 < sum) {
            maxx2 = sum;
        }
    }
    printf("%d",maxx2);
}
