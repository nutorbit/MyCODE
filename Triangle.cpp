#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)


int T,S,a=1;
bool ch;

int main(){
    cin >> T;
    while(T--){
        cin >> S;
        ch = false;
        cout << "Case #" << a++ << ":" << endl;
        FOR(A,1,(S+1)/2){
            double B = (pow(S-A,2)-pow(A,2))/(2*(S-A));
            if(ceil(B) == B and A <= B){
                double C = sqrt(pow((double)A,2)+(pow(B,2)));
                if(ceil(C) == C){
                    cout << A << " " << B << " " << C << endl;
                    ch = true;
                }
            }
        }
        if(!ch)cout << "-1" << endl;
    }
    
}
