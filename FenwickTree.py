class Fenwick:
    def __init__(self, N):
        self.arr = [0]*N**2
        self.size = N**2
    def Update(self, idx, data):
        while idx < self.size:
            self.arr[idx] += data
            idx += idx & -idx
    def RSCal(self, idx):
        v = 0
        while idx > 0:
            v += self.arr[idx] 
            idx -= idx & -idx
        return v
    def RS(self, a, b):
        return self.RSCal(b) - self.RSCal(a-1)

if __name__ == '__main__':
    fw = Fenwick(10)
    fw.Update(1, 10)
    fw.Update(2, 20)
    fw.Update(3, -30)
    print(fw.RS(1, 2))
