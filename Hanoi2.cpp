#include<iostream>

using namespace std;

void recur(int n,int start,int end){
    if (n==1) {
        cout << "move disk " << n << " from " << start << " to " << end << endl;
    }else{
        int temp = 2;
        recur(n-1,start,temp);
        cout << "move disk " << n << " from " << start << " to " << end << endl;
        recur(n-1,temp,end);
    }
}
int main(){
    int v;
    cin >> v;
    recur(v,1,3);
}
