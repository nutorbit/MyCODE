#include<bits/stdc++.h>

using namespace std;


template <class TYPE>
class fenwick {
    int size;
    int arr[1000001];
public:
    fenwick(int n){
        size = n;
    }
    void update(int idx,int val){
        while(idx <= size){
            arr[idx] -= val;
            idx += idx & (-idx);
        }
    }
    int sum(int a){
        int total = 0;
        while(a > 0){
            total += arr[a];
            a -= a & (-a);
        }
        return total;
    }
    int sum(int a,int b){
        return sum(b) - sum(a-1);
    }
};


int main(){
    ios :: sync_with_stdio(false);
    int N,P; cin >> N >> P;
    fenwick<int> fw(N);
    while(P--){
        int A,K,D,Q; cin >> A >> K >> D >> Q;
        int left = A-K-1,right = A + K-1;
        cout << left << " " << right << endl;
        if(left >= 0 and right < N){
            for (int i = left; i<=right; i++) {
                fw.update(i+1,D);
            }
        }else if(left <= 0 and right < N){
            for (int i=0; i<=right; i++) {
                fw.update(i+1,D);
            }
            for (int i=N+left-1; i<N; i++) {
                fw.update(i+1,D);
            }
            //cout << " x" << endl;
        }else if(left >=0 and right >= N){
            for (int i=left; i<=N-1; i++) {
                fw.update(i+1,D);
            }
            for (int i = 0; i<=right-N; i++) {
                fw.update(i+1,D);
            }
        }
        while(Q--){
            int query; cin >> query;
            if(10000+fw.sum(query,query) < 0)cout << "0 ";
            else cout << 10000+fw.sum(query,query) << " " ;
        }
        cout << endl;
    }
    
    
}
