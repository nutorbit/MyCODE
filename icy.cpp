#include<cstdio>

using namespace std;

int field[1001][1001]={};
bool ch[1001][1001]={};
int n;
int minn = 100001;
bool op = false;
int i=0,j=0;
int si=0,sj=0;

void T(int dis){
    ch[i][j] = true;
    printf("@T %d %d\n",i,j);
    if(i == n-1 && j == n-1){
        if(dis > minn)minn = dis;
        op = true;
        return;
    }
    else if(field[i-1][j] == 0 && i-1 >= 0 && !ch[i-1][j]){
        i--;
        T(dis+1);
        
    }else{
        
        return;
    }
}

void R(int dis){
    ch[i][j] = true;
    printf("@R %d %d\n",i,j);
    if(i == n-1 && j == n-1){
        if(dis > minn)minn = dis;
        op = true;
        return;
    }
    else if(field[i][j+1] == 0 && j+1 < n && !ch[i][j+1]){
        j++;
        R(dis+1);
        
    }else {
        
        return;
    }
}

void D(int dis){
    ch[i][j] = true;
    printf("@D %d %d\n",i,j);
    if(i == n-1 && j == n-1){
        if(dis > minn)minn = dis;
        op = true;
        return;
    }
    else if(field[i+1][j] == 0 && i+1 < n && !ch[i+1][j]){
        i++;
        D(dis+1);
        
    }else{
        
        return;
    }
}

void L(int dis){
    ch[i][j] = true;
    printf("@L %d %d\n",i,j);
    if(i == n-1 && j == n-1){
        if(dis > minn)minn = dis;
        op = true;
        return;
    }
    else if(field[i][j-1] == 0 && j-1 >= 0 && !ch[i][j-1]){
        j--;
        L(dis+1);
        
        //printf("123\n");
    }else{
        
        return;
    }
}

void travel(int dis){
    while(!op){
        ch[i][j] = true;
        printf("%d %d\n",i,j);
        si = i;
        sj = j;
        if(field[i+1][j] == 0 && i+1 < n && !ch[i+1][j]){
            //printf("D\n");
            D(dis);
        }
        if(field[i-1][j] == 0 && i-1 >= 0 && !ch[i-1][j]){
            //printf("T\n");
            T(dis);
        }
        if(field[i][j+1] == 0 && j+1 < n && !ch[i][j+1]){
            //printf("R\n");
            R(dis);
        }
        if(field[i][j-1] == 0 && j-1 >= 0 && !ch[i][j-1]){
            //printf("L\n");
            L(dis);
        }
        if(ch[i+1][j] && ch[i-1][j] && ch[i][j+1] && ch[i][j-1]){
            i = si;
            j = sj;
        }
    }
}

int main(){

    
    scanf("%d",&n);
    for (int k=0; k<n; k++) {
        for (int l=0; l<n; l++) {
            scanf("%d",&field[k][l]);
            if(field[k][l] == 1)ch[k][l] = true;
        }
    }
    travel(1);
}
