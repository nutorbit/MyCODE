#include<cstdio>
#include<algorithm>

using namespace std;

int arr[1000001]={};
int DP1[1000001]={};//left
int DP2[1000001]={};//right

int main(){
    int N;
    scanf("%d",&N);
    for (int i=0; i<N; i++) {
        scanf("%d",&arr[i]);
        DP1[i] = max(DP1[i-1],arr[i]);
    }
    for (int i=N-1; i>=0; i--) {
        DP2[i] = max(DP2[i+1],arr[i]);
    }
    for (int i=0; i<N; i++) {
        arr[i] = min(DP1[i],DP2[i+1]);
    }
    /*
    for (int i=0; i<N; i++) {
        printf("%d ",DP1[i]);
    }
    printf("\n");
    for (int i=0; i<N; i++) {
        printf("%d ",DP2[i]);
    }
    printf("\n");
     */
    for (int i=0; i<N-1; i++) {
        printf("%d ",arr[i]);
    }
}
