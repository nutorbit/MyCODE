#include<bits/stdc++.h>

using namespace std;
typedef long long ll;

int main(){
    ios_base::sync_with_stdio(0);
    ll N;
    while((cin >> N) and N > 0 ){
        vector<ll> A(N);
        vector<ll> curr;
        for(int i=0; i<N; i++)
            cin >> A[i];
        ll idx=0;
        ll MAX=-1;
        while(idx<N){
            if(curr.empty() or A[curr.back()] <= A[idx]){
                curr.push_back(idx++);
            }else{
                ll st = curr.back();
                curr.pop_back();
                ll area = A[st]*(curr.empty()?idx: idx-curr.back()-1);
                MAX = max(MAX, area);
            }
        }
        while(!curr.empty()){
            ll st = curr.back();
            curr.pop_back();
            ll area = A[st]*(curr.empty()?idx: idx-curr.back()-1);
            MAX = max(MAX, area);
        }
        cout << MAX << endl;
    }
    
}
