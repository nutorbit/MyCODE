#include<cstdio>

using namespace std;

typedef long long int ll;

ll cnr(ll c,ll r){
    if(r == 0 || r == c)return 1;
    else if(c<r || c < 0)return 0;
    else{
        return cnr(c-1,r)+cnr(c-1,r-1);
    }
}

int main(){
    
    ll n;
    scanf("%lld",&n);
    if(n == 2) printf("2\n");
    else if(n & 1) printf("%lld\n",cnr(n,(n/2)+1)*2);
    else printf("%lld\n",cnr(n,n/2));
    
    

}
