#include <bits/stdc++.h>

 using namespace std;
  typedef pair<long long int,long long int> ii;
  vector<ii> Store;
  bool Comp(const ii &a,const ii &b){
   return a.first*b.second > b.first*a.second;
  }
  main(){
   long long int N;
   scanf("%lld",&N);
   for(long long int i=0;i<N;i++){
    long long int A,B;
    scanf("%lld%lld",&A,&B);
    Store.push_back(make_pair(A,B));
   }
   sort(Store.begin(),Store.end(),Comp);
   long long int Des=0,Vol=0;
   long double Max = 0.00;
   for(long long int i=0;i<N;i++){
//    printf("%d %d\n",Store[i].first,Store[i].second);
    Des +=Store[i].first;
    long long int T = Vol;
    Vol = (Vol<Store[i].second)? Store[i].second : Vol;
    long double tmp = (long double)Des/(long double)Vol;
    if(tmp>Max){
        Max = tmp;
    }
    else{
         Vol = T;
         Des -= Store[i].first;
        break;
    }
//    printf("%lf\n",Max);
   }
   printf("%lld %lld\n",Des,Vol);
  }
