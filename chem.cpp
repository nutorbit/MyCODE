#include<bits/stdc++.h>

using namespace std;

// struct container_hash {
//     size_t operator()(vector<int> const& c) const {
//         size_t seed = c.size();
//         for(auto& i: c) {
//             seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
//         }
//         return seed;
//     }
// };

// unordered_map<vector<int>, int, container_hash> M;
vector<uint32_t> A[100005];
vector<uint32_t> t, need;
unordered_map<uint32_t, uint32_t> HASH;

int h(vector<uint32_t> c){
    uint32_t seed = 0;
    for(auto v: c)
        seed ^= hash<uint32_t>{}(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

int main(){
    ios_base::sync_with_stdio(0);
    uint32_t N, P, inp;  cin >> N >> P;
    for(uint32_t i=0; i<N; i++){
        for(uint32_t j=0; j<P; j++){
            cin >> inp;
            A[i].push_back(inp);
            t.push_back(inp);
        }
        // cout << h(t) << endl;
        HASH.insert({h(t), i});
        t.clear();
    }
    for(uint32_t i=0; i<P; i++){
        cin >> inp;
        t.push_back(inp);
    }
    auto it = HASH.find(h(t));
    if(it != HASH.end()){
        cout << it->second+1 << endl;
        return 0;
    }
    for(uint32_t i=0; i<N; i++){
        for(uint32_t j=0; j<P; j++)
            need.push_back(fabs(t[j]-A[i][j]));
        auto it = HASH.find(h(need));
        if(it != HASH.end() and it->second != i and it->second > i){
            cout << i+1 << " " << it->second+1 << endl;
            return 0;
        }
        need.clear();
    }
    cout << "NO" << endl;
}