#include<cstdio>
#include<cstring>
#include<string.h>

using namespace std;

typedef struct INFO{
    char txt[1001];
}INFO;

int ans[1001]={};
INFO in[1001]={};
int count = 0;
bool ch = false;

int main(){
    
    int N;
    scanf("%d",&N);
    for (int i=0; i<N; i++) {
        scanf(" %s",in[i].txt);
        //printf("%lu\n",strlen(in[i].txt));
    }
    for (int i=0; i<N; i++) {
        count = 0;
        ch = false;
        for (int j=0; j<strlen(in[i].txt); j++) {
            if(in[i].txt[j] == 'P'){
                count++;
            }
            
            if(in[i].txt[j] == 'X'){
                ans[i] = 1;
                break;
            }
            else if(in[i].txt[j] == 'T'){
                ans[i] = 2;
                ch = true;
            }
            else if(in[i].txt[j] == '-' && !ch){
                //printf("\n%d %d\n",i+1,j);
                ans[i] = 3;
            }
            else if(count == strlen(in[i].txt)){
                ans[i] = 4;
                break;
            }
        }
    }
    for (int i=0; i<N; i++) {
        if(ans[i] == 1){
            printf("Case #%d: No - Runtime error\n",i+1);
        }
        else if(ans[i] == 2){
            printf("Case #%d: No - Time limit exceeded\n",i+1);
        }
        else if(ans[i] == 3){
            printf("Case #%d: No - Wrong answer\n",i+1);
        }
        else if(ans[i] == 4){
            printf("Case #%d: Yes\n",i+1);
        }
    }
}


