#include<bits/stdc++.h>

using namespace std;

struct node{
	int x, y, r;
};

vector<node> v;
int s = 0;

double distFrom(node a, node b){
	return sqrt(pow(a.x-b.x, 2)+pow(a.y-b.y, 2));
}

bool comp(const node &a, const node &b){
	if(a.x == b.x)
		return a.y < b.y;
	return a.x < b.x;
}

int main(){
	int N; cin >> N;
	for(int i=0; i<N; i++){
		node u;
		cin >> u.x >> u.y >> u.r;
		v.push_back(u);
	}
	sort(v.begin(), v.end(), comp);
	/* // debug
	for(auto it: v){
		cout << it.x << " " << it.y << endl;
	}
	*/
	for(int i=0; i<N; i++){
		for(int j=i+1; j<N; j++){
			if(distFrom(v[i], v[j]) < v[i].r + v[j].r){
				s += 1;
			}else if((v[i].x == v[j].x || v[i].y == v[j].y) && distFrom(v[i], v[j]) >= 20){
				break;
			}
		}
	}
	cout << s << endl;
}