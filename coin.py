'''coin V2'''
def main(amount, dynamic):
    '''Buttom-up method '''
    for i in range(1, amount+1):
        for coin in [1, 2, 5, 10]:
            if dynamic[i-coin]:
                dynamic[i] += dynamic[i-coin]
    print('method =', dynamic[amount])
main(int(input()), [1] + [0]*10000)
