from math import fabs
i = lambda:map(int,input().split())
n,m,k = i()
arr = list(i())
ans = float('inf')
for j in range(n):
    if arr[j] != 0 and arr[j] <= k  :
        ans = min(fabs(j+1-m)*10,ans)
print(int(ans))

