#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef struct info{
    int x,y,time,st;
    bool j,b,p;
}INFO;

int N,M,sx,sy,ex,ey;
char board[505][505]={};
bool run[505][505][10]={};
queue<INFO> q;
int MIN = INT_MAX;
INFO u,now;

void travel(){
    u.x = sx,u.y = sy,u.time = 0,u.st = 0,u.j = u.b = u.p = false;
    run[u.x][u.y][u.st] = true;
    q.push(u);
    while (!q.empty()) {
        now = q.front();
        q.pop();
        int rx[] = {1,-1,0,0};
        int ry[] = {0,0,-1,1};
        //cout << now.x << " " << now.y << " " << now.time << " st: " << now.st << endl;
        if(now.x == ex && now.y == ey){
            MIN = min(MIN,now.time);
            //cout << "OK " << now.time << endl;
        }
        if(board[now.x][now.y] == 'j')now.j = true,now.st |= 1;
        if(board[now.x][now.y] == 'b')now.b = true,now.st |= 1 << 1;
        if(board[now.x][now.y] == 'p')now.p = true,now.st |= 1 << 2;
        FOR(i,0,4){
            int tx = rx[i]+now.x;
            int ty = ry[i]+now.y;
            if(tx >= 0 && ty >= 0 && tx < N && ty < M && board[tx][ty] != '#' && !run[tx][ty][now.st]){
                
                if(board[tx][ty] == 'J' && !now.j)continue;
                
                if(board[tx][ty] == 'B' && !now.b)continue;
                
                if(board[tx][ty] == 'P' && !now.p)continue;
                
                u.x = tx,u.y = ty,u.time = now.time+1,u.st = now.st,u.j = now.j,u.b = now.b,u.p = now.p;
                run[u.x][u.y][u.st] = true;
                q.push(u);
            }
        }
    }
}

int main(){
    scanf("%d %d",&N,&M);
    FOR(i,0,N){
        FOR(j,0,M){
            scanf(" %c",&board[i][j]);
            if(board[i][j] == 'S')sx = i,sy = j;
            if(board[i][j] == 'E')ex = i,ey = j;
        }
    }
    //printf("st %d %d\nend %d %d\n",sx,sy,ex,ey);
    travel();
    if(MIN == INT_MAX)printf("-1\n");
    else printf("%d\n",MIN);

}
