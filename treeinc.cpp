#include<climits>
#include<cstdio>
#include<vector>
#include<queue>
#include<algorithm>
#include<stack>

using namespace std;

typedef struct node{
    int r,i;
}NODE;

vector<int> vec[300001];
queue<NODE> q;
bool visit[300001]={};
int maxx = INT_MIN;
int DP[300001]={};

void BFS(int i){
    NODE now,u;
    u.i = i;
    u.r = 1;
    q.push(u);
    visit[i] = true;
    while(!q.empty()){
        now = q.front();
        q.pop();
        for (int i=0; i<vec[now.i].size(); i++) {
            if(!visit[vec[now.i][i]]){
                visit[vec[now.i][i]] = true;
                u.i = vec[now.i][i];
                u.r = now.r+1;
                if(u.r > maxx)maxx = u.r;
                q.push(u);
            }
        }
    }
}

void DFS(int i,int r){
    DP[i] = r;
    for (int l=0; l<vec[i].size(); l++) {
            //printf("%d->%d :%d\n",i,vec[i][l],r+1);
        if(r+1 > DP[vec[i][l]]){
            if(r+1 > maxx)maxx = r+1;
            visit[vec[i][l]] = true;
            DFS(vec[i][l],r+1);
        
        }
    }
}

int main(){
    int N;
    scanf("%d",&N);
    int a,b;
    for (int i=0; i<N-1; i++) {
        scanf("%d %d",&a,&b);
        vec[min(a,b)].push_back(max(a,b));
    }
    for (int i=1; i<=N; i++) {
        if(!visit[i]){
            DFS(i,1);
        }
    }
    printf("%d\n",maxx);
    
}
