'''sortList'''
import json
def forsort(item):
    '''sort ไส้ในของแต่ละ list '''
    if isinstance(item, list):
        item[:] = sorted(item, key=forsort)
        return False, -len(item)
    else:
        return True, -item

def main():
    '''main'''
    rawlist = input()
    rawlist = json.loads(rawlist)
    print(sorted(rawlist, key=forsort))
main()
