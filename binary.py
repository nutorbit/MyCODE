'''binary'''
def binary(value):
    '''convert binary'''
    if value == 1:
        return 1
    return str(binary(value>>1)) + str(value%2)

def main():
    '''main'''
    num = int(input())
    print(binary(num))
main()
