#include<bits/stdc++.h>

using namespace std;

int N, M;

struct node{
    int id, army;
};

node A[100001]={};

int root(int id){
    while(A[id].id != id) id = A[A[id].id].id;
    return id;
}

void Add(int a, int b){
    int x = root(a);
    int y = root(b);
    A[y].id = A[x].id;
    A[x].army += A[y].army/2;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin >> N >> M;
    for(int i=0; i<N; i++){
        cin >> A[i].army;
        A[i].id = i;
    }
    for(int i=0; i<M; i++){
        int a, b; cin >> a >> b;
        a -= 1, b-=1;
        if(root(a) == root(b)){
            printf("-1\n");
        }
        else if(A[root(a)].army == A[root(b)].army){
            if(root(a) > root(b)){
                Add(b, a);
                printf("%d\n", root(b)+1);
            }else{
                Add(a, b);
                printf("%d\n", root(a)+1);
            }
        }
        else if(A[root(a)].army > A[root(b)].army){
            Add(a, b);
            printf("%d\n", root(a)+1);
        }
        else if(A[root(a)].army < A[root(b)].army){
            Add(b, a);
            printf("%d\n", root(b)+1);
        }
    }
}
