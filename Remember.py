import math

class FlightDataRecorder:
    def getDistance(self, heading, distance):
        x, y = 0, 0
        for i in range(len(heading)):
            x += math.cos(heading[i]*math.pi/180)*distance[i]
            y += math.sin(heading[i]*math.pi/180)*distance[i]
        return (x**2 + y**2)**0.5
x = FlightDataRecorder()
print(x.getDistance([90, 0], [3, 4]))
