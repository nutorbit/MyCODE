#include<cstdio>
#include<algorithm>

using namespace std;

bool comp(int &a,int &b){
    return a<b;
}

int t[1000001]={};
int DP[1000001]={};

int main(){
    int N;
    scanf("%d",&N);
    for (int i=0; i<N; i++) {
        scanf("%d",&t[i]);
        DP[i] = 1;
    }
    sort(t,t+N,comp);
    int c = 0;
    for (int i=1; i<N; i++) {
        for (int j=0; j<i; j++) {
            if(t[i] > t[j] && DP[i] < DP[j]+1)DP[i] = DP[j]+1;
        }
    }
    printf("%d\n",N-DP[N-1]+1);
}
