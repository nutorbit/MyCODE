def mazeEscape(c, r, b):
    m = 1e7
    g = [[0]*c for _ in range(r)]
    for i, j in zip(b[1::2], b[::2]):
        g[i][j] = 1
    q = [(0, 0, 0)]
    g[0][0] = 1
    while q:
        a, d, p = q.pop(0)
        x = [1, -1, 0,  0]
        y = [0,  0, 1, -1]
        if a == r-1 and d == c-1:
            m = min(m, p)
        for i in range(4):
            k = a+x[i]
            l = d+y[i]
            if 0 <= k < r and 0 <= l < c and not g[k][l]:
                g[k][l] = 1
                q += [(k, l, p+1)]
    return m if m != 1e7 else -1

mazeEscape(4, 4, [1, 0, 1, 1, 2, 2, 3, 1])