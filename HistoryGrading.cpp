#include<cstdio>
#include<algorithm>

using namespace std;

int arr[101]={};
int dp[101][101]={};
int h[101]={};

int lcs(int m, int n ){
    
    for (int i=1; i<=m; i++) {
        for (int j=1; j<=n; j++) {
            if(h[i-1] == arr[j-1])
                dp[i][j] = dp[i-1][j-1]+1;
            else
                dp[i][j] = max(dp[i-1][j],dp[i][j-1]);
        }
    }
    return dp[m][n];
    
}

int main(){
    int n,a,b;
    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%d",&a);
        h[a-1] = i;
    }
    
    for (int i=0; ; i++) {
        for (int j=0; j<n; j++) {
            if(scanf("%d",&b) != 1)return 0;
            arr[b-1] = j;
            
        }
        int maxx = lcs(n,n);
        printf("%d\n",maxx);
    }
    
    
    
}
