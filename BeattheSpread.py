n = int(input())
for i in range(n):
    A,B = map(int,input().split())
    if A >= B and (A+B)%2 == 0:
        x = (A+B)/2
        y = (A-B)/2
        print(int(x),int(y))
    else:
        print("impossible")
