a = list(map(int, input().split()))
n = len(a)
m = [[float('-inf')]*n for _ in range(n)]

for i in range(n):
    for size in range(n):
        for j in range(i, i+size+1):
            if size == 0:
                m[i][j] = a[i]
            elif j < n and j-1 >= i:
                m[i][j] = max(m[i][j-1], a[j])

print(m[2][2])
print(m)

