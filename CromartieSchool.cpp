#include <cstdio>

using namespace std;

short code[65][65] = {};
int maxi, maxj, min = 10000, max = 0;
char c;

int take(int i, int j){
    code[i][j] = 2;
    if (code[i + 1][j] == 1){
        take(i + 1, j);
    }
    if (code[i][j - 1] == 1){
        take(i, j - 1);
    }
    if (code[i - 1][j] == 1){
        take(i - 1, j);
    }
    if (code[i][j + 1] == 1){
        take(i, j + 1);
    }
    return 0;
}

int re(int i, int j, int size, int water){
    int temp;
    temp = water;
    for (int x = i; x <= i + size; x++){
        for (int y = j; y <= j + size; y++)
            if (code[x][y] == 1){
                temp++;
                take(x, y);
            }else if (code[x][y] == 0){
                if ((size > max) || (size == max && water < min)){
                    max = size;
                    min = water;
                }
                return 0;
            }
    }
    re(i, j, size + 1, temp);
    return 0;
}

void clear(){
    for (int i = 0; i < maxi; i++){
        for (int j = 0; j < maxj; j++){
            if (code[i][j] == 2){
                code[i][j] = 1;
            }
        }
    }
}

int main(){

    scanf("%d %d", &maxj, &maxi);
    getchar();
    for (int i = 0; i < maxi; i++){
        for (int j = 0; j < maxj; j++){
            scanf("%c", &c);
            if (c == 'S'){
                code[i][j] = -2;
            }else if (c == 'P'){
                code[i][j] = 1;
            }
        }
        getchar();
    }
    for (int i = 0; i < maxi; i++){
        for (int j = 0; j < maxj; j++){
            if (code[i][j] == -2 || code[i][j] == 1){
                re(i, j, 0, 0);
                clear();
            }
        }
    }
    printf("%d ", max * max);
    if (min == 10000){
        printf("0\n");
    }else{
        printf("%d\n", min);
    }
    return 0;
}
