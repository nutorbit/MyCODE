#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int T,N,K,tmp;
vector<int> arr(1005);
vector<int> sum(1005);
set<int> s;
set<int>::iterator it;

//http://stackoverflow.com/questions/31113993/maximum-subarray-sum-modulo-m

int main(){

    cin >> T;
    FOR(i,1,T+1){
        cin >> N >> K ;
        FOR(j,0,N)cin >> arr[j],arr[j]%=K;
        sort(arr.begin(),arr.end(),greater<int>());
        sum[0] = arr[0];
        s.insert(sum[0]);
        int result = sum[0];
        FOR(k,1,N){
            sum[k] = sum[k-1] + arr[k];
            sum[k] %= K;
            it = s.lower_bound(sum[k]);
            //cout << *it << endl;
            result = max((sum[k] - *it + K)%K,result);
            s.insert(sum[k]);
        }
        cout << "Case #" << i << ": " << result << endl;
        arr.clear();s.clear();sum.clear();
    }

}
