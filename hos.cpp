#include<iostream>
#include<vector>
#include<cstdio>
#include<algorithm>

using namespace std;

typedef struct node{
    int type;
    char name;
    int q;
}NODE;
NODE n;

bool compare(const NODE &a,const NODE &b ){
    return a.q < b.q;
}

int main(){
    vector<NODE> q1;
    vector<NODE> q0;
    int i=0;
    char tmp;
    while (true) {
        scanf("%d%c%d%c",&n.type,&n.name,&n.q,&tmp);
        if (n.type == 1) {
            q1.push_back(n);
        }else{
            q0.push_back(n);
        }
        if (tmp == '\n') {
            break;
        }
    }
    sort(q1.begin(),q1.end(),compare);
    sort(q0.begin(),q0.end(),compare);
    
    for (int i=0; i<q1.size(); i++) {
        cout << q1[i].name << " ";
    }
    for (int i=0; i<q0.size(); i++) {
        cout << q0[i].name << " ";
    }
}