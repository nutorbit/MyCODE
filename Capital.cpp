#include<cstdio>
#include<vector>
#include<queue>
#include<algorithm>

using namespace std;

typedef struct node{
    int p;
    int range;
}node;

vector<node> vec[100001];
queue<int> q;
bool visit[100001]={};
int dis[100001];

int main(){
    
    int N;
    scanf("%d",&N);
    int a,b,c;
    int maxi = -999;
    for (int i=0; i<N-1; i++) {
        scanf("%d %d %d",&a,&b,&c);
        node n;
        n.p = b;
        n.range = c;
        vec[a].push_back(n);
        if (maxi < b) {
            maxi = b;
        }
    }
    q.push(1);
    visit[1] = true;
    while(!q.empty()){
        int f = q.front();
        q.pop();
        for (int i=0; i<vec[f].size(); i++) {
            if (visit[vec[f][i].p]==false) {
                dis[vec[f][i].p] = max(dis[vec[f][i].p],dis[f]+vec[f][i].range);
                q.push(vec[f][i].p);
                visit[vec[f][i].p]=true;
            }
        }
    }
    int maxx = *max_element(dis,dis+maxi+1);
    printf("%d",maxx);
}
