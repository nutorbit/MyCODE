#include<bits/stdc++.h>

using namespace std;

vector<int> A(1<<18);

inline int sum(int N){
	int ans = 0, curr = 0;
	for(int i=0; i<N; i++){
		curr = max(0, curr+A[i]);
		ans = max(ans, curr);
	}
	return ans;
}

int main(){
	int N; cin >> N;
	int mm = INT_MIN;
	for(int i=0; i<N; i++){
		cin >> A[i];
		mm = max(mm, A[i]);
	}
	int ma = sum(N);
	int wr = 0;
	for(int i=0; i<N; i++){
		wr += A[i];
		A[i] = -A[i];
	}
	wr += sum(N);
	
	cout << (mm>=0?max(ma, wr):mm) << endl;
}