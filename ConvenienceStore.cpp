#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int N,M,in;
int DP[50001]={};

int main(){
	scanf("%d %d",&N,&M);
	FOR(i,0,N){
		scanf("%d",&in);
		DP[in] = 1;
	}
	DP[0] = 1;
	FOR(i,1,M+1){
		int tmp = 0;
		//cout << i << " :::: " << endl;
		for(int j=0;i&1?j<=i/2:j<i/2;j++){
			if(DP[j]!=0 and DP[i-j]!=0){
				tmp+=1;
				//cout << j << " " << i-j << endl;
			}
		}
		DP[i] = 1;
		DP[i] += tmp;
		DP[i] %= 1000007;
		//cout << " ans " << DP[i] << endl;
	}
	cout << DP[M] << endl;
}
