#include<cstdio>
#include<iostream>

char i='i',v='v',x='x',l='l',c='c';

using namespace std;

void recur(int in){
    if (in >= 90 && in < 100) {
        cout << l << c;
        return recur(in-90);
    }
    if (in >= 40 && in < 50) {
        cout << x << l;
        return recur(in-40);
    }
    if (in == 9) {
        cout << i << x;
        return recur(in-9);
    }
    if(in == 4){
        cout << i << v;
        return recur(in-4);
    }
    if(in>=1 && in <5){
        cout << i;
        return recur(in-1);
    }
    if(in>=5 && in <10){
        cout << v;
        return recur(in-5);
    }
    if(in>=10 && in <50){
        cout << x;
        return recur(in-10);
    }
    if(in >=50 && in <100){
        cout << l;
        return recur(in-50);
    }
    if(in >=100 && in <250){
        cout << c;
        return recur(in-100);
    }
}

int main(){
    int in;
    cin >> in;
    recur(in);
    return 0;
}
