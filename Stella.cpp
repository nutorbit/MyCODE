#include <bits/stdc++.h>

using namespace std;
char c;
long long int M;
typedef struct{
    long long int m;
    int t;
    int i;
}CO;
typedef struct{
    long long int m;
    int i;
    int c;
}MO;
bool CompCO(const CO &a,const CO &b){
    return a.t > b.t;
}
bool CompMO(const MO &a,const MO &b){
    if(a.m==b.m){
        return a.i > b.i;
    }
    return a.m  < b.m;
}
int Dat[100001] = {};
int rad[100001];
vector<MO> N;
vector<CO> R;
int main(){
    long long int Ans = 0;
    int K,i;
    scanf("%d",&K);
    for(i=0;i<K;i++){
        while(!R.empty()&&R[0].t==i){
            CO T = R[0];
            pop_heap(R.begin(),R.end(),CompCO);
            R.pop_back();
            if(Dat[T.i]!=-1){
                T.t += rad[T.i];
                Dat[T.i]++;
                if(T.m/2!=0){
                    CO u = {T.m/2,T.t,T.i};
                    R.push_back(u);
                    push_heap(R.begin(),R.end(),CompCO);
                    MO x = {T.m/2,T.i,Dat[T.i]};
                    N.push_back(x);
                    push_heap(N.begin(),N.end(),CompMO);
                }
            }
        }
        scanf(" %c",&c);
        if(c == 'c'){
            scanf(" %c%lld",&c,&M);
            if(c=='r'){
                scanf("%d",&rad[i]);
                CO u = {M,i+rad[i],i};
                R.push_back(u);
                push_heap(R.begin(),R.end(),CompCO);
                MO x = {M,i,Dat[i]};
                N.push_back(x);
                push_heap(N.begin(),N.end(),CompMO);
            }else if(c=='n'){
                MO x = {M,i,Dat[i]};
                N.push_back(x);
                push_heap(N.begin(),N.end(),CompMO);
            }
        }else if(c =='u'){
            while(!N.empty()&&N[0].c!=Dat[N[0].i]){
                pop_heap(N.begin(),N.end(),CompMO);
                N.pop_back();
            }
            if(!N.empty()){
                Ans += N[0].m;
                Dat[N[0].i] = -1;
                pop_heap(N.begin(),N.end(),CompMO);
                N.pop_back();
            }
        }
    }
    int Count = 0;
    int S = N.size();
    for(i=0;i<S;++i){
        if(N[i].c==Dat[N[i].i]&&N[i].m!=0){
            Count++;
        }
    }
    printf("%lld\n",Ans);
    printf("%d",Count);
}
