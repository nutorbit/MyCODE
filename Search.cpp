#include <iostream>
#include <algorithm>
using namespace std;

int main(){
      int count;
      int arr[1001];
      int search;
      int low=0;
      bool find =false;
      cin >> count;
      int hi=count-1;
      for (int i=0 ; i<count ; i++){
            cin >> arr[i];
      }
      sort(arr,arr+count);
      cin >> search;
      while (low <= hi) {
            int mid = (low + hi)/2;
            if (search == arr[mid]) {
                  find = true;
                  break;
            }else if(search < arr[mid]){
                   hi = mid - 1;
            }else if(search >= arr[mid]){
                   low = mid + 1;
            }
      }
      if (find) {
            cout << search << " IS FOUND" << endl;
      }else{
            cout << search << " IS NOT FOUND" << endl;
      }

}
