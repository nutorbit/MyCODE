#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string.h>

using namespace std;

int arr[1<<15]={};
int DP[1<<15]={};
int f,s;
int n;
bool ch = false;

void recur(int i){
    if(i >= n+1){
        int tmp = 0;
        for (int i=0; i<n; i++) {
            if(arr[i] == 0)tmp++;
            //printf("%d ",arr[i]);
        }
        if(tmp == n){
            ch = true;
        }
        //printf("\n");
        return;
    }
    if(arr[i] == 0){
        recur(i+1);
    }
    if(arr[i]-1 < 0 && arr[i+1]-1 < 0)return;
    else if(arr[i]-1 >= 0 && arr[i+1]-1 >= 0){
        arr[i]--;
        arr[i+1]--;
        if(arr[i] == 0)
            DP[i]++;
        if(arr[i+1] == 0)
            DP[i+1]++;
        recur(i+1);
        //printf("ddd\n");
        arr[i]++;
        arr[i+1]++;
    }
    if(arr[i]-2 < 0)return;
    else if(arr[i]-2 >=0){
        arr[i]-=2;
        if(arr[i] == 0)
            DP[i]++;
        recur(i+1);
        arr[i]+=2;
        
    }
    
}

int main(){

    
    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%d",&arr[i]);
    }
    if(n == 1 && arr[0] == 1){
        printf("YES\n");
        return 0;
    }
    recur(0);
    if(ch)printf("YES\n");
    else printf("NO\n");
    /*for (int i=0; i<n; i++) {
        printf("%d\n",DP[i]);
    }*/
}

