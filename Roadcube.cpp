
#include <bits/stdc++.h>

using namespace std;
int Road[101][10001];
int DP[101][10001];
int main(){
    int N,M,K;
    scanf("%d%d%d",&N,&M,&K);
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            scanf("%d",&Road[i][j]);
        }
    }
    for(int i=0;i<M;i++){
        DP[N-1][i] = Road[N-1][i];
    }
    for(int i=N-2;i>=0;i--){
        int rel = -1;
        for(int j=0;j<M;j++){
            int x = j-K;
            int y = j+K;
            if(x<0)x=0;
            if(y>=M)y = M-1;
            if(rel==-1||rel<x){
                for(int k=x;k<=y;k++){
                    DP[i][j] = max(DP[i][j],DP[i+1][k]);
                    if(DP[i][j] == DP[i+1][k])rel = k;
                }
                DP[i][j] += Road[i][j];
            }
            else{
                if(DP[i+1][y]>=DP[i+1][rel])rel = y;
                DP[i][j] = DP[i+1][rel]+Road[i][j];
            }
        }
    }
    int maxx = 0;
    for(int i=0;i<M;i++){
        maxx = max(DP[0][i],maxx);
    }
    printf("%d",maxx);
}
