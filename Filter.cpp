#include<iostream>

using namespace std;

int sq[10001]={};

int main(){
    int W,H,N;
    cin >> W >> H >> N;
    for(int i=0;i<N;i++){
        int a,b;
        cin >> a >> b;
        while(b != 0 && a < W){
            sq[a]++;
            a++;
            b--;
        }
    }
    int ans1=0,ans2=0;
    for(int i=0;i<W;i++){
        if(sq[i] == 1){
            ans2 += 1;
        }else if(sq[i] == 0){
            ans1 += 1;
        }
        cout << sq[i];
    }
    cout << endl << ans1*H << " " << ans2*H << endl;
}

