#include<bits/stdc++.h>

using namespace std;

int main(){
    int N, K; cin >> N >> K;
    vector<int > A;
    int ans = -1;
    for(int i=0; i<N; i++){
        int in; cin >> in;
        if(!A.empty()){
            vector<int>::iterator idx = lower_bound(A.begin(), A.end(), in-K);
            int len = A.size();
            int posi = idx-A.begin();
            ans = max(ans ,len-posi);
        }
        A.push_back(in);
    }
    cout << ans << endl;

}
