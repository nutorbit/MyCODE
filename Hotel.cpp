#include<bits/stdc++.h>

using namespace std;

int main(){
    int N; cin >> N;
    vector<int> DP(N+1);
    for(int i=1; i<=N; i++){
        DP[i] = DP[i-1] + 500;
        DP[i] = min(DP[i], i-2>=0?DP[i-2]+800:800);
        DP[i] = min(DP[i], i-5>=0?DP[i-5]+1500:1500);
        DP[i] = min(DP[i], i-15>=0?DP[i-15]+3000:3000);
    }
    cout << DP[N] << endl;
}
