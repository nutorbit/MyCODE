#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

typedef long long int ll;

bool comp(const ll &a,const ll &b){
    return a<b;
}

ll arr[100001]={};

ll GCD(ll x,ll y){
    while((x%=y) && (y%=x));
    return x+y;
}

int main(){
    ll R,G;
    int index=0;
    scanf("%lld %lld",&R,&G);
    ll gcd = GCD(R,G);
    ll s = sqrt(gcd);
    for (int i=1; i<=s; i++) {
        if(gcd%i==0){
            arr[index++] = i;
            arr[index++] = gcd/i;
        }
    }
    /*
    for (int i=0; i<index; i++) {
        printf("%lld ",arr[i]);
    }
    */
    sort(arr,arr+index,comp);
    printf("%lld %lld %lld\n",arr[0],R/arr[0],G/arr[0]);
    for (int i=1; i<index; i++) {
        if(arr[i] != arr[i-1])
            printf("%lld %lld %lld\n",arr[i],R/arr[i],G/arr[i]);
    }
    
    /*
    for (int i=0; i<10000; i++) {
        if(ch[i])printf("%d ",i);
    }
     */
    
}
