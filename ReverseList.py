'''ReverseList'''
def main():
    '''main'''
    number = int(input())
    array = []
    for _ in range(number):
        val = input()
        array += [val]
    for idx in range(number):
        print(array[-idx-1])
main()
