#include<cstdio>
#include<algorithm>
using namespace std;

int P[10001]={};
int WW[100001]={};
int DP[1001][1001]={};

int recur(int i,int w){
    if(DP[i][w] > 0){
        return DP[i][w];
    }
    if (i == 0 && w == 0) {
        DP[i][w] = 0;
    }
    if(WW[i-1] > w){
        DP[i][w] = recur(i-1,w);
    }
    if(WW[i-1] <= w){
        DP[i][w] = max(P[i-1]+recur(i-1,w-WW[i-1]),recur(i-1,w));
    }
    return DP[i][w];
    
}

int main(){

    int N,W;
    scanf("%d %d",&N,&W);
    for (int i=0; i<N; i++) {
        scanf("%d",&P[i]);
    }
    for (int i=0; i<N; i++) {
        scanf("%d",&WW[i]);
    }
    int maxx = recur(N,W);
}
