#include<bits/stdc++.h>

using namespace std;

int fac[10010]={};
const int mod = 49999;

int f(int i){
    if(i<=0)
        return 1;
    return fac[i] = (i*f(i-1))%mod;
}

int main(){
    int N; cin >> N;
    f(10001);
    vector<int> A(N);
    for(int i=0; i<N; i++)
        cin >> A[i];
    sort(A.begin(), A.end(), greater<int>());
    int sum = 0;
    for(int i=0; i<N; i++){
        sum += (N-(i*2)) * fac[N-1] * A[i];
    }
    cout << sum << endl;

}
