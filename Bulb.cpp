#include <cstdio>
#include <algorithm>

using namespace std;

int n;
int runbit(int posi,int k) {
    
    int xxx=0;
    if(posi == 0) {
        xxx+=1<<posi;
        xxx+=1<<(posi+1);
    } else if(posi == n-1)  {
        xxx+=1<<posi;
        xxx+=1<<(posi-1);
    } else {
        xxx+=1<<posi;
        xxx+=1<<(posi-1);
        xxx+=1<<(posi+1);
    }
    if(posi == n-1) {
        if(k==0)
            return 0;
        if((k^xxx)==0)
            return 1;
        else
            return 999999999;
    }
    //printf("posi %d  %d xor %d = %d\n",posi,k,xxx,k^xxx);
    return min(runbit(posi+1,k),1+runbit(posi+1,(k^xxx)));
}
int main() {
    
    int tmp,k=0;
    scanf("%d",&n);
    for(int i=0;i<n;i++) {
        scanf("%d ",&tmp);
        k=k*2+tmp;
    }
    if(n==1 && k==1){
        printf("1\n");
        return 0;
    }
    tmp=runbit(0,k);
    if(tmp != 999999999)
        printf("%d\n",tmp);
    else
        printf("-1\n");
    
    return 0;
}
