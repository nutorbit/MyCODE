#include<bits/stdc++.h>

typedef long long ll;

using namespace std;

int main(){
    int N; cin >> N;
    vector<ll> A, B;
    for(int i=0; i<N; i++){
        ll in; cin >> in;
        A.push_back(in);
    }
    for(int i=0; i<N; i++){
        ll in; cin >> in;
        B.push_back(in);
    }
    ll ans = 0;
    sort(A.begin(), A.end());
    sort(B.begin(), B.end());
    for(int i=0; i<N; i++){
        ans += fabs(A[i]-B[i]);
    }
    cout << ans << endl;
}
