'''60070134 60070140'''
def main(score, hist):
    '''auka auka auka'''
    for val in score:
        hist[int(val+1) if int(val) != val else int(val) if int(val) != 0 else int(val)+1] += 1
    print('\n'.join('{}-{}:{}'.format(index-1, index, '|'*hist[index]) for index in range(1, 11)))
main([float(val) for val in input().split(',')], [0]*20)
