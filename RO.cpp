#include<bits/stdc++.h>

using namespace std;

typedef double ll;

char arr[1<<21]={};
ll dp[1<<21]={};
map<ll, ll > m;
set<ll > vis;

int main(){
	ios_base::sync_with_stdio(false);
    int n, k; cin >> n >> k;
    cin >> arr;
    ll MAX = 0;
    dp[0] = (arr[0]=='R'?k:-1);
    for(int i=1; i<n; i++){
        dp[i] = dp[i-1] + (arr[i]=='R'?k:-1);
        if(dp[i] == 0) MAX = max(MAX, (ll)i+1);
        else if(vis.find(dp[i]) == vis.end()) m[dp[i]] = i, vis.insert(dp[i]);
        MAX = max(MAX, i - m[dp[i]]);
        // cout << i << " " << m[dp[i]] << endl;
        // m[dp[i]] = i;
    }
    /*
    for(int i=0; i<n; i++){
        cout << i << " ";
        cout << arr[i] << " ";
        cout << dp[i] << " " << endl;
    }
    */
    printf("%.0lf\n", MAX);
}
