#include<cstdio>
#include<algorithm>
#include<iostream>

using namespace std;

int recur(int n,int k){

    if(n==0){
        return 1;
    }
    if(n%2==1){
        return (k*recur(n-1,k))%777;
    }
    int ans = recur(n/2,k);
    return (ans*ans)%777;

}

int main(){

    int i;
    printf("%d\n",recur(1000000000,2));

}