#include<cstdio>
#include<cmath>

using namespace std;

int main(){
    
    int count;
    int arr[11][11]={};
    double sum=0.00;
    scanf("%d",&count);
    for (int i=0; i<count; i++) {
        for (int j=0; j<5; j++) {
            scanf("%d",&arr[i][j]);
            if (arr[i][j] != 0) {
                if (j==0) {
                    sum += (1.00)*arr[i][j];
                }
                if (j==1) {
                    sum += (0.75)*arr[i][j];
                }
                if (j==2) {
                    sum += (0.50)*arr[i][j];
                }
                if (j==3) {
                    sum += (0.25)*arr[i][j];
                }
                if (j==4) {
                    sum += (0.125)*arr[i][j];
                }
            }
        }
    }
    sum = ceil(sum);
    int ans = (int)sum;
    printf("%d",ans);
    
}