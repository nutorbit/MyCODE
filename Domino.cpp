#include<bits/stdc++.h>

using namespace std;

#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

typedef struct node{
    int posi,h;
}NODE;


NODE arr[100001]={};
bool toR[100001]={};
bool toL[100001]={};
int n,x,h;
int MAX = 1;
int si = 0;
bool st = false;

void LR(int o,int i,int dis){
    //printf("xxx\n");
    int sum = dis;
    if(MAX < dis)MAX = dis,si = o,st = true;
    for(int j=i+1;j<n;j++){
        //printf("%d : [%d - %d] = %d\n",o,j, i,sum);
        if(arr[j].posi - arr[i].posi < arr[i].h)toR[j] = true,sum++,LR(o,j,sum);
        else{
            //LR(o,j,sum);
            break;
        }
    }
}

void RL(int o,int i,int dis){
    int sum = dis;
    if(MAX < dis)MAX = dis,si = o,st = false;
    for(int j=i-1;j>=0;j--){
        if(arr[i].posi - arr[j].posi < arr[i].h)toL[j] = true,sum++,RL(o,j,sum);
        else{
            //RL(o,j,sum);
            break;
        }
    }
}




int main(){
    
    scanf("%d",&n);
    FOR(i,0,n){
        scanf("%d %d",&x,&h);
        arr[i].posi = x;
        arr[i].h = h;
    }
    FOR(i,0,n){//left to right
        if(!toR[i])LR(i,i,0);
        //cout << endl;
    }
    
    FOR(i,n-1,0){//right to left
        if(!toL[i])RL(i,i,0);
    }
    
    st?printf("%d R\n",si+1):printf("%d L\n",si+1);
}
