#include<cstdio>

using namespace std;

char field[100][100]={};
int row,col;
char tmp;

void create(int i,int j,char symbol){
	if(symbol == '>'){
		if(j+1 < col && (field[i][j+1] == '.' || field[i][j+1] == '|')){
			if(field[i][j+1] == '|'){
				field[i][j+1] = '+';
				create(i,j+1,symbol);
			}else if(field[i][j+1] == '.'){
				field[i][j+1] = '-';
				create(i,j+1,symbol);
			}
		}
	}else if(symbol == 'V'){
		if(i+1 < row && (field[i+1][j] == '.' || field[i+1][j] == '-')){
			if(field[i+1][j] == '-'){
				field[i+1][j] = '+';
				create(i+1,j,symbol);
			}else if(field[i+1][j] == '.'){
				field[i+1][j] = '|';
				create(i+1,j,symbol);
			}
		}
	}else if(symbol == '<'){
		if(j-1 >= 0 && (field[i][j-1] == '.' || field[i][j-1] == '|')){
			if(field[i][j-1] == '|'){
				field[i][j-1] = '+';
				create(i,j-1,symbol);
			}else if(field[i][j-1] == '.'){
				field[i][j-1] = '-';
				create(i,j-1,symbol);
			}
		}
	}else if(symbol == '^'){
		if(i-1 >= 0 && (field[i-1][j] == '.' || field[i-1][j] == '-')){
			if(field[i-1][j] == '-'){
				field[i-1][j] = '+';
				create(i-1,j,symbol);
			}else if(field[i-1][j] == '.'){
				field[i-1][j] = '|';
				create(i-1,j,symbol);
			}
		}
	}
}

int main() {
	
	
	scanf("%d %d ",&col,&row);
	for(int i=0;i<row;i++){
		for (int j=0; j<col; j++) {
			scanf(" %c",&tmp);
			if(tmp == 'B'){
				tmp = '*';
			}
			field[i][j] = tmp;
		}
		//scanf("%c",&tmp);
	}
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			if(field[i][j] == '>' || field[i][j] == '^' || field[i][j] == 'V' || field[i][j] == '<'){
				create(i,j,field[i][j]);
			}
		}
	}
	
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			printf("%c",field[i][j]);
		}
		printf("\n");
	}
	
	
	return 0;
}
