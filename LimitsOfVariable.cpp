#include<iostream>
#include<limits>
#include<cstdlib>

using namespace std;

int main(){
    cout << "SHORT" << endl;
    cout << sizeof(short) << endl;
    cout << numeric_limits<short>::min() << endl;
    cout << numeric_limits<short>::max() << endl;
    cout << "INT" << endl;
    cout << sizeof(int) << endl;
    cout << numeric_limits<int>::min() << endl;
    cout << numeric_limits<int>::max() << endl;
    cout << "LONG" << endl;
    cout << sizeof(long) << endl;
    cout << numeric_limits<long>::min() << endl;
    cout << numeric_limits<long>::max() << endl;
    
    cout << "FLOAT" << endl;
    cout << sizeof(float) << endl;
    cout << numeric_limits<float>::min() << endl;
    cout << numeric_limits<float>::max() << endl;
    cout << "DOUBLE" << endl;
    cout << sizeof(double) << endl;
    cout << numeric_limits<double>::min() << endl;
    cout << numeric_limits<double>::max() << endl;
    cout << "LONG DOUBLE" << endl;
    cout << sizeof(long double) << endl;
    cout << numeric_limits<long double>::min() << endl;
    cout << numeric_limits<long double>::max() << endl;
    return 0;
}
