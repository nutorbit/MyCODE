#include<cstdio>

using namespace std;

int main(){
    int x;
    scanf("%d",&x);
    int mid = (x+1)/2;
    int l = mid,r = mid;
    for (int i=1; i<=x; i++) {
        for (int j=1; j<=x; j++) {
            if (j == l || j == r) {
                for (int k=l; k<=r; k++) {
                    printf("*");
                    j++;
                }
                j--;
            }else{
                printf("-");
            }
        }
        if (i >= mid) {
            l++;
            r--;
        }else{
            l--;
            r++;
        }
        printf("\n");
    }
}
