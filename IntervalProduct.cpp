#include<cmath>
#include<cstdlib>
#include<algorithm>
#include<iostream>

using namespace std;

template <class TYPE>
class FenWick {
    TYPE *arr;
    int size;
    
public:
    FenWick(TYPE n){
        arr = (TYPE*)malloc(sizeof(TYPE)*n);
        size = n;
        for (int i=1; i<=size; i++) {
            arr[i] = 1;
        }
    }
    void update(int i,int val){
        int diff = val/arr[i];
        while(i <= size){
            arr[i] *= diff;
            i += i & (-i);
        }
    }
    int RSQ(int i){
        int total = 1;
        while(i > 0){
            total *= arr[i];
            i -= i & (-i);
        }
        return total;
    }
    
    int RSQ(int a,int b){
        return RSQ(b) / RSQ(a-1);
    }
    
    void pr(){
        for (int i=1; i<=size; i++) {
            printf("%d ",arr[i]);
        }
        printf("\n");
    }
};

FenWick<int> fenwick(20);

int main(){

    
    int n,k,tmp;
    char c;
    int a,b;
    scanf("%d %d",&n,&k);
    for (int i=1; i<=n; i++) {
        scanf("%d",&tmp);
        fenwick.update(i,tmp);
    }
    fenwick.pr();
    for (int i=1; i<=k; i++) {
        scanf(" %c",&c);
        if(c == 'C'){
            scanf("%d %d",&a,&b);
            fenwick.update(a,b);
        }else{
            scanf("%d %d",&a,&b);
            int total = fenwick.RSQ(a,b);
            printf("%d\n",total);
        }
    }
    
}
