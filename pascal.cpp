#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
#define mod 55555

vector<ll> pascal(ll n){
    vector<ll> row(n+1);
    row[0] = 1;
    for(ll i=1; i<n/2+1; i++){
        row[i] = row[i-1] * (n-i+1)/i;
        row[i] %= mod;
    }
    for(ll i=n/2+1; i<=n; i++){
        row[i] = row[n-i];
        row[i] %= mod;
    }
    return row;
}

int main(){
    ll N; cin >> N;
    for(auto v: pascal(N)){
        cout << v << " ";
    }
    cout << endl;
}