#include<cstdio>
#include<algorithm>

using namespace std;

#define FOR(i,j,k) for(int i=j;(j<k)?i<k:i>=k;(j<k)?i++:i--)

const int MAX = 101;

bool ch[MAX][MAX]={};
int field[MAX][MAX]={};
bool de[MAX][MAX]={};

int main(){
    int r,c;
    char tmp;
    int del=-1;
    int si,sj;
    scanf("%d %d ",&r,&c);
    FOR(i,0,r){
        FOR(j,0,c){
            scanf(" %c",&tmp);
            if(tmp == 'x')ch[i][j] = true,field[i][j] = 1;
        }
    }
    FOR(i,1,r){
        FOR(j,1,c){
            ch[i][j] ?field[i][j] = min(field[i-1][j],min(field[i][j-1],field[i-1][j-1]))+1:0;
            if(del < field[i][j])del = field[i][j],si=i,sj=j;
        }
    }
    printf("%d %d %d\n",si-del+2,sj-del+2,del);
    FOR(i,si-del+1,si+1){
        FOR(j,sj-del+1,sj+1){
            de[i][j] = true;
        }
    }
    /*
    FOR(i,0,r){
        FOR(j,0,c){
            if(de[i][j])printf("%d",field[i][j]);
            else printf("0");
        }
        printf("\n");
    }
    */
    int del2 = -1;
    int t;
    int sii,sjj;
    FOR(i,0,r){
        FOR(j,0,c){
            if(field[i][j] != 0 && !de[i][j] && del2 <= field[i][j])del2 = field[i][j],sii = i,sjj = j;
        }
    }
    //printf("m %d i %d j %d\n",del2,sii,sjj);
    if(del2 == -1){
        printf("%d %d %d\n",si-del+2,sj-del+2,del);
    }else{
        int maxx = field[sii][sjj];
        si = sii;
        sj = sjj;
        int tmpx[5] ={1,0,0,-1};
        int tmpy[5] ={0,1,-1,0};
        bool can = false;
        FOR(i,0,4){
            if(!de[sii+tmpx[i]][sjj+tmpy[i]] && field[sii+tmpx[i]][sjj+tmpy[i]] != 0)can = true;
        }
        if(can){
            FOR(i,0,4){
                if(maxx < field[sii+tmpx[i]][sjj+tmpy[i]] && de[sii+tmpx[i]][sjj+tmpy[i]])maxx = field[sii+tmpx[i]][sjj+tmpy[i]],si = sii+tmpx[i],sj = sjj+tmpy[i];
            }
        }
        printf("%d %d %d\n",si-maxx+2,sj-maxx+2,maxx);
    }
}
