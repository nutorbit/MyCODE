a = list(map(int, input().split()))
n = len(a)

class SegmentTree:
    def __init__(self, arr):
        self.values = [None]*(n**2)
        self.build(0, arr, 0, n-1)
    def build(self, node, arr,  l, r):
        if l == r:
            self.values[node] = arr[l]
            # print(node, l, self.values[node])
            return arr[l]
        else:
            mid = (l+r)//2
            p = self.build((node<<1)+1, arr, l, mid)
            q = self.build((node<<1)+2, arr, mid+1, r)
            self.values[node] = min(p, q)
            return self.values[node]
    def Find(self, node, l, r, i, j):
        if i > r or j < l:
            return float('inf')
        elif l >= i and r <= j:
            return self.values[node]
        else:
            mid = (l+r)//2
            p = self.Find((node<<1)+1, l, mid, i, j)
            q = self.Find((node<<1)+2, mid+1, r, i, j)
            return min(p, q)
    def RMQ(self, i, j):
        return self.Find(0, 0, n-1, i, j)
    def showAll(self):
        print(self.values)

if __name__ == '__main__':
    st = SegmentTree(a)
    while True:
        a, b = map(int, input().split())
        print(st.RMQ(a, b))
    # print(st.showAll())
