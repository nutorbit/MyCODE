#include <bits/stdc++.h>

using namespace std;
bool Sieve[1000001] = {};
vector<int> Prime;
int main(){
    int N ;
    scanf("%d",&N);
    for(int i=2;i*i<1000001;i++){
        if(!Sieve[i]){
            for(int j=i*i;j<1000001;j+=i){
                Sieve[j] = true;
            }
        }
    }
    for(int i=2;i<1000001;i++){
        if(!Sieve[i])Prime.push_back(i);
    }
    int i = lower_bound(Prime.begin(),Prime.end(),N)-Prime.begin();
    if(Prime[i]==N){
        printf("0");
        return 0;
    }else{
        if(i==0){
            printf("%d",abs(Prime[i]-N));
        }else{
            printf("%d",min(abs(Prime[i]-N),abs(Prime[i-1]-N)));
        }
    }
}
