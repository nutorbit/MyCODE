#include<bits/stdc++.h>

using namespace std;

template <class TYPE>
class LinkedList{
    
    struct Node {
        int x;
        Node *next;
    };
    
public:
    
    Node *head,*front,*del;
    LinkedList(){
        head = NULL;
        front = NULL;
    }
    void appendFirst(int val){
        if(head == NULL){
            Node *tmp = new Node();
            tmp->x = val;
            tmp->next = NULL;
            head = tmp;
            front = head;
        }
        else{
            Node *tmp = new Node();
            tmp->x = val;
            tmp->next = front;
            front = tmp;
        }
    }
    void appendLast(int val){
        if(head == NULL){
            Node *tmp = new Node();
            tmp->x = val;
            tmp->next = NULL;
            head = tmp;
            front = head;
        }
        else{
            Node *tmp = new Node();
            tmp->x = val;
            head->next = tmp;
            head = tmp;
        }
        
    }
    
    int popLast(){
        Node *tmp = head;
        int val = tmp->x;
        head = head->next;
        delete tmp;
        return val;
    }
    int popFront(){
        Node *tmp = front;
        int val = tmp->x;
        front = front->next;
        delete tmp;
        return val;
    }
    
    int delIn(int posi){
        Node *del = front;
        posi--;
        while(posi--){
            del = del->next;
        }
        
        int val = del->next->x;
        
        if(del->next->next != NULL)
            del->next = del->next->next;
        else del->next = NULL;
        return val;
    }
    void print(){
        Node *pivot = front;
        while(pivot->next != NULL){
            printf("%d ",pivot->x);
            pivot = pivot->next;
        }
        printf("%d\n",pivot->x);
    }
    
};

int main() {
    LinkedList<int> list;
    
    list.appendLast(5);
    list.appendLast(10);
    list.appendLast(20);
    list.appendLast(30);
    cout << "\nstart" << endl;
    list.print();
    cout << "\nappendfirst" << endl;
    list.appendFirst(1);
    list.appendFirst(0);
    list.print();
    cout << "\ndelin i" << endl;
    cout << list.delIn(2) << endl;
    list.print();
    cout << "\npopfront" << endl;
    cout << list.popFront() << endl;
    list.print();
    //cout << list.popLast() << endl;
    //cout << list.popLast() << endl;
    //cout << list.popLast() << endl;

    return 0;
}
