#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int dp[100][100];

int inversions(int n, int k){
    
    if (dp[n][k] != -1) return dp[n][k];
    if (k == 0) return dp[n][k] = 1;
    if (n == 0) return dp[n][k] = 0;
    int val = 0;
    for (int j = 0; j <= k && k-j >= 0; j++)
        val += inversions(n-1, k-j);
    return dp[n][k] = val;
}

int main(){
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d %d", &n, &k);
        FOR(i,1,n+1)
            FOR(j,0,k+1)
                dp[i][j] = -1;
        printf("%d\n", inversions(n, k));
        /*
        FOR(i,0,n+1){
            FOR(j,0,k+1){
                cout << dp[i][j] << " ";
            }
            cout << endl;
        }
        */
    }
    return 0;
}
