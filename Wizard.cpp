#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
typedef pair<ll, ll> pii;

struct cmp{
    template<class T1, class T2>
    size_t operator()(const pair<T1, T2> &p)const {
        auto h1 = hash<T1>{}(p.first);
        auto h2 = hash<T2>{}(p.second);
        return h1 ^ h2;
    }
};

unordered_map<pii, pii, cmp> m;

int main(){
    ios_base::sync_with_stdio(0);
    ll x, y; cin >> x >> y;
    int n; cin >> n;
    pii A[4][2000]={};
    for(int i=0; i<4; i++){
        for(int j=0; j<n; j++){
            cin >> A[i][j].first >> A[i][j].second;
        }
    }
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            ll a = A[0][i].first + A[1][j].first;
            ll b = A[0][i].second + A[1][j].second;
            m.insert({{a, b}, {i, j}});
        }
    }
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            ll a = x - (A[2][i].first + A[3][j].first);
            ll b = y - (A[2][i].second + A[3][j].second);
            auto it = m.find({a, b});
            if(it != m.end()){
                cout << A[0][it->second.first].first << " " << A[0][it->second.first].second << endl;
                cout << A[1][it->second.second].first << " " << A[1][it->second.second].second << endl;
                cout << A[2][i].first << " " << A[2][i].second << endl;
                cout << A[3][j].first << " " << A[3][j].second << endl;
                return 0;
            }
            
        }
    }
}

