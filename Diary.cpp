#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;
int N, K, A;
int Arr[101] = {};
int Start[10001] = {};

int main()
{
    scanf("%d%d%d", &N, &K, &A);
    Arr[0] = 0;
    for (int i = 1; i <= N; i++)
    {
        scanf("%d", &Arr[i]);
        if (i != 0)
        {
            Arr[i] += Arr[i - 1];
        }
    }
    int maxx = 0;
    int idx = 0;
    for (int i = 1; i <= Arr[N - 1]; i++)
    {
        int Count = 0;
        int Tmp = 0;
        for (int j = 0; j < K; j++)
        {
            int T = Tmp;
            for (int k = Tmp + 1; k <= N; k++)
            {
                if (Arr[k] >= i + (j * A) && Arr[k - 1] < i + (j * A))
                {
                    Tmp = k;
                    break;
                }
            }
            if (T != Tmp)
                Count++;
            if (maxx < Count)
            {
                maxx = Count;
                idx = i;
            }
        }
    }
    printf("%d %d\n", idx, maxx);
}
