#include<iostream>

using namespace std;

int recur(int a,int b){
    if (a==0) {
        return 0;
    }
    
    if (a<b) {
        return a;
    }
    
    return recur(a-b,b);
}

int main(){
    int a,b;
    cin >> a >> b;
    cout << recur(a,b);
}
