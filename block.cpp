#include <cstdio>
using namespace std;

bool field[21][21]={};

int main() {
    
    int n;
    scanf("%d",&n);
    int mid = n;
    int left = n+1;
    int right = n+1;
    int mm = n;
    int c = 0;
    int l = -1;
    for(int i=1;i<=n+(2*(n-1))-1;i++){
        for(int j=1;j<=((2*n)-1)+2;j++){
            if(j>=left && j <= right){
                field[i][j] = true;
            }else{
                field[i][j] = false;
            }
        }
        if(l == mm){
            left++;
            right--;
            
        }
        else if(l != mm && l != -1){
            l++;
        }
        else if(c == mid){
            l = 3;
        }else if(c != mid){
            c++;
            if(c == mid){
                continue;
            }
            left--;
            right++;
        }
    }
    int ll = 3;
    int rr = ((2*n)-1)+2-2;
    int tmp = n;
    for(int i=1;i<=n+(2*(n-1))-1;i++){
        for(int j=1;j<=((2*n)-1)+2;j++){
            if(i == n+(2*(n-1))-1){
                field[i][j+3] = false;
                field[i][((2*n)-1)+2-2] = false;
            }
            else if(ll == rr){
                field[i][ll] = false;
            }
            else if(i == tmp){
                field[i][ll] = false;
                field[i][rr] = false;
                tmp++;
                ll++;
                rr--;
            }
            else if(j == n+1 && i == 1){
                field[i][j] = false;
            }
        }
    }
    for(int i=1;i<=n+(2*(n-1))-1;i++){
        for(int j=1;j<=((2*n)-1)+2;j++){
            if(field[i][j]){
                printf("# ");
            }else{
                printf("- ");
            }
        }
        printf("\n");
    }
    return 0;
}