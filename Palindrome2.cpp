#include<iostream>
#include<string>
#include<vector>

using namespace std;

bool is_pa(string s){
    if(s.length() <= 1) return true;
    char first = s[0];
    char last = s[s.length()-1];
    if(first == last) {
        string shorter = s.substr(1,s.length()-2);
        return is_pa(shorter);
    }else{
        return false;
    }
}

int main(){
    string in;
    getline(cin,in);
    cout << in << " is ";
    if (!is_pa(in)) {
        cout << "not";
    }
    cout << " a palindrome";
}
