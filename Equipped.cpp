#include<cstdio>
#include<algorithm>

using namespace std;

int h[(1<<9)]={};
int minn = 9999999;
int N,S;
int tmp,v;
int tembit = 0;

void recur(int now,int pick,int val){
    if (val > minn) {
        return;
    }
    if (pick == (1<<S)-1) {
        if (val < minn) {
            minn = val;
        }
        return;
    }
    if (now == (1<<S)-1) {
        return;
    }
    if (h[now+1] != 1000000) {
        recur(now+1,pick|now+1,val+h[now+1]);
        recur(now+1,pick,val);
    }else{
        recur(now+1,pick,val);
    }
    
}

int main(){
    
    scanf("%d %d",&N,&S);
    for (int i=0; i<(1<<9); i++) {
        h[i] = 1000000;
    }
    for (int i=0; i<N; i++) {
        tmp = 0;
        tembit = 0;
        scanf("%d",&v);
        for (int j=0; j<S; j++) {
            scanf("%d",&tmp);
            if (tmp == 1) {
                tembit |= (1<<(S-1-j));
            }
        }
        if (h[tembit] > v) {
            h[tembit] = v;
        }
    }
    
    recur(0,0,0);
    
    printf("%d\n",minn);
    
}
