#include<cstdio>
#include<algorithm>

using namespace std;

#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll data[100009][3]={};
ll n,p,exp=0,pow=0,maxx,now;

int main(){

    scanf("%lld",&n);
    p=n;
    FOR(i,0,n){
        scanf("%lld%lld",&data[i][0],&data[i][1]);
        exp+=data[i][1];
        pow+=data[i][0];
        data[i][2]=1;
    }
    maxx=exp-(pow*2);
    
    FOR(i,0,n){
        now=(exp-data[i][1])-2*(pow-data[i][0]);
        now-=(n-(p-1))*(n-(p-1));
        if(maxx<now){
            //printf("%d %lld\n",i,now);
            maxx=now;
            exp-=data[i][1];
            pow-=data[i][0];
            data[i][2]=0;
            p--;
        }
    }
    
    FOR(i,0,n){
      if(data[i][2]==0){
        now=(exp+data[i][1])-2*(pow+data[i][0]);
        now-=(n-(p+1))*(n-(p+1));
        if(maxx<now){
            //printf("%d %lld\n",i,now);
            maxx=now;
            exp+=data[i][1];
            pow+=data[i][0];
            p++;
        }
      }
    }
    
    printf("%lld\n",maxx);
}
