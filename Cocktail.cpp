#include<stdio.h>

#define SIZE 1000001

int main(){
    
    int a;
    int n;
    int tail[SIZE]={0};
    int index;
    int num;
    int i;
    long long count=0;
    
    scanf("%d",&n);
    
    for(i=0;i<n;i++){
        scanf("%d",&num);
        tail[num]++;
    }
    scanf("%d",&a);
    for(i=0;i<=a;i++){
        if(tail[i]!=0){
            index=a-i;
            if(tail[index]!=0){
                if(i!=index)
                    count=count+(long long)tail[index]*tail[i];
                else
                    count=count+((long long)tail[i]*(tail[i]-1))/2;
                tail[i]=0;
                tail[index]=0;
            }
        }
    }
    printf("%lld\n",count);
    
    return 0;
    
}