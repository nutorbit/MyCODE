#include<bits/stdc++.h>

using namespace std;

bool n[1<<20]={};
int c[15]={};

int main(){
    int N; cin >> N;
    while(N--){
        int id, d, m, y; scanf("%d %d/%d/%d",&id, &d, &m, &y);
        if(!n[id]){
            c[m] += 1;
            n[id] = true;
        }
    }
    for(int i=1; i<=12; i++)
        cout << i << " " << c[i] << endl;
}
