#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

int n;
ll  s1, s2, s3;
ll one[3][10]; // 0: h 1: t 2: u
ll two[3][100]; // 0: 0 1, 1: 1 2, 3: 0 2
ll three[1000];

int main(){
    scanf("%d", &n);
    FOR(i,0,n){
        ll temp;
        scanf("%lld", &temp);
        ll h = temp/100;//ร้อย
        ll t = temp/10 - h*10;//สิบ
        ll u = temp - h*100 - t*10;//หน่วย
        one[0][h]++;
        one[1][t]++;
        one[2][u]++;
        two[0][h*10+t]++;
        two[1][t*10+u]++;
        two[2][h*10+u]++;
        three[temp]++;
    }
    
    FOR(i,0,3){
        for(int j = 0; j < 10; j++) s1 += one[i][j]*(one[i][j]-1)/2;
        for(int j = 0; j < 100; j++) s2 += two[i][j]*(two[i][j]-1)/2;
        
    }
    for(int i = 0; i < 1000; i++) s3 += three[i]*(three[i]-1)/2;
    
    printf("%lld\n", s1-s2+s3);
    
}
