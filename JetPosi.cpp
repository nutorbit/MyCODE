#include<cstdio>
#include<cmath>

using namespace std;

int n,v;
char c;
int sumR=0,sumL=0;
int maxR,maxL;
int sir=1,sjr=1;
int sil=1,sjl=1;
int R[100002]={};
int L[100002]={};

void recurR(int i,int val,int c){
    if(i > n)return;
    val += L[i]-R[i];
    if(val < 0 || val < sumR)return;
    //printf("R :%d val: %d\n",sir,val);
    if(maxR <= val){
        if(maxR == val && sjr - sir >= i - c){
            if(sjr - sir == i - c && sir > c){
                sir = c;
                sjr = i;
            }
            else if(sjr - sir > i - c){
                sir = c;
                sjr = i;
            }
        }else if(maxR < val){
            maxR = val;
            sjr = i;
            sir = c;
        }
        
    }
    recurR(i+1,val,c);
}

void recurL(int i,int val,int c){
    if(i > n)return;
    val += R[i]-L[i];
    if(val < 0 || val < sumL)return;
    //printf("L:%d val: %d\n",sil,val);
    if(maxL <= val){
        if(maxL == val && sjl - sil >= i - c){
            if(sjl - sil == i - c && sil > c){
                sil = c;
                sjl = i;
            }
            else if(sjl - sil > i - c){
                sil = c;
                sjl = i;
            }
        }else if(maxL < val){
            maxL = val;
            sjl = i;
            sil = c;
        }
        
    }
    recurL(i+1,val,c);
}

int main(){
    
    scanf("%d",&n);
    for (int i=1; i<=n; i++) {
        scanf(" %c %d",&c,&v);
        if(c == 'R'){
            R[i] = v;
            sumR += v;
        }
        else{
            L[i] = v;
            sumL += v;
        }
    }
    maxR = sumR;
    maxL = sumL;
    for(int i=1;i<=n;i++){
        recurR(i,sumR,i);
    }
    
    for(int i=1;i<=n;i++){
        recurL(i,sumL,i);
    }
    int difR =sjr - sir;
    int difL =sjl - sil;
    if(maxR > maxL)printf("R %d %d %d\n",maxR,sir,sjr);
    else if(maxR < maxL)printf("L %d %d %d\n",maxL,sil,sjl);
    else{
        if(sir < sil)printf("R %d %d %d\n",maxR,sir,sjr);
        else if(sjr < sjl)printf("L %d %d %d\n",maxL,sil,sjl);
        else if(difR < difL)printf("R %d %d %d\n",maxR,sir,sjr);
        else printf("L %d %d %d\n",maxL,sil,sjl);
    }
        
    
}
