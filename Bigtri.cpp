#include<cstdio>
#include<cmath>

using namespace std;

typedef struct info{
    double x;
    double y;
}INFO;

INFO arr[1001];

int main(){
    
    double maxx;
    int count;
    
    scanf("%d",&count);
    
    for (int i=0; i<count; i++) {
        scanf("%lf %lf",&arr[i].x,&arr[i].y);
    }
    maxx = -1;
    for (int i=0; i<count; i++) {
        for (int j=i+1; j<count; j++) {
            for (int k=j+1; k<count; k++) {
                double tmp = abs((arr[i].x*arr[j].y)+(arr[j].x*arr[k].y)+(arr[k].x*arr[i].y)-(arr[i].y*arr[j].x)-(arr[j].y*arr[k].x)-(arr[k].y*arr[i].x))/2.000;
                if (tmp > maxx) {
                    maxx = tmp;
                }
            }
        }
    }
    printf("%.3lf",maxx);
}
