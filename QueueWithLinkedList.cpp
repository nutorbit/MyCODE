#include<bits/stdc++.h>

using namespace std;


template <class TYPE>
class Queue {
    struct node{
        TYPE data;
        node *next;
        node *prev;
    };
    node *front;
    node *tail;
    int N;
    
public:
    Queue(){
        front = tail = NULL;
        N = 0;
    }
    TYPE Front(){
        return front->data;
    }
    bool isEmpty(){
        if(N == 0)return true;
        return false;
    }
    void EnQueue(int val){
        if(isEmpty()){
            node *tmp = new node();
            tmp->data = val;
            tmp->next = NULL;
            tmp->prev = NULL;
            front = tail = tmp;
        }else{
            node *tmp = new node();
            tmp->data = val;
            tmp->next = tail;
            tail->prev = tmp;
            tail = tmp;
        }
        N++;
    }
    
    TYPE DeQueue(){
        if(isEmpty()){
            cout << "Queue Empty" << endl;
            return 0;
        }
        int val = front->data;
        front = front->prev;
        front->next = NULL;
        N--;
        return val;
    }
    void Display(){
        node *pivot = new node();
        pivot = front;
        while(pivot->prev != NULL and pivot->prev != front){
            cout << pivot->data << " " ;
            pivot = pivot->prev;
        }
        cout << pivot->data << endl;
    }
    int SizeOfQueue(){
        return N;
    }
    void MakeCircularQueue(){
        front->next = tail;
        tail->prev = front;
    }
    void solve(int from,int to){
        MakeCircularQueue();
        node *pivot = new node();
        node *pivot2 = new node();
        pivot = front;
        pivot2 = front;
        while(from--){
            pivot = pivot->prev;
        }
        while(to--){
            pivot2 = pivot2->prev;
        }
        while(pivot != pivot2){
            cout << pivot->data << " " ;
            pivot = pivot->prev;
        }
        cout << pivot->data << "\n" ;
    }
};
int main(){
    Queue<int> q;
    int n; cin >> n;
    for (int i=0; i<n; i++) {
        int tmp; scanf("%d",&tmp);
        q.EnQueue(tmp);
    }
    //q.Display();
    int from ,to; cin >> from >> to ;
    q.solve(from,to);
    /*
    q.EnQueue(4);
    q.Display();
    q.EnQueue(20);
    q.EnQueue(30);
    q.Display();
    cout << "Dequeue :" << q.DeQueue() << endl;
    q.MakeCircularQueue();
    q.Display();
     */
}
