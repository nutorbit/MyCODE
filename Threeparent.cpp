#include<cstdio>
#include<stack>
#include<string.h>
#include<vector>

using namespace std;

vector<char> st;
char tmp[1000001]={};

int main(){

    int t;
    
    scanf("%d",&t);
    for (int i=0; i<t; i++) {
        scanf("%s",tmp);
        int len = strlen(tmp);
        bool ck=false;
        for(int j=0; j < len;j++){
            if(tmp[j] == '(' || tmp[j] == '[' || tmp[j] == '{'){
                st.push_back(tmp[j]);
            }else{
                if(st.empty()){
                    ck=true; break;
                }
                else if(tmp[j] == ')' && st.back() == '(')st.pop_back();
                else if(tmp[j] == '}' && st.back() == '{')st.pop_back();
                else if(tmp[j] == ']' && st.back() == '[')st.pop_back();
                else {ck=true; break;}
            }
        }
        if(st.empty()&&!ck)printf("yes\n");
        else{
            printf("no\n");
            st.clear();
        }
    }

}
