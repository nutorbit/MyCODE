#include<cstdio>
#include<cmath>

using namespace std;

int field[100001]={};

int main(){
    int tc;
    int l,h,r;
    int maxx=-1;
    int minn=9999999;
    scanf("%d",&tc);
    for(int i=0;i<tc;i++){
        scanf("%d %d %d",&l,&h,&r);
        if(maxx < r){
            maxx = r;
        }
        if(minn > l){
            minn = l;
        }
        for(int j=l;j<r;j++){
            if(h > field[j]){
                field[j] = h;
            }
        }
    }
    /*
    printf("%d %d\n",maxx,0);
    for(int i=minn;i<=maxx;i++){
        printf("%d ",field[i]);
    }
    */
    
    int tmp = field[minn];
    printf("%d %d ",minn,tmp);
    for(int i=minn;i<=maxx;i++){
        if(tmp < field[i] || tmp > field[i]){
            printf("%d ",i);
            printf("%d ",field[i]);
            tmp = field[i];
        }
    }
}
