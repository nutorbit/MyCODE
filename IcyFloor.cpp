#include<bits/stdc++.h>

using namespace std;
typedef struct node{
    int x,y;
}NODE;


int N;
bool field[1001][1001]={};
int DP[1001][1001]={};
queue<NODE> q;
NODE u ,f;

int main(){
    ios :: sync_with_stdio(false);
    cin >> N;
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            DP[i][j] = INT_MAX;
            int tmp; cin >> tmp;
            if(tmp == 1)field[i][j] = true;
        }
    }
    u.x = u.y = 0;
    q.push(u);
    DP[0][0] = 0;
    while(!q.empty()){
        f = q.front(); q.pop();
        int dirx[] = {1,0,-1,0};
        int diry[] = {0,1,0,-1};
        for (int i=0; i<4; i++) {
            int tmpx = dirx[i] + f.x;
            int tmpy = diry[i] + f.y;
            if(DP[tmpx][tmpy] > DP[f.x][f.y]+1 and !field[tmpx][tmpy] and tmpx < N and  tmpx >= 0 and tmpy < N and tmpy >= 0){
                int t = DP[f.x][f.y]+1;
                while(DP[tmpx][tmpy] > DP[f.x][f.y]+1 and !field[tmpx+dirx[i]][tmpy+diry[i]] and tmpx+dirx[i] < N and tmpy+diry[i] < N and tmpx+dirx[i] >= 0 and tmpy+diry[i] >= 0){
                    t++;
                    //DP[tmpx+dirx[i]][tmpy+diry[i]] = min(DP[tmpx+dirx[i]][tmpy+diry[i]],DP[tmpx][tmpy]+1);
                    tmpx = tmpx+dirx[i];
                    tmpy = tmpy+diry[i];
                }
                //cout << "i :" << tmpx << " j :" << tmpy << endl;
                //cout << "t : " << t << endl;
                if(DP[tmpx][tmpy] > t){
                    DP[tmpx][tmpy] = t;
                    u.x = tmpx,u.y = tmpy;
                    q.push(u);
                }
            }
        }
    }
    /*
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            cout << DP[i][j] << "\t";
        }
        cout << endl;
    }
    
    cout << endl;
    */
    cout << DP[N-1][N-1] << endl;
}
