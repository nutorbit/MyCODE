#include<cstdio>

using namespace std;

char two[3]={'A','B','C'},three[3]={'D','E','F'},four[3]={'G','H','I'},five[3]={'J','K','L'},six[3]={'M','N','O'},seven[4]={'P','Q','R','S'},eight[3]={'T','U','V'},nine[4]={'W','X','Y','Z'};

int start,t;
int posii,posij;
char ans[10001]={};
int index = 1;

void set(int number,int t){
    if (start == 1) {
        posii = 0;
        posij = 0;
        index = 0;
    }
    if (start == 2) {
        posii = 0;
        posij = 1;
        ans[0] = two[(t-1)%3];
    }
    if (start == 3) {
        posii = 0;
        posij = 2;
        ans[0] = three[(t-1)%3];
    }
    if (start == 4) {
        posii = 1;
        posij = 0;
        ans[0] = four[(t-1)%3];
    }
    if (start == 5) {
        posii = 1;
        posij = 1;
        ans[0] = five[(t-1)%3];
    }
    if (start == 6) {
        posii = 1;
        posij = 2;
        ans[0] = six[(t-1)%3];
    }
    if (start == 7) {
        posii = 2;
        posij = 0;
        ans[0] = seven[(t-1)%4];
    }
    if (start == 8) {
        posii = 2;
        posij = 1;
        ans[0] = eight[(t-1)%3];
    }
    if (start == 9) {
        posii = 2;
        posij = 2;
        ans[0] = nine[(t-1)%4];
    }
}



void move(int x,int y,int t){
    //printf("i %d j %d\n",posii+y,posij+x);
    if (posii+y < 3 && posii+y >= 0 && posij+x < 3 && posij+x >=0) {
        posii+=y;
        posij+=x;
        
        if (posii == 0 && posij == 0) {
            //printf("index -> %d\nt -> %d\nindex-t -> %d\n",index,t,index-t);
            index-=t;
            if (index < 0) {
                index = 0;
            }
        }
        if (posii == 0 && posij == 1) {
            ans[index] = two[(t-1)%3];
            index++;
        }
        if (posii == 0 && posij == 2) {
            ans[index] = three[(t-1)%3];
            index++;
        }
        if (posii == 1 && posij == 0) {
            ans[index] = four[(t-1)%3];
            index++;
        }
        if (posii == 1 && posij == 1) {
            ans[index] = five[(t-1)%3];
            index++;
        }
        if (posii == 1 && posij == 2) {
            ans[index] = six[(t-1)%3];
            index++;
        }
        if (posii == 2 && posij == 0) {
            ans[index] = seven[(t-1)%4];
            index++;
        }
        if (posii == 2 && posij == 1) {
            ans[index] = eight[(t-1)%3];
            index++;
        }
        if (posii == 2 && posij == 2) {
            ans[index] = nine[(t-1)%4];
            index++;
        }
    }
}

int main(){
    int N;
    int x,y,p;
    scanf("%d",&N);
    scanf("%d %d",&start,&t);
    set(start,t);
    for (int i=1; i<N; i++) {
        scanf("%d %d %d",&x,&y,&p);
        move(x,y,p);
    }
    //printf("index -> %d\n",index);
    if (index <= 0) {
        printf("null\n");
    }else{
        for (int i=0; i<index; i++) {
            printf("%c",ans[i]);
        }
        printf("\n");
    }
}
