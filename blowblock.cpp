#include<bits/stdc++.h>

using namespace std;

int field[505][505]={};

int main(){
    ios_base::sync_with_stdio(0);
    int N; cin >> N;
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            cin >> field[i][j];
        }
    }
    
    /*
    cout << "--------------" << endl;
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            cout << field[i][j] << " ";
        }
        cout << endl;
    }
    */
    int ans = 0;
    for(int a=0; a<N; a+=2){
        for(int b=0; b<N; b+=2){
            int mul = 1;
            for(int i=a; i<a+2; i++){
                for(int j=b; j<b+2; j++){
                    int m = -1;
                    int si, sj;
                    for(int k=i; k<N; k+=2){
                        for(int l=j; l<N; l+=2){
                            if(m < field[k][l]){
                                m = field[k][l];
                                si = k;
                                sj = l;
                            }
                        }
                    }
                    swap(field[i][j], field[si][sj]);
                    mul *= field[i][j];
                    field[i][j] = 0;
                }
            }
            ans += mul;
        }
    }
    /*
    cout << "--------------" << endl;
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            cout << field[i][j] << " ";
        }
        cout << endl;
    }
    
    int ans = 0;
    for(int i=0; i<N; i+=2){
        for(int j=0; j<N; j+=2){
            int mul = 1;
            for(int k=i; k<i+2; k++){
                for(int l=j; l<j+2; l++){
                    mul *= field[k][l];
                }
            }
            ans += mul;
        }
    }
    */
    cout << ans << endl;
}
