#include<cstdio>
#include<algorithm>

using namespace std;

int field[501][501]={};
int QS[501][501]={};

int main(){

    int row,col,t,r,c;
    scanf("%d %d",&row,&col);
    scanf("%d",&t);
    for (int i=0; i<t; i++) {
        scanf("%d %d",&r,&c);
        field[r][c]=1;
    }
    for (int i=0; i<row; i++) {
        QS[i][0] = QS[i-1][0] + field[i][0];
    }
    for (int j=0; j<col; j++) {
        QS[0][j] = QS[0][j-1] + field[0][j];
    }
    for (int i=1; i<row; i++) {
        for (int j=1; j<col; j++) {
            QS[i][j] = QS[i-1][j] + QS[i][j-1] - QS[i-1][j-1] + field[i][j];
        }
    }
    /*
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            printf("%d ",QS[i][j]);
        }
        printf("\n");
    }
    */
    int a;
    int maxsize = min(row,col);
    int maxx = -1;
    int tmp = 0;
    //a = (2+1)*(2+1)-(2+1-2)*(2+1-2);
    //printf("a:%d\n",a);
    for (int s = 3; s<maxsize; s++) {
        for (int i=2+tmp; i<row; i++) {
            for (int j=2+tmp; j<col; j++) {
                a = (QS[i][j]-QS[i-s][j]-QS[i][j-s]+QS[i-s][j-s])-(QS[i-1][j-1]-QS[i-s+1][j-1]-QS[i-1][j-s+1]+QS[i-s+1][j-s+1]);
                //printf("%d\n",a);
                if(a == 0){
                    if(maxx < s)maxx = s;
                }
            }
        }
        tmp++;
    }
    printf("%d\n",maxx);
}

