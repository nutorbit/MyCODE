#include<bits/stdc++.h>

using namespace std;

double cal(int n){
    if(n == 0)return 0;
    if(n % 2 == 0)return cal(n-2) + 2;
    if(n >= 3)return cal(n-3) - 1 + 2*sqrt(3) + 1;
}

int main(){
    int N;
    cin >> N;
    if(N == 1)printf("%.6lf\n", 2.0); 
    else if(N == 3)printf("%.6lf\n", 2+sqrt(3));
    else{
        double ans = cal(N-2)+2;
        printf("%.6lf\n", ans);

    }
}
