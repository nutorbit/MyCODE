#include<cstdio>

using namespace std;

int main(){
    int N;
    scanf("%d",&N);
    if (N%2 != 0) {
        int left = 1;
        int right = N;
        for (int i=1; i<=N; i++) {
            for (int j=1; j<=N; j++) {
                if (j == left) {
                    for (int k=left; k<=right; k++) {
                        printf("*");
                    }
                }else{
                    printf(" ");
                }
            }
            if (i>=(N+1)/2) {
                left--;
                right++;
            }else{
                left++;
                right--;
            }
            printf("\n");
        }
    }else{
        printf("error\n");
    }
}
