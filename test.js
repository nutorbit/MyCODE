var row,col;
var grid;

function R(){
    for(var i=0; i<row; i++){
        for(var j=col-1; j>=0; j--){
            if(grid[i][j].match(/[A-Z]/i)){
                var k = j;
                while(grid[i][k+1] == ".") k += 1;
                var tmp = grid[i].substring(0,j) + "." + grid[i].substring(j+1);
                tmp = tmp.substring(0,k) + grid[i][j] + tmp.substring(k+1);
                grid[i] = tmp;
            }
        }
    }
}

function L(){
    for(var i=0; i<row; i++){
        for(var j=0; j<col; j++){
            if(grid[i][j].match(/[A-Z]/i)){
                var k = j;
                while(grid[i][k-1] == ".") k -= 1;
                var tmp = grid[i].substring(0,j) + "." + grid[i].substring(j+1);
                tmp = tmp.substring(0,k) + grid[i][j] + tmp.substring(k+1);
                grid[i] = tmp;
            }
        }
    }
}

function U(){
    for(var j=0; j<col; j++){
        for(var i=0; i<row; i++){
            if(grid[i][j].match(/[A-Z]/i)){
                var k = i;
                while(k-1 >= 0 && grid[k-1][j] == ".") k -= 1;
                var tmp1 = grid[i];
                tmp1 = tmp1.substring(0, j) + "." + tmp1.substring(j+1);
                var tmp2 = grid[k];
                tmp2 = tmp2.substring(0, j) + grid[i][j] + tmp2.substring(j+1);
                // console.log(tmp1, tmp2);
                grid[i] = tmp1;
                grid[k] = tmp2;
            }
        }
    }
}

function D(){
    for(var j=col-1; j>=0; j--){
        for(var i=row-1; i>=0; i--){
            if(grid[i][j].match(/[A-Z]/i)){
                var k = i;
                while(k+1 < row && grid[k+1][j] == ".") k += 1;
                var tmp1 = grid[i];
                tmp1 = tmp1.substring(0, j) + "." + tmp1.substring(j+1);
                var tmp2 = grid[k];
                tmp2 = tmp2.substring(0, j) + grid[i][j] + tmp2.substring(j+1);
                // console.log(tmp1, tmp2);
                grid[i] = tmp1;
                grid[k] = tmp2;
            }
        }
    }
}

function secretArchivesLock(lock, actions) {
    row = lock.length;
    col = lock[0].length;
    grid = lock;
    // console.log(row, col);
    for(var i in actions) {
        if(actions[i] === 'R')R();
        if(actions[i] === 'D')D();
        if(actions[i] === 'L')L();
        if(actions[i] === 'U')U();
        // console.log(grid);
    }
    console.log(grid);
}




var a = ["....",
         "AB..",
         ".C..",
         "...."];
var b = "RDL";

secretArchivesLock(a, b);
