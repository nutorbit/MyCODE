#include <cstdio>
#include <algorithm>

using namespace std;

typedef struct{
    int data;
    int from, to;
}Node;

Node node[1000001];
short colorTmp[1000001];
int N,M, Frm, To, R, G, B,In;

Node create(int now, int to, int order){
    Node temp;
    temp.from = now;
    temp.to = to;
    if(now == to)
        temp.Color[(now-1)%3] = 1;
    else{
        int mid = (now+to)/2;
        node[order*2] = create(now,mid,order*2);
        node[order*2 + 1] = create(mid+1,to, order*2+1);
        for(int i =0;i<3;i++)
            temp.Color[i] = node[order*2].Color[i] + node[order*2 + 1].Color[i];
    }
    return temp;
}

void update(int order){
    node[order].Color[Frm]--;
    node[order].Color[To]++;
    if(node[order].from!= node[order].to){
        if((node[order].from+node[order].to)/2 >= In)
            update(order*2);
        else
            update(order*2 + 1);
    }
}

void query(int from, int to, int order){
    int mid = (node[order].from+node[order].to)/2;
    if(from <= node[order].from && to >= node[order].to){
        R += node[order].Color[0];
        G += node[order].Color[1];
        B += node[order].Color[2];
        return;
    }
    if(to < node[order].from || from > node[order].to)
        return;
    
    query(from,to, order*2);
    query(from,to, order*2 + 1);
}

int main(){
    int a, b, c;
    char k;
    scanf("%d %d",&N,&M);
    node[1] = create(1,N,1);
    for(int i=1;i<=N;i++)
        colorTmp[i] = (i-1)%3;
    for(int i =0;i<M;i++){
        scanf("%d ",&a);
        if(a==1){
            scanf("%d %c ", &In, &k);
            if(k =='R')
                To = 0;
            else if(k == 'G')
                To = 1;
            else
                To = 2;
            Frm = colorTmp[In];
            colorTmp[In] = To;
            update(1);
        }
        else{
            scanf("%d %d", &b, &c);
            R = G = B = 0;
            query(b, c, 1);
            if(R>G && R > B)
                printf("R\n");
            else if(G>R && G>B)
                printf("G\n");
            else if(B>R && B>G)
                printf("B\n");
            else
                printf("None\n");
        }
        
    }
    //  for(int i=1;i<32;i++)
    //      printf("id :%d from %d to %d : R %d G %d B %d\n",i,node[i].from, node[i].to, node[i].Color[0], node[i].Color[1], node[i].Color[2]);
}
