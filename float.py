'''float'''
def main():
    '''ohooh'''
    num = input()
    try:
        if num.index('.'):
            pass
    except ValueError:
        print(str(int(num)+5)+'.000000')
        return
    first, second = num[:num.index('.')], num[num.index('.')+1:]
    while len(second) < 6:
        second += '0'
    if len(second) > 6 and int(second[6]) >= 5:
        second = int(second[:6])+1
    else:
        second = int(second[:6])
    if second > 999999:
        second = '000000'
        first = int(first)+1
    print(str(int(first)+5)+'.'+str(second))
main()
