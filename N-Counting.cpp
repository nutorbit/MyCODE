#include<bits/stdc++.h>

using namespace std;

template<class T>
class FenwickTree{
    int Size;
    int Arr[1<<20];
    public:
        FenwickTree(int N){
            Size = N;
        }
        void update(int i, int val){
            while(i <= Size){
                Arr[i] += val;
                i += (i & (-i));
            }
        }
        int cal(int i){
            int result = 0;
            while(i > 0){
                result += Arr[i];
                i -= (i & (-i));
            }
            return result;
        }
        int RS(int a, int b){
            return cal(b) - cal(a-1);
        }

};

FenwickTree<int> FW(20005);
vector<int> A(20005);

int main(){
    ios_base::sync_with_stdio(0);
    int N, Q; cin >> N >> Q;
    for(int i=1; i<=N; i++){
        cin >> A[i];
    }
    for(int T=0; T<Q; T++){
        
    }
}