#include<bits/stdc++.h>

using namespace std;
typedef long long int ll ;

int arr[2000002]={};

void initial(){
    arr[0] = 0,arr[1] = 1;
    //cout << "{" ;
    for(int i=2;i<=2000000;i++){
        arr[i] += arr[i-1] + arr[i-2];
        arr[i]%=1000000007;
        //cout << arr[i] << "," ;
    }
    //cout << "};";
}

int main(){
    ios :: sync_with_stdio(false);
    initial();
    int Q; cin >> Q;
    while(Q--){
        int n,k; cin >> n >> k;
        ll total = 0LL;
        total = arr[n];
        while(k--){
            ll i,bi; cin >> i >> bi;
            //tmp[i] = bi;
            total += bi*(arr[n-i+1]);
            total %= 1000000007;
        }
        /*
        for (int i = 2; i<=n and n-i+1 >= 0; i++) {
            total += tmp[i]*(arr[n-i+1]);
            total %= 1000000007;
            tmp[i] = 0;
        }
         */
        cout << total << endl;
    }
}
