/*

#include<cstdio>
#include<queue>

using namespace std;

typedef struct node{
    int i,j,day;
}NODE;

int field[1001][1001]={};
int row,col;
bool ch[1001][1001]={};
int tmp = 0;
queue<NODE> q;

void BFS(int i,int j,int day){
    NODE u,now;
    u.i = i;
    u.j = j;
    u.day = day;
    q.push(u);
    ch[u.i][u.j]= true;
    while(!q.empty()){
        now = q.front();
        q.pop();
        //printf("i %d j %d \n",now.i,now.j);
        if(field[now.i-1][now.j]-now.day >= 1 && now.i-1 >= 0 && !ch[now.i-1][now.j]){
            u.i = now.i-1;
            u.j = now.j;
            u.day = now.day+1;
            field[u.i][u.j] = -1;
            ch[u.i][u.j]=true;
            q.push(u);
        }
        if(field[now.i][now.j+1]-now.day >= 1 && now.j+1 < col && !ch[now.i][now.j+1]){
            u.j = now.j+1;
            u.i = now.i;
            u.day = now.day+1;
            field[u.i][u.j] = -1;
            ch[u.i][u.j]=true;
            q.push(u);
        }
        if(field[now.i+1][now.j]-now.day >= 1 && now.i+1 < row && !ch[now.i+1][now.j]){
            u.i = now.i+1;
            u.j = now.j;
            u.day = now.day+1;
            field[u.i][u.j] = -1;
            ch[u.i][u.j]=true;
            q.push(u);
        }
        if(field[now.i][now.j-1]-now.day >= 1 && now.j-1 >= 0 && !ch[now.i][now.j-1]){
            u.j = now.j-1;
            u.i = now.i;
            u.day = now.day+1;
            field[u.i][u.j] = -1;
            ch[u.i][u.j]=true;
            q.push(u);
        }
        
    }
}

int main(){

    scanf("%d %d",&row,&col);
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            scanf("%d",&field[i][j]);
        }
    }
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            if(field[i][j] == 0){
                BFS(i,j,1);
            }
        }
    }
    //printf("\n");
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            if(ch[i][j]){
                //printf("1 ");
                tmp++;
            }
            //else printf("0 ");
        }
        //printf("\n");
    }
    tmp = (row*col)-tmp;
    printf("%d\n",tmp);

}

*/
 
 #include <bits/stdc++.h>
 
 using namespace std;
 
 int Village[1001][1001]={};
 
 queue<pair<pair<int,int>,int > > Q;
 
 int main(){
 
    int N,M;
    scanf("%d%d",&N,&M);
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            scanf("%d",&Village[i][j]);
            if(Village[i][j]==0){
                Q.push(make_pair(make_pair(i,j),1));
            }
        }
    }
    while(!Q.empty()){
        int x = Q.front().first.first;
        int y = Q.front().first.second;
        int day = Q.front().second;
        Q.pop();
        if(x+1<N&&day<Village[x+1][y]){
            Village[x+1][y] = 0;
            Q.push(make_pair(make_pair(x+1,y),day+1));
        }
        if(x-1 >=0&&day<Village[x-1][y]){
            Village[x-1][y] = 0;
            Q.push(make_pair(make_pair(x-1,y),day+1));
        }
        if(y+1<M&&day<Village[x][y+1]){
            Village[x][y+1] = 0;
            Q.push(make_pair(make_pair(x,y+1),day+1));
        }
        if(y-1>=0&&day<Village[x][y-1]){
            Village[x][y-1] = 0;
            Q.push(make_pair(make_pair(x,y-1),day+1));
        }
    }
    int Count = 0;
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            if(Village[i][j]!=0)Count++;
        }
    }
    printf("%d\n",Count);
 }
 
 
 

