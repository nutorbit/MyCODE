def main():
    num = int(input())
    lst = []
    for val in range(2, num+1):
        isPrime = True
        for m in range(2, val):
            if val % m == 0:
                isPrime = False
                break
        if isPrime == True:
            lst.append(val)
    print(*lst)

main()