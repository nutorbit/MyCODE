#include<bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<ll, pair<ll, ll> > piii;
vector<piii> box;

bool cmp(const piii &a, const piii &b){
    if(a.first == b.first)
        return a.second > b.second;
    return a.first > b.first;
}

bool can(piii A, piii B){
    return (A.first > B.first and A.second.first > B.second.first)
        or (A.first > B.second.first and A.second.first > B.first)
}

int main(){
    int N; cin >> N;
    for(int i=0; i<N; i++){
        ll a, b, c; cin >> a >> b >> c;
        box.push_back({max(a, b), {min(a, b), c}});
        box.push_back({max(c, b), {min(c, b), a}});
        box.push_back({max(a, c), {min(a, c), b}});
    }
    sort(box.begin(), box.end(), cmp);
    vector<ll> ans(box.size());
    ll MAX = LLONG_MIN;
    for(int i=0; i<box.size(); i++)
        ans[i] = box[i].second.second;
    for(int i=0; i<box.size(); i++){
        ll tmp = 0;
        for(int j=0; j<i; j++){
            if(can(box[j], box[i])){
                tmp = max(tmp, ans[j]);
            }
        }
        ans[i] += tmp;
        MAX = max(MAX, ans[i]);
    }
    cout << MAX << endl;
}
