#include<cstdio>
#include<vector>
#include<queue>
#include<algorithm>

using namespace std;

queue<char> q[1001];
int arr[1000][1000]={};
bool field[1000][1000]={};
int ans[10001]={};

typedef struct position{
    int x,y;
}position;

position x[100];
position t;

int S,walk;

void move(position t,char tmp,int i){
    if (tmp == 'N') {
        if(field[t.y-1][t.x] == false && t.y-1 >= 0){
            field[t.y][t.x] = false;
            x[i].y = t.y-1;
            field[x[i].y][x[i].x] = true;
            arr[x[i].y][x[i].x] = i+1;
        }
    }
    if (tmp == 'S') {
        if(field[t.y+1][t.x] == false && t.y+1 < S){
            field[t.y][t.x] = false;
            x[i].y = t.y+1;
            field[x[i].y][x[i].x] = true;
            arr[x[i].y][x[i].x] = i+1;
        }
    }
    if (tmp == 'E') {
        if(field[t.y][t.x+1] == false && t.x+1 < S){
            field[t.y][t.x] = false;
            x[i].x = t.x+1;
            field[x[i].y][x[i].x] = true;
            arr[x[i].y][x[i].x] = i+1;
        }
    }
    if (tmp == 'W') {
        if(field[t.y][t.x-1] == false && t.x-1 >= 0){
            field[t.y][t.x] = false;
            x[i].x = t.x-1;
            field[x[i].y][x[i].x] = true;
            arr[x[i].y][x[i].x] = i+1;
            //printf("%d %d\n",x[i].y,x[i].x);
        }
    }
}

void set(char tmp,int i){
    t = x[i];
    move(t,tmp,i);
    
}

int main(){
    
    char a,b;
    scanf("%d %d",&S,&walk);
    for (int i=0; i<4; i++) {
        for (int j=0; j<walk; j++) {
            scanf(" %c",&a);
            q[i].push(a);
        }
    }
    //position 1
    arr[0][S-1] = 1;
    field[0][S-1] = true;
    x[0].y = 0;
    x[0].x = S-1;
    //position 2
    arr[S-1][S-1] = 2;
    field[S-1][S-1] = true;
    x[1].y = S-1;
    x[1].x = S-1;
    //position 3
    arr[S-1][0] = 3;
    field[S-1][0] = true;
    x[2].y = S-1;
    x[2].x = 0;
    //position 4
    arr[0][0] = 4;
    field[0][0] = true;
    x[3].y = 0;
    x[3].x = 0;
    //printf("----\n");
    for(int i=0;i<walk*4;i++){
        char tmp = q[i%4].front();
        //printf("%c\n",tmp);
        set(tmp,i%4);
        q[i%4].pop();
    }
    int one=0,two=0,three=0,four=0;
    /*
    printf("\n");
    for (int i=0; i<S; i++) {
        for (int j=0; j<S; j++) {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
    */
    for (int i=0; i<S; i++) {
        for (int j=0; j<S; j++) {
            if(arr[i][j] == 0){
                printf("No\n");
                return 0;
            }
            if (arr[i][j] == 1) {
                one++;
            }
            if (arr[i][j] == 2) {
                two++;
            }
            if (arr[i][j] == 3) {
                three++;
            }
            if (arr[i][j] == 4) {
                four++;
            }
        }
    }
    
    int maxx = max(one,max(two,max(three,four)));
    //printf("%d\n",maxx);
    int index = 0;
    if (one == maxx) {
        ans[index++] = 1;
    }
    if (two == maxx) {
        ans[index++] = 2;
    }
    if (three == maxx) {
        ans[index++] = 3;
    }
    if (four == maxx) {
        ans[index++] = 4;
    }
    printf("%d %d\n",index,maxx);
    for (int i=0; i<index; i++) {
        printf("%d\n",ans[i]);
    }
    
    
    
}
