#include<bits/stdc++.h>

using namespace std;
int r, c; 
int stx, sty, endx, endy;
int dp[155][155]={};
bool can[155][155]={};
queue<pair<int, int> > q;

int dirx[] = {1, -1, 0, 0};
int diry[] = {0, 0, -1, 1};
const int MAXIMUM = INT_MAX;
bool vis[155][155]={};

void travel(){
    q.push(make_pair(stx, sty));
    dp[stx][sty] = 1;
    while(!q.empty()){
        int x = q.front().first;
        int y = q.front().second;
        q.pop();
        for(int i=0; i<4; i++){
            int toX = x+dirx[i];
            int toY = y+diry[i];
            if(toX < r and toX >= 0 and toY < c and toY >= 0){
                if(dp[toX][toY] > dp[x][y] + 1 and can[x][y] and !can[toX][toY]){
                    dp[toX][toY] = dp[x][y] + 1;
                    vis[toX][toY] = true;
                }
                if(dp[toX][toY] > dp[x][y] + 1 and can[toX][toY]){
                    dp[toX][toY] = dp[x][y] + 1;
                    q.push(make_pair(toX, toY));
                }
            }
        }
    }
}

int ans = INT_MAX;
int Count = 0;
bool vv[155][155]={};

void go(){
    q.push(make_pair(endx, endy));
    dp[endx][endy] = 1;
    while(!q.empty()){
        int x = q.front().first;
        int y = q.front().second;
        q.pop();
        for(int i=0; i<4; i++){
            int toX = x+dirx[i];
            int toY = y+diry[i];
            if(toX < r and toX >= 0 and toY < c and toY >= 0){
                if(vis[toX][toY]){
                    ans = min(ans, dp[toX][toY] + dp[x][y]);
                    if(!vv[toX][toY])Count += 1;
                    vv[toX][toY] = true;
                }
                if(dp[toX][toY] > dp[x][y] + 1 and can[x][y] and !can[toX][toY]){
                    dp[toX][toY] = dp[x][y] + 1;
                }
                if(dp[toX][toY] > dp[x][y] + 1 and can[toX][toY]){
                    dp[toX][toY] = dp[x][y] + 1;
                    q.push(make_pair(toX, toY));
                }
            }
        }
    }
}

int main(){
    // ios_base::sync_with_stdio(0);
    scanf("%d %d", &r, &c);
    scanf("%d %d", &stx, &sty);
    scanf("%d %d", &endx, &endy);
    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            int sym; scanf("%d", &sym);
            can[i][j] = sym?true:false;
            dp[i][j] = MAXIMUM;
        }
    }
    stx -= 1, sty -=1, endx -= 1, endy -=1;
    travel();
    bool ch = false;
    if(dp[endx][endy] != MAXIMUM){
        ans = dp[endx][endy];
        printf("%d\n", 0);
        ch = true;
    }
    go();
    if(!ch)printf("%d\n", Count);
    /*
    cout << endl;
    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            cout << dp[i][j] << "\t" ;
        }
        cout << endl;
    }
    */
    printf("%d\n", ans);
}
