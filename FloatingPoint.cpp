#include<bits/stdc++.h>

using namespace std;

int main(){
	int N; cin >> N;
	while(N--){
		long double in; cin >> in;
		cout << std::setprecision(pow(2, 14));
		cout << (long double)pow(2, in) << endl;
	}
}