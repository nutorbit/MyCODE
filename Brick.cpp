#include <iostream>
#include <cstdio>

using namespace std ;

int main (){

    char matrix[20][20] ;
    int n,m,i,j,k,object[20],col[20] ;
    
    cin >> n >> m ;
    for (i=0;i<m;i++){
        col[i] = -1 ;
    }
    for (i=0;i<n;i++){
        for (j=0;j<m;j++){
            cin >> matrix[i][j] ;
            if (col[j] < 0)
                if (matrix[i][j] == 'O') col[j] = i ;
        }
    }
    for (i=0;i<m;i++){
        cin >> object[i] ;
    }
    for (j=0;j<m;j++){
        if (col[j] > 0) {
            if (object[j] > col[j])
                for (k=col[j]-1;k>=0;k--){
                    matrix[k][j] = '#' ;
                }
            else{
                for (k=col[j]-1;k>=(col[j]-object[j]);k--){
                    matrix[k][j] = '#' ;
                }
            }
        }
        else if (col[j] < 0){
            if (object[j] > n)
                for ( k=n-1;k>=0;k--){
                    matrix[k][j] = '#' ;
                }
            else{
                for ( k=n-1;k>=(n-object[j]);k--){
                    matrix[k][j] = '#' ;
                }
            }
        }
    }
    for ( i=0;i<n;i++){
        for ( j=0;j<m;j++){
            cout << matrix[i][j] ;
        }
        cout<<endl ;
    }
}