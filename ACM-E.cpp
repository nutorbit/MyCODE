#include <bits/stdc++.h>

using namespace std;
int Hash2[1001] = {},Hash3[1001] = {};
int main(){
    int N;
    char str[52];
    scanf("%d",&N);
    int MAX2 = 0,MAX3 =0;
    while(N--){
        scanf("%s",str);
        int l = strlen(str);
        if(l<=1)continue;
        int now2=(str[0]-'0')*10 + (str[1]-'0') ,now3 = 0;
        now3 =now2;
        Hash2[now2]++;
        for(int i=2;i<l;i++){
            now2*=10;
            now3*=10;
            now2+= (str[i]-'0');
            now3+= (str[i]-'0');
            now2%=100;
            now3%=1000;
            Hash2[now2]++;
            Hash3[now3]++;
            //        printf("%02d %03d\n",now2,now3);
            MAX2 = max(MAX2,Hash2[now2]);
            MAX3 = max(MAX3,Hash3[now3]);
        }
    }
    for(int i=0;i<100;i++){
        if(Hash2[i]==MAX2){
            printf("%02d ",i);
        }
    }
    printf("\n");
    for(int i=0;i<1000;i++){
        if(Hash3[i]==MAX3){
            printf("%03d ",i);
        }
    }
    //    printf("\n");
}
