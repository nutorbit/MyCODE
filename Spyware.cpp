#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int N,M,K,a,b,MaxTime = INT_MIN;
int t[100001]={};
long long int point[100001]={};
queue<int> q;
vector<int> H;
vector<int> adj[100001],MAX;
typedef struct co{
    int n,o;
}CO;
vector<CO> SO;
bool Comp1(const CO &a,const CO &b){
    return a.o > b.o;
}
void push(int A){
    H.push_back(A);
    push_heap(H.begin(),H.end());
}

void pop(){
    pop_heap(H.begin(),H.end());
    H.pop_back();
}

void flush(){
    q.push(K);
    t[K] = 0;
    while(!q.empty()){
        int f = q.front();
        q.pop();
        FOR(i,0,adj[f].size()){
            if(t[adj[f][i]] == -1){
                t[adj[f][i]] = t[f]+1;
                MaxTime = max(MaxTime,t[adj[f][i]]);
                q.push(adj[f][i]);
            }
        }
    }
}

int main(){
    scanf("%d %d %d",&N,&M,&K);
    FOR(i,1,N+1)scanf("%lld",&point[i]),t[i] = -1;
    FOR(i,0,M){
        scanf("%d %d",&a,&b);
        adj[a].push_back(b),adj[b].push_back(a);
    }
    flush();
    // store point
    long long int Ans = 0;
    for(int i=1;i<=N;i++){
        CO u; u.n = i,u.o = t[i];
        if(t[i]!=-1)SO.push_back(u);
        else if(point[i]>0){
            Ans += point[i];
        }
    }
    sort(SO.begin(),SO.end(),Comp1);
    int now = SO[0].o;
    int N = SO.size();
    for(int i =0;i<N;i++){
        if(SO[i].o==now){
            push(point[SO[i].n]);
        }
        else{
            if(H[0]>0)Ans += H[0];
            pop();
            push(point[SO[i].n]);
            now = SO[i].o;
        }
    }
    printf("%lld\n",Ans);
}

/* by pracha
 
#include <bits/stdc++.h>
 
using namespace std;
typedef struct{
    int n,o;
}CO;
 
vector<int> H;
 
bool Comp1(const CO &a,const CO &b){
    return a.o > b.o;
}
 
void push(int A){
    H.push_back(A);
    push_heap(H.begin(),H.end());
}
 
void pop(){
    pop_heap(H.begin(),H.end());
    H.pop_back();
}
 
long long int Val[100001];
vector<int> G[100001];
int Order[100001];
queue<int> q;
vector<CO> SO;
 
int main(){
    int N,M,K,i,now,A,B,S,n;
    long long int Ans;
    scanf("%d%d%d",&N,&M,&K);
    for(i=1;i<=N;i++){
        scanf("%lld",&Val[i]);
	}
	for(i=0;i<M;i++){
        scanf("%d%d",&A,&B);
        G[A].push_back(B);
        G[B].push_back(A);
	}
	for(i=1;i<=N;i++)Order[i] = -1;
	Order[K] = 0;
	q.push(K);
	while(!q.empty()){
        n = q.front();
        q.pop();
        S = G[n].size();
        for(i =0;i<S;i++){
            if(Order[G[n][i]]==-1){
                Order[G[n][i]] = Order[n]+1;
                q.push(G[n][i]);
            }
        }
	}
	Ans = 0;
	for(i=1;i<=N;i++){
        if(Order[i]!=-1)SO.push_back({i,Order[i]});
        else if(Val[i]>0){
            Ans += Val[i];
        }
	}
	sort(SO.begin(),SO.end(),Comp1);
    now = SO[0].o;
	N = SO.size();
	for(i =0;i<N;i++){
        if(SO[i].o==now){
            push(Val[SO[i].n]);
        }
        else{
            if(H[0]>0)Ans += H[0];
            pop();
            push(Val[SO[i].n]);
            now = SO[i].o;
        }
	}
	printf("%lld",Ans);
 }
 */
