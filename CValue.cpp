#include<cstdio>
#include<algorithm>
#include<climits>

using namespace std;

int DP[1<<20]={};
int BF[1<<20]={};

int main(){

    int n,tmp;
    scanf("%d",&n);
    for (int i=1; i<=n; i++) {
        scanf("%d",&tmp);
        if(i&1)DP[i]+=DP[i-1]-tmp;
        else DP[i]+=DP[i-1]+tmp;
    }
    for (int i=1; i<=n; i++) {
        BF[i] = min(BF[i-1],DP[i-1]);
    }
    //printf("DPN:%d\n",DP[n]);
    /*
    for (int i=1; i<=n; i++) {
        printf("%d %d\n",i,BF[i]);
    }
    */
    int minn = DP[n];
    for (int i=1; i<=n; i++) {
        //printf("aaa :%d\n",DP[i]-(2*BF[i]));
        tmp = (DP[i]-BF[i])*2;
        if(minn > DP[n]-tmp)minn = DP[n]-tmp;
    }
    printf("%d\n",minn);
    
}
