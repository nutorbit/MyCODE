#include<cstdio>

using namespace std;

int main(){

    long long int n;
    long long int maxx = -1;
    long long int minn = 1000000001;
    long long int tmp;
    long long int sum = 0;
    
    scanf("%lld",&n);
    for(int i=0;i<n;i++){
        scanf("%lld",&tmp); 
        sum += tmp;
        if(maxx < tmp){
            maxx = tmp;
        }
        if(minn > tmp){
            minn = tmp;
        }
    }
    if(n>=3){
        printf("%lld",sum+(minn*(n-3)));
    }
    else{
        printf("%lld",maxx);
    }
    
}
