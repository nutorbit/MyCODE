#include<bits/stdc++.h>

using namespace std;

int main(){
    string tmp; cin >> tmp;
    for(int base=2; base<=9; base++){
        deque<int> in;
        for(int i=0; i<tmp.size(); i++){
            in.push_back(tmp[i]-'0');
        }
        printf("%d : ", base);
        deque<int> ans;
        while(!in.empty()){
            for(int i=0; i<in.size(); i++){
                int tmp = (in[i]-(in[i]/base)*base);
                if(i == in.size()-1)
                    ans.push_back(tmp);
                else
                    in[i+1] += tmp*10;
                in[i] /= base;
            }
            while(in.front() == 0 and !in.empty())in.pop_front();
        }
        for(int i=ans.size()-1; i>=0; i--){
            printf("%d", ans[i]);
        }
        printf("\n");
        /*
        for(int i=ans.size()-1; i>=0; i--){
            printf("%d", ans[i]);
        }
        printf("\n");
         */
    }
}
