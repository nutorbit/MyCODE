#include<bits/stdc++.h>

using namespace std;

int solve(int l, int N, int dp[105][10005]){
    if(l*l == N){
        return 1;
    }
    if(dp[l][N] > 0)return dp[l][N];
    if(l*l > N) return 1<<30;
    return dp[l][N] = min(solve(l, N-(l*l), dp)+1, min(solve(l+1, N-(l*l), dp)+1, solve(l+1, N, dp)));
}

int main(){
    int T; cin >> T;
    for(int t=1; t<=T; t++){
        int N; cin >> N;
        int dp[105][10005]={};
        cout << "Case #" << t << ": "<< solve(1, N, dp) << endl;
    }
}
