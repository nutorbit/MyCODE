#include<cstdio>

using namespace std;

typedef long long int ll;

ll arr[1000001]={};
ll MAX = -1;
ll n,t;

ll ref(ll val){
    ll c = t-1;
    ll mm = val;
    int i = 0;
    while(i!=n+1){
        //printf("%lld\n",mm);
        if(mm < arr[i])mm = val,c--;
        mm-=arr[i];
        i++;
    }
    return c;
}


ll top,down;


void recurD(int l,int r){
    ll mid = (l+r)/2;
    if(l > r)return ;
    if(ref(mid) == 0){
        if(ref(mid-1) != 0){
            down = mid;
            return;
        }
        else recurD(l,mid);
    }
    else if(ref(mid) < 0){
        recurD(mid+1,r);
    }
    else recurD(l,mid);
}

void recurT(int l,int r){
    ll mid = (l+r)/2;
    if(l > r)return ;
    if(ref(mid) == 0){
        if(ref(mid+1) != 0){
            top = mid;
            return;
        }
        else recurT(mid+1,r);
    }
    else if(ref(mid) < 0){
        recurT(mid+1,r);
    }
    else recurT(l,mid);
}

int main(){

    
    scanf("%lld %lld",&n,&t);
    for (int i=0; i<n; i++) {
        scanf("%lld",&arr[i]);
        if(arr[i] > MAX)MAX = arr[i];
    }
    //printf("T:%lld\n",t);
    //printf("%lld\n",ref(11));
    if(t == 1)printf("-1\n");
    else{
        recurD(MAX-1,2147483647);
        recurT(MAX-1,2147483647);
        printf("%lld\n",top-down+1);
    }
}
