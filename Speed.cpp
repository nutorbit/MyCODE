#include<bits/stdc++.h>

using namespace std;
typedef long long ll;


int main(){
    ll N, M, L, Q; cin >> N >> M >> L >> Q;
    vector<vector<pair<ll, ll> > > adj(80008);
    for(int i=0; i<M; i++){
        ll a, b, w; cin >> a >> b >> w;
        adj[a].push_back({b, w});
        // adj[b].push_back({a, w});
    }
    set<ll> power;
    for(int i=0; i<L; i++){
        ll inp; cin >> inp;
        power.insert(inp);
    }
    queue<pair<ll, pair<ll, ll> > > q;
    ll DP[80008][11]={};
    for(int i=0; i<80008; i++)
        for(int j=0; j<11; j++)
            DP[i][j]=INT_MAX;
    DP[1][0] = 0;
    q.push({1, {0, 0}});
    if(power.find(1) != power.end() and Q > 0){
        q.push({1, {1, 1}});
        DP[1][1] = 0;
    }
    ll MIN = LLONG_MAX;
    while(!q.empty()){
        ll from = q.front().first;
        ll nowbot = q.front().second.first;
        ll before = q.front().second.second;
        q.pop();
        if (from == N)
            MIN = min(MIN, DP[N][nowbot]);
        // cout << from << " " << DP[from][nowbot] << " " << nowbot << endl;
        for(auto to: adj[from]){
            if(nowbot+1 <= Q  and before != to.first and power.find(to.first) != power.end() and DP[to.first][nowbot+1] > DP[from][nowbot] + (to.second/(1<<(nowbot)))){
                DP[to.first][nowbot+1] = DP[from][nowbot] + (to.second/(1<<(nowbot)));
                q.push({to.first, {nowbot+1, to.first}});
            }
            // cout << from << " " << to.first << endl;
            // cout << DP[to.first][nowbot] << " " << DP[from][nowbot] + (to.second/(1<<nowbot)) << endl;
            if(DP[to.first][nowbot] > DP[from][nowbot] + (to.second/(1<<nowbot))){
                DP[to.first][nowbot] = DP[from][nowbot] + (to.second/(1<<nowbot));
                q.push({to.first, {nowbot, before}});
            }
        }

    }

    cout << MIN << endl;
}
