#include<bits/stdc++.h>

using namespace std;

int arr[100000]={};
int dp[100001]={};

int main(){
    ios :: sync_with_stdio(false);
    int N,K; cin >> N >> K;
    for (int i=0; i<N; i++) {
        cin >> arr[i];
    }
    sort(arr,arr+N);
    int total = 0,tmp = 0;
    /*
    for (int i=1; i<N; i++) {
        total += arr[i-1];
        if(total <= N){
            tmp++;
            if(tmp >= K)break;
        }
    }
     */
    for (int i=1; i<=N; i++) {
        dp[i] = INT_MIN;
        for (int j=0; j<N; j++) {
            if(i-arr[j]>=0  and dp[i] < dp[i-arr[j]]+1){
                dp[i] = dp[i-arr[j]]+1;
            }
        }
    }
    cout << dp[N] << endl;
}
