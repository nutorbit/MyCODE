#include<cstdio>

using namespace std;

long long int GCD(long long int a,long long int b){
    if (b==0) {
        return a;
    }else{
        return GCD(b,a%b);
    }
    
}

long long int LCM(long long int a, long long int b)
{
    long long int temp = GCD(a, b);
    
    return temp ? (a / temp * b) : 0;
}

long long int arr[100001]={};
long long int cal[100001]={};

int main(){
    long long int ans=0;
    long long int N;
    long long int mu=1;
    scanf("%lld",&N);
    for (int i=0; i<N; i++) {
        scanf("%lld",&arr[i]);
        cal[i] = arr[i];
        mu*=cal[i];
        
    }
    for (int i=1; i<N; i++) {
        long long int x = LCM(arr[i],arr[i-1]);
        arr[i] = x;
    }
    printf("%lld",arr[N-1]);
    /*
    for (int i=0; i<N; i++) {
        ans+=cal[i]/arr[N-1];
    }
    */
    //printf("%lld\n",ans);
}
