#include<bits/stdc++.h>

using namespace std;

int main(){
	int N, M; cin >> N >> M;
	int field[N][M];
	int dp[N][M];
	for(int i=0; i<N; i++){
		for(int j=0; j<M; j++){
			cin >> field[i][j];
		}
	}
	int MAX = -1;
	for(int i=0; i<N; i++){
		for(int j=0; j<M; j++){
			dp[i][j] = max(i-1>=0?dp[i-1][j]:0, j-1>=0?dp[i][j-1]:0) + field[i][j];
			MAX = max(dp[i][j], MAX);
		}
	}
	cout << MAX << endl;

}