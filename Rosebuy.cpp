#include<cstdio>
#include<algorithm>

using namespace std;

int table[1000005];
long long sum[1000005];

int main(){
    
    int n,k;
    long long ans = -1000000000000000,sp = 0;
    long long temp;
    
    scanf("%d %d",&n,&k);
    for(int i=0; i<n; i++){
        scanf("%d",&table[i]);
        sum[i] = sum[i-1] + table[i];
    }
    for(int i=0; i<=n-k; i++){
        temp = sp+sum[i+k-1]-sum[i-1];
        ans = max(temp,ans);
        sp += table[i];
        sp = max(0,sp);
    }
    printf("%lld",ans);
}

