#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef struct path{
    int from,to,dis;
}PATH;

int total[1001]={};
vector<PATH> edge;
PATH u;

int main(){
	int V,E; scanf("%d %d",&V,&E);
	int X,Y; scanf("%d %d",&X,&Y);
    int A,B,T,K; scanf("%d %d %d %d",&A,&B,&T,&K); u.from = A; u.to = B; u.dis = T; edge.push_back(u);
	FOR(i,0,E){
		scanf("%d %d %d",&u.from,&u.to,&u.dis);
        edge.push_back(u);
	}
    FOR(i,0,V)total[i] = INT_MAX;
    total[X] = 0;
    bool st = true;
    FOR(i,0,V-1){
        FOR(j,0,edge.size()){
            int F,T,W; F = edge[j].from; T = edge[j].to; W = edge[j].dis;
            if(F == A and T == B and total[F] > K)continue;
            if(total[F] + W < total[T] and total[F] != INT_MAX ){
                total[T] = total[F] + W;
            }
        }
    }
    cout << total[Y] << endl;
	
}
