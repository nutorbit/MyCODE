#include<cstdio>
#include<algorithm>
#include<unistd.h>
#include<fstream>
#include<string>

using namespace std;

#define MAX_Height 1<<18
#define MAX_Num_Poll 1<<20
#define MAX_Num_Branch 1<<20

typedef struct B{
    double Height;
    int from;
}BRANCH;

bool sort_height(const BRANCH& A, const BRANCH& B){
    return A.Height<B.Height;
}

int M,N,K;
int NumBanana[MAX_Num_Poll];
bool NearPoll[MAX_Num_Poll];
int FromPoll[MAX_Num_Poll];
BRANCH Branch[MAX_Num_Branch];

void setNear(int now){
    NearPoll[FromPoll[now]]=true;
    if(now>0)
        NearPoll[FromPoll[now-1]] = true;
    if(now<N-1)
        NearPoll[FromPoll[now+1]] = true;
}

int main(){
    int i,j,k,Start,Now, Max = 0;
    scanf("%d%d%d",&M,&N,&K);
    for(i=0;i<N;i++){
        scanf("%d",&NumBanana[i]);
        FromPoll[i] = i;
        NearPoll[i] = false;
    }
    for(i=0;i<K;i++){
        scanf("%d%lf",&Branch[i].from,&Branch[i].Height);
    }
    sort(Branch,Branch+K,sort_height);

    scanf("%d",&Start);
    Now = Start;
    setNear(Now-1);
    for(i=0;i<K;i++){
        if((Branch[i].from)==Now){
            Now = Branch[i].from+1;
        }
        else if((Branch[i].from+1)==Now){
            Now = Branch[i].from;
        }
        swap(FromPoll[Branch[i].from-1],FromPoll[Branch[i].from]);
        setNear(Now-1);
    }
    for(i=0;i<N;i++){
        if(NearPoll[FromPoll[i]]){
            Max = max(Max,NumBanana[i]);
        }
    }
    printf("%d\n",Max);
    if(Max>NumBanana[Now-1]){
        printf("USE\n");
    }
    else{
        printf("NO\n");
    }
    //process_mem_usage();
}

