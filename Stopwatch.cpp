#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

bool NoDisplay[11]={};
bool check(int x){
	int fr = x/10;
	int t = x%10;	
	if(NoDisplay[fr] or NoDisplay[t]){
		return true;
	}
	return false;
}

int main(){
	int T; scanf("%d",&T);
	while(T--){
		int N; scanf("%d",&N);
		FOR(i,0,N){
			int no; scanf("%d",&no);
			NoDisplay[no] = true;
		}
		int MM,SEC,MS,round;
		char X1,X2;
		scanf("%d%c%d%c%d",&MM,&X1,&SEC,&X2,&MS);
		//printf("%02d:%02d:%02d\n",MM,SEC,MS);
		int TMM=MM,TSEC=SEC,TMS=MS;
		MM = SEC = MS = round = 0;
		while(true){
			if(!check(MS))MS++;
			while(check(MS))MS++;
			if(MS >= 100){
				if(!check(SEC))SEC++;
				while(check(SEC))SEC++;
				//SEC++;
				MS = 0;
			}
			if(SEC >= 60){
				if(!check(MM))MM++;
				while(check(MM))MM++;
				//MM++;
				SEC = 0;
			}
			//cout << "MM :" << MM << "SEC :" << SEC << "MS :" << MS << endl;
			round++;
			if(TMM <= MM and TSEC <= SEC and TMS <= MS)break;
		}
		MM = (round/6000);round -= MM*6000;
		SEC = (round/100);round -= SEC*100;
		MS = round;
		printf("%02d:%02d:%02d\n",MM,SEC,MS);
		FOR(i,0,10)NoDisplay[i] = false;
	}

}
