#include <cstdio>

using namespace std;

int lines[10001];

int rounds[100001][3];

int main(){
    
    int n, m;
    
    scanf("%d %d", &n, &m);
    
    for(int i = 1; i <= n; i++){
        
        int a,l,s;
        
        scanf("%d %d %d", &a, &l, &s);
        
        lines[l]++;
        
        if((s > rounds[lines[l]][1]) || (s == rounds[lines[l]][1] && l < rounds[lines[l]][2])){
            
            rounds[lines[l]][0] = a;
            rounds[lines[l]][1] = s;
            rounds[lines[l]][2] = l;
        }
        
    }
    
    int i = 1;
    
    while(rounds[i][0] > 0)

        printf("%d\n", rounds[i++][0]);
    
}