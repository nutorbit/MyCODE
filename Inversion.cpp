#include<bits/stdc++.h>

long long inversion = 0;

std::deque<long long > mergeSort(std::deque<long long > A){
    if(A.size() < 2) return A;
    std::size_t mid = A.size()/2;
    std::deque<long long > L(A.begin(), A.begin()+mid), R(A.begin()+mid, A.end()), ans;
    L = mergeSort(L);
    R = mergeSort(R);
    while(L.size() > 0 and R.size() > 0){
        if(L.front() > R.front()){
            inversion += L.size();
            ans.emplace_back(R.front());
            R.pop_front();
        }else{
            ans.emplace_back(L.front());
            L.pop_front();
        }
    }
    ans.insert(ans.end(), L.begin(), L.end());
    ans.insert(ans.end(), R.begin(), R.end());
    return ans;
}

int main(){
    std::ios_base::sync_with_stdio(0);
    long long  T; std::cin >> T;
    while(T--){
        long long  N; std::cin>> N;
        inversion = 0;
        std::deque<long long > A(N);
        for(long long  i=0; i<N; i++){
            std::cin >> A[i];
        }
        std::deque<long long > ans =  mergeSort(A);
        std::cout << inversion << std::endl;
    }
}