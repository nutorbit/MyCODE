#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)


int a[502]={};
int dp[50001]={};
int N,M;

int num(int n, int sum){
    dp[0] = 1;
    int current = 0;
    FOR(i,0,n){
        current += a[i];
        FOR(j,min(sum,current),a[i]){
            dp[j] += dp[j-a[i]];
            dp[j] %= 1000007;
        }
    }
    return dp[sum]%1000007;
    /*
    if(idx>N)return 0;
    if(val<0)return 0;
    if(val==0)return 1;
    return (num(val,idx+1)%1000007 + num(val-a[idx],idx+1)%1000007)%1000007;
     */
}

int main(){
    
    scanf("%d %d",&N,&M);
    FOR(i,0,N)scanf("%d",&a[i]);
    cout << num(N,M) << endl;
    
}
