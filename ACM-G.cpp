#include<bits/stdc++.h>
using namespace std;
# define INF 0x3f3f3f3f


typedef pair<int, int> iPair;

class Graph
{
    int V;
    
    list< pair<int, int> > *adj;
    
public:
    Graph(int V);
    
    
    void addEdge(int u, int v, int w);
    
    
    void shortestPath(int s,int b);
};


Graph::Graph(int V)
{
    this->V = V;
    adj = new list<iPair> [V];
}

void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

void Graph::shortestPath(int src,int b)
{
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq;
    
    vector<int> dist(V, INF);
    
    pq.push(make_pair(0, src));
    dist[src] = 0;
    
    while (!pq.empty())
    {
        int u = pq.top().second;
        pq.pop();
        
        
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            
            int v = (*i).first;
            int weight = (*i).second;
            
            
            if (dist[v] > dist[u] + weight)
            {
                
                dist[v] = dist[u] + weight;
                pq.push(make_pair(dist[v], v));
            }
        }
    }
    
    printf("%d\n", dist[b]);
}

int main()
{
    
    int N,K; cin >> N >> K;
    Graph g(N);
    for(int i=0;i<N-1;i++){
        int from,to,w; cin >> from >> to >> w;
        g.addEdge(from, to, w);
    }
    for (int i=0; i<K; i++) {
        int a,b; cin >> a >> b;
        g.shortestPath(a,b);
    }
    
    
    
    return 0;
}
