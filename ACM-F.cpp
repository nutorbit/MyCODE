#include <bits/stdc++.h>

using namespace std;
char Arr[12][5] = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
int main(){
    int N;
    char A[5];
    scanf("%s",A);
    int now = 0;
    for(int i=0;i<12;i++){
        bool F = true;
        for(int j=0;j<3;j++){
            if(A[j]!=Arr[i][j]){
                F = false;
                break;
            }
        }
        if(F){
            now = i;
        }
    }
    scanf("%d",&N);
    now += N;
    now %= 12;
    while(now < 0){
        now += 12;
    }
    now %=12;
    printf("%s",Arr[now]);
}
