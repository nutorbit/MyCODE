def mergeSort(sequence):
    if len(sequence) < 2:
        return sequence
    result = []
    mid = len(sequence)//2
    x = mergeSort(sequence[:mid])
    y = mergeSort(sequence[mid:])
    while len(x) > 0 and len(y) > 0:
        if x[0] < y[0]:
            result.append(x.pop(0))
        else:
            result.append(y.pop(0))
    result += x
    result += y
    return result

arr = [2, 1, 5, 3, 2, 1]
print(mergeSort(arr))