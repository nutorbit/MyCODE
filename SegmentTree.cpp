#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll N,Q;
ll tmp;
vector<ll> ARR;

template <class TYPE>
class SegmentTree {
    vector<ll> st;
    
public:
    
    SegmentTree(){
        st.assign(5*N,0);
        ARR.assign(N+1,0);
    }
    
    ll left(ll L){return L << 1;}
    ll right(ll R){return (R << 1)+1;}
    ll mid(int L,int R){return (L+R)/2;}
    
    ll update(ll node, ll l, ll r, ll idx, ll new_value){
        if (idx > r || idx < l)
            return st[node];
        if (l == idx && r == idx) {
            ARR[idx] = new_value;
            return st[node] = l;
        }
        ll p1, p2;
        p1 = update(left(node),l,mid(l,r),idx,new_value);
        p2 = update(right(node),mid(l,r)+1,r,idx,new_value);
        return st[node] = (ARR[p1] >= ARR[p2])?p1 : p2;
    }
    
    ll update(ll idx, ll new_value){
        return update(1, 0, N - 1, idx, new_value);
    }
    
    ll rmq(ll node,ll l,ll r,ll i,ll j){
        if(i > r || j < l)return -1;
        if(l >= i && r <= j)return st[node];
        int p1 = rmq(left(node),l,mid(l,r),i,j);
        int p2 = rmq(right(node),mid(l,r)+1,r,i,j);
        if(p1 == -1)return p2;
        if(p2 == -1)return p1;
        return (ARR[p1] >= ARR[p2])?p1:p2;
    }
    
    ll rmq(ll i,ll j){
        return rmq(1,0,N-1,i,j);
    }
};

int main(){
    
    cin >> N >> Q;
    char ch;
    ll a,b;
    SegmentTree<ll> smt;
    FOR(i,0,Q){
        scanf(" %c",&ch);
        if(ch == 'U')scanf("%lld %lld",&a,&b),smt.update(a,b);
        else scanf("%lld %lld",&a,&b),printf("%lld\n",ARR[smt.rmq(a,b)]);
        /*
        cout << "ARR : " ;
        FOR(j,0,N){
            printf("%lld ",ARR[j]);
        }
        cout << endl;
         */
    }
    
    
    
    
}
