#include<bits/stdc++.h>

using namespace std;

template <class TYPE>
class Stack {
    struct node{
        TYPE data;
        node *next;
        node *prev;
    };
    node *top ;
    node *down;
    int N;
    
public:
    Stack(){
        top = down = NULL;
        N = 0;
    }
    bool isEmpty(){
        if(N == 0)return true;
        return false;
    }
    TYPE Top(){
        return top->data;
    }
    void Push(int val){
        if(isEmpty()){
            node *tmp = new node();
            tmp->data = val;
            tmp->next = NULL;
            tmp->prev = NULL;
            top = down =  tmp;
        }else{
            node *tmp = new node();
            tmp->data = val;
            tmp->next = top;
            top->prev = tmp;
            top = tmp;
        }
        N++;
    }
    TYPE Pop(){
        if(isEmpty()){
            cout << "Stack Empty" << endl;
            return 0;
        }
        int val = top->data;
        if(top->next != NULL)
            top = top->next;
        top -> prev = NULL;
        N--;
        return val;
    }
    void Display(){
        node *pivot = new node();
        pivot = down;
        while(pivot->prev != NULL){
            cout << pivot->data << " " ;
            pivot = pivot->prev;
        }
        cout << pivot->data << endl;
    }
    int SizeOfStack(){
        return N;
    }
    
};

void OESort(){
    int arr[1<<10] ;
    int n; cin >> n;
    for (int i=0; i<n; i++) {
        cin >> arr[i];
    }
    Stack<int> odd,even;
    for(int i=0;i<n;i++){
        if(arr[i]&1)odd.Push(arr[i]);
        else even.Push(arr[i]);
        cout << arr[i] << " ";
    }
    cout << endl;
    //odd.Display();
    //cout << odd.SizeOfStack() << endl;
    //even.Display();
    //cout << even.SizeOfStack() << endl;
    
    while(even.SizeOfStack() != 0){
        cout << even.Pop() << " " ;
    }
    while(odd.SizeOfStack() != 0){
        cout << odd.Pop() << " " ;
    }
    cout << endl;

}

void InfixToPostfix(){
    char arr[1<<10];
    cin >> arr ;
    Stack<char> s;
    for (int i=0; i<strlen(arr); i++) {
        if(arr[i] == '+' || arr[i] == '-' || arr[i] == '*' || arr[i] == '/'){
            if(s.isEmpty()){
                s.Push(arr[i]);
            }
            else if((arr[i] == '*' or arr[i] == '/') and (s.Top() == '+' or s.Top() == '-')){
                s.Push(arr[i]);
            }
            else if((arr[i] == '*' or arr[i] == '/') and (s.Top() == '/' or s.Top() == '*')){
                while(true){
                    if(s.Top() == '+' or s.Top() == '-')break;
                    if(s.isEmpty())break;
                    cout << s.Pop() << " ";
                }
                s.Push(arr[i]);
            }
            else if((arr[i] == '+' or arr[i] == '-') and (s.Top() == '+' or s.Top() == '-' or s.Top() == '*' or s.Top() == '/')){
                while(true){
                    //if(s.Top() == '+' or s.Top() == '-')break;
                    if(s.isEmpty())break;
                    cout << s.Pop() << " ";
                }
                s.Push(arr[i]);
            }

        }
    }
    //cout << s.SizeOfStack() << endl;
    while(!s.isEmpty()){
        cout << s.Pop() << " ";
    }
    cout  << endl;
}

int main(){
    //OESort();
    InfixToPostfix();
    /*
    Stack<int> s;
    s.Push(10);
    s.Display();
    s.Push(20);
    s.Display();
    s.Push(30);
    s.Display();
    cout << "Pop :" << s.Pop() << endl;
    s.Display();
    cout << "SizeOfStack :" << s.SizeOfStack() << endl;
    */
}
