#include<cstdio>
#include<algorithm>

int c[32]={};
int n;

int r(int money,int now){

    int i,result=0;
    for(i=now;i<n&&result==0;i++)
        if(money%c[i]==0)
            result =1;
        else if(money > c[i])
            result |=r(money-c[i],i);
    return result;
}

int main(){

    int m,k,i,j;
    scanf("%d",&n);
    for(i=0;i<n;i++)
        scanf("%d",&c[i]);
    scanf("%d",&m);
    while(m--){
        scanf("%d",&i);
        if(r(i,0))
            printf("Y\n");
        else
            printf("N\n");
    }

}