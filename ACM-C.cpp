#include <bits/stdc++.h>

using namespace std;
int Arr[201][201] = {};
int Dir[2][4] = {{1,0,-1,0},{0,1,0,-1}};
int main(){
    int R,C;
    cin >> R >> C;
    for(int i=1;i<=R;i++){
        for(int j=1;j<=C;j++){
            cin >> Arr[i][j];
        }
    }
    for(int i=1;i<=R;i++){
        for(int j=1;j<=C;j++){
            if(Arr[i][j])continue;
            int N = 0;
            int Avg = 0;
            for(int k=0;k<4;k++){
                int A = i+ Dir[0][k];
                int B = j+ Dir[1][k];
                if(A<0||A>=R||B<0||B>=C)continue;
                if(Arr[A][B]==0){
                    continue;
                }else{
                    Avg += Arr[A][B];
                    N++;
                }
            }
            if(N==0)continue;
            Avg /= N;
            Arr[i][j] = Avg;
        }
    }
    for(int i=1;i<=R;i++){
        for(int j=1;j<=C;j++){
            cout << Arr[i][j] << " " ;
        }
        cout << '\n' ;
    }
}
