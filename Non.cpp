#include<bits/stdc++.h>

using namespace std;

int main(){
    int T; cin >> T;
    while(T--){
        int N; cin >> N;
        vector<int> A(1<<21);
        int MAX = -1;
        for(int i=0; i<N; i++){
            int f, t; cin >> f >> t;
            A[f] += 1;
            A[t+1] -= 1;
            MAX = max(MAX, t+1);
        }
        int now = 0;
        bool ch = true;
        for(int i=0; i<=MAX; i++){
            now += A[i];
            if(now == N)ch = false;
        }
        if(ch) cout << "yes" << endl;
        else cout << "no" << endl;
    }
}
