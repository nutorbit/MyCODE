#include<cstdio>

using namespace std;

long long int GCD(long long int a,long long int b){
    if (b==0) {
        return a;
    }else{
        return GCD(b,a%b);
    }
    
}

long long int arr[100001]={};
long long int cal[100001]={};

int main(){
    long long int ans=0;
    long long int N;
    scanf("%lld",&N);
    for (int i=0; i<N; i++) {
        scanf("%lld",&arr[i]);
        cal[i] = arr[i];
        
    }
    for (int i=1; i<N; i++) {
        long long int x = GCD(arr[i],arr[i-1]);
        arr[i] = x;
    }
    for (int i=0; i<N; i++) {
        ans+=cal[i]/arr[N-1];
    }
    printf("%lld\n",ans);
}
