#include<bits/stdc++.h>

using namespace std;
#define MAX_N 2000
#define MAX_T 2000

vector<int> solved[MAX_N];
vector<int> sortedIds;

int points[MAX_T]={};
int score[MAX_N]={};
int n,t,p;

bool poiLess(int x, int y){
    if (score[x] > score[y])
        return true;
    else if (score[x] == score[y]){
        if (solved[x].size() > solved[y].size())
            return true;
        else if (solved[x].size() == solved[y].size())
            return x < y;
        else
            return false;
    }
    else
        return false;
}
int main(){
    
    scanf("%d %d %d", &n, &t, &p);
    for (int i = 0; i < n; i++){
        int x;
        for (int j = 0; j < t; j++){
            scanf("%d", &x);
            if (x == 1)
                solved[i].push_back(j);
            else
                points[j]++;
        }
    }
    for (int i = 0; i < n; i++){
        score[i] = 0;
        for (int j = 0; j < solved[i].size(); j++)
            score[i] += points[solved[i][j]];
        sortedIds.push_back(i);
    }
    sort(sortedIds.begin(), sortedIds.end(), poiLess);
    for (int i = 0; i < n; i++){
        if (sortedIds[i] == p-1){
            cout << score[p-1] << " " << (i + 1) << endl;
            return 0;
        }
    }
    return 0;
}
