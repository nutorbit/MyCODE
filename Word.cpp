#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cctype>
#include<vector>

using namespace std;
vector<pair<int,int> > v;
int row,col;
char arr[101][101]={};
int fid;
char ha[10001]={};
bool ch = false;
int si,sj;

void check(int x,int y,int length){
    int tmp = 1;
    for(int i=x+1;i<row;i++){// s
        if(arr[i][y] == ha[tmp] && tmp < length){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int j=y+1;j<col;j++){ // e
        if(arr[x][j] == ha[tmp] && tmp < length ){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int j=y-1;j>=0;j--){ // w
        if(arr[x][j] == ha[tmp] && tmp < length ){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int i=x-1;i>=0;i--){ // n
        if(arr[i][y] == ha[tmp] && tmp < length ){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int i=1;i<row && i<col;i++){ // es
        if(arr[x+i][y+i] == ha[tmp] && tmp < length){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int i=1;i<row && i<col;i++){ // sw
        if(arr[x+i][y-i] == ha[tmp] && tmp < length){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int i=1;i<row && i<col;i++){ //ne
        if(arr[x-i][y+i] == ha[tmp] && tmp < length){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
    for(int i=1;i<row && i<col;i++){ // nw
        if(arr[x-i][y-i] == ha[tmp] && tmp < length){
            tmp++;
        }else{
            break;
        }
    }
    if(tmp == length){
        si = x;
        sj = y;
        ch = true;
        return;
    }
    tmp = 1;
}

int main(){
    char tmp;
    scanf("%d %d",&row,&col);
    for (int i=0; i<row; i++) {
        scanf("%c",&tmp);
        for (int j=0; j<col; j++) {
            char tmp;
            scanf("%c",&tmp);
            arr[i][j] = tolower(tmp);
        }
    }
    
    scanf("%d",&fid);
    for (int i=0; i<fid; i++) {
        char space;
        scanf("%c",&space);
        scanf("%s",ha);
        for(int i=0;i<strlen(ha);i++){
            ha[i] = tolower(ha[i]);
        }
        for(int j=0;j<row;j++){
            for (int k=0; k<col; k++) {
                if(arr[j][k] == ha[0]){
                    int tmp = strlen(ha);
                    check(j,k,tmp);
                    if(ch){
                        v.push_back(make_pair(si,sj));
                        break;
                    }
                }
            }
            if(ch){
                ch = false;
                break;
            }
        }
    }
    for(int i=0;i<v.size();i++){
        printf("%d %d\n",v[i].first,v[i].second);
    }
}
