#include<cctype>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<algorithm>

using namespace std;

int row,col,con;
int box[101][101]={};
int ans[21]={};
int sr,sc,ch;
bool x[101][101]={};

void check(int i,int j){
    
    if (x[i][j] == true) {
        ch = -1;
        return;
    }
    
    x[i][j] = true;
    
    if (box[i][j] == 1) {
        if (i-1 < 0) {
            ch = 1;
            return;
        }else
            check(i-1,j);
    }
    if (box[i][j] == 2) {
        if (j+1 > col-1) {
            ch = 2;
            return;
        }else
            check(i,j+1);
    }
    if (box[i][j] == 3) {
        if (i+1 > row-1) {
            ch = 3;
            return;
        }else
            check(i+1,j);
    }
    if (box[i][j] == 4) {
        if (j-1 < 0) {
            ch = 4;
            return;
        }else
            check(i,j-1);
    }
}

int main(){

    
    scanf("%d %d %d",&col,&row,&con);
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            scanf("%d",&box[i][j]);
        }
    }
    
    for (int i=0; i<con; i++) {
        scanf("%d %d",&sc,&sr);
        sc = sc-1;
        sr = sr-1;
        check(sr,sc);
        for (int k=0; k<row; k++) {
            for (int l=0; l<col; l++) {
                x[k][l] = false;
            }
        }
        ans[i] = ch;
    }
    
    for (int i=0; i<con; i++) {
        if (ans[i] == -1) {
            printf("NO\n");
        }
        if (ans[i] == 1) {
            printf("N\n");
        }
        if (ans[i] == 2) {
            printf("E\n");
        }
        if (ans[i] == 3) {
            printf("S\n");
        }
        if (ans[i] == 4) {
            printf("W\n");
        }
    }
}
