'''ASCII'''
def main():
    '''main'''
    total = sum(ord(alphabet) for alphabet in input())
    print(total)
main()
