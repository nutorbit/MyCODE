#include<cstdio>
#include<iostream>
#include<cmath>

#define MaxNode 32

using namespace std;

bool found = false;


struct node{
  node* left;
  int data;
  node* right;

  node(int n){
    data = n;
    left = NULL;
    right = NULL;
  }
};

typedef struct node NODE;

void find(NODE *t,int x) {
      if (t->data == x) {
            found = true;
      }else{
            if(t->left!=NULL) find(t->left,x);
            if(t->right!=NULL) find(t->right,x);
            cout << t->data << " ";
      }
}

int main(){

        NODE a(25);
        NODE* t = &a;
        t->left = new NODE(67);
        t->right = new NODE(48);
        t = t->left;
        t->left = new NODE(54);
        t->right = new NODE(12);
        t = t->left;
        t->left = new NODE(66);
        t = t->left;
        t->left = new NODE(95);

        t = &a;
        t = t->right;
        t->left = new NODE(21);
        t = t->left;
        t->left = new NODE(43);

        find(&a,25);
        cout << found;
}
