#include<bits/stdc++.h>

using namespace std;

//int arr[1<<9][1<<9]={};
//int dp[1<<9][1<<9]={};

int arr[1<<9]={};
int dp[1<<9]={};
int N, K; //cin >> N >> K;

int recur(int now, int k){
    if(now < 0){
        return 0;
    }
    else if(k == K){
        return recur(now-1, 0);
    }
    else{
        return dp[now] =  max(recur(now-1, k+1)+arr[now],
        recur(now-1, k));
    }
}
int main(){
    ios_base::sync_with_stdio(0);
    cin >> N >> K;
    for(int i=0; i<N; i++){
        cin >> arr[i];
        //dp[i] = arr[i];
    }
    int m = recur(N-1, 0);
    cout << m << endl;
    for(int i=0; i<N; i++){
        cout << dp[i] << " " ;
    }
}
