#include<bits/stdc++.h>

using namespace std;

int main(){
    int work[1<<5]={}, army[1<<5]={};
    for(int i=0; i<(1<<5); i++){
        work[i+1] = army[i] + work[i] + 1;
        army[i+1] = work[i];
    }
    int inp;
    vector<int> ans1, ans2;
    while(cin >> inp and inp != -1){
        ans1.push_back(work[inp+1]);
        ans2.push_back(work[inp+1]+army[inp+1]+1);
    }
    for(int i=0; i<ans1.size(); i++){
        cout << ans1[i] << " " << ans2[i] << endl;
    }
}
