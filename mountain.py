'''mountain'''
def main(number, lst):
    '''main'''
    lst[0], lst[1], lst[2] = list('O'*(number*5)), list(' OOO '*number), list('  O  '*number)
    lst[2] = ['*' if i%15 == 0 else lst[2][i-1] for i in range(1, len(lst[2])+1)]
    print('\n'.join([''.join(lst[2]), ''.join(lst[1]), ''.join(lst[0])]))

main(int(input()), [[], [], []])
