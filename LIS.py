a = list(map(int, input().split()))
n = len(a)
ans = []

from bisect import bisect_left

for v in a:
    idx = bisect_left(ans, v)
    if idx == len(ans):
        ans.append(v)
    else:
        ans[idx] = v
print(len(ans))

