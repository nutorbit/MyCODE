#include<cstdio>
#include<stack>

using namespace std;

int main(){
    
    char in;
    char tmp;
    bool c = false;
    stack<char> st;
    
    while(scanf("%c",&in ) && in != '\n'){
        if (st.empty()) {
            st.push(in);
            c = true;
        }else{
            if (in == ')') {
                if (st.top() == '(') {
                    st.pop();
                }else{
                    st.push(in);
                }
            }
            else if(in == '('){
                st.push(in);
            }
        }
    }
    printf("%lu",st.size());
}

//())
//))()(
