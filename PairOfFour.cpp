
#include<bits/stdc++.h>

using namespace std;

char arr[1001]= {};
int N;
int DP[1001][1001]= {};
int m=0;

int recur(int i,int j){

    int A=0,B = 0;
    if(i+1 == j && arr[i] == arr[j]){
        return DP[i][j] = 1;
    }
    if(i == j){
        return DP[i][j] = 0;
    }
    if(i+1 == j){
        return DP[i][j] = 0;
    }
    if(DP[i][j] > 0){
        return DP[i][j];
    }
    if(arr[i] == arr[j]){
        A = 1+recur(i+1,j-1);
    }
    if(i+1 != j){
        for(int k=i; k<j; k++){
            B = max(B,recur(i,k)+recur(k+1,j));
        }
    }
    return DP[i][j] = max(A,B);
}

int main(){

    scanf("%d",&N);
    for(int i=0; i<N; i++){
        scanf(" %c",&arr[i]);
    }
    int maxx = recur(0,N-1);
    printf("%d",maxx);

}
