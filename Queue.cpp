#include<iostream>

using namespace std;

int Q[10];
int Head = 0;
int Tail = 0;

void Queue(int num){
    Q[Tail++] = num;
}

void Dequeue(){
    Q[Head++] = 0;
}

int main(){
    Queue(30);
    Queue(20);
    Queue(10);
    Dequeue();
    for (int i=Head; i<Tail; i++) {
        cout << Q[i] << endl;
    }

}