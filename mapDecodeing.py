def mapDecoding(message):
    dp = [0]*1000001
    txt = message
    def recur(i):
        A, B = 0, 0 if dp[i] > 0:
            return dp[i]%1000000007
        if i == len(txt):
            return 1
        if i+2 <= len(txt) and txt[i] != '0' and 1 <= int(txt[i]+txt[i+1]) <= 26:
            A = recur(i+2)%1000000007
        if txt[i] != '0':
            B = recur(i+1)%1000000007
        dp[i] = (A + B)%1000000007
        return dp[i]%1000000007
    return recur(0)%1000000007 

txt = '1221112111122221211221221212212212111221222212122221222112122212121212221212122221211112212212211211'


print(mapDecoding(txt))
