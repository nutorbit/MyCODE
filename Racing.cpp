#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

typedef long long int  ll;

ll arr[100001]={};
int n;
ll tmp[100001]={};
short bit[100001]={};
ll val = 0;

ll sum(int index){
    ll sss = 0;
    while(index > 0){
        sss += bit[index];
        index -= index & (-index);
    }
    return sss;
}

void update(int index,ll v){
    while(index <= n){
        bit[index] += v;
        index += index & (-index);
    }
}

int main(){


    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%lld",&arr[i]);
        tmp[i] = arr[i];
        bit[i] = 0;
    }
    sort(tmp,tmp+n);
    for (int i=0; i<n; i++) {
        arr[i] = lower_bound(tmp,tmp+n,arr[i])-tmp+1;
    }
    for (int i=n-1; i>=0; i--) {
        val += sum(arr[i]-1);
        update(arr[i],1);
    }
    printf("%lld\n",val);
    
}
