#include <bits/stdc++.h>

using namespace std;

int row,col;
int sti,stj,endi,endj;
bool board[1001][1001]={};
int DP[1001][1001]={};
int dir[3][9] = {{-2,-2,1,-1,2,2,1,-1},{1,-1,2,2,1,-1,-2,-2}};
queue<pair<int,int> > q;

void travel(int a,int b){
    q.push(make_pair(a,b));
    DP[a][b] = 0;
    while(!q.empty()){
        int ii = q.front().first;
        int jj = q.front().second;
        board[ii][jj] = true;
        q.pop();
        if(ii == endi and jj == endj){
            continue;
        }
        for(int i=0;i<8;i++){
            if(ii+dir[0][i] < row and ii+dir[0][i] >= 0 and jj+dir[1][i] < col and jj+dir[1][i] >= 0 and !board[ii+dir[0][i]][jj+dir[1][i]] and DP[ii+dir[0][i]][jj+dir[1][i]] > DP[ii][jj] + 1){
                DP[ii+dir[0][i]][jj+dir[1][i]] = DP[ii][jj] + 1;
                q.push(make_pair(ii+dir[0][i],jj+dir[1][i]));
            }
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin >> row >> col;
    for(int i=0;i<row;i++){
        for(int j=0;j<col;j++){
            DP[i][j] = INT_MAX;
            char tmp ; scanf(" %c",&tmp);
            if(tmp == '#')board[i][j] = true;
        }
    }
    cin >> sti >> stj >> endi >> endj;
    sti--; stj--;
    endi--; endj--;
    travel(sti,stj);
    if(DP[endi][endj] == INT_MAX)cout << "-1\n";
    else cout << DP[endi][endj] << endl;
}
