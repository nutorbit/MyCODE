#include <cstdio>
#include <algorithm>
#include <climits>

using namespace std;

typedef struct node{
    int val;
    int in;
}NODE;

int DP[1000001]={};
NODE DP2[1000001]={};
int Coin[11]={};
int ans1[11]={};
int ans2[11]={};
int K;

int main(){
    int P,M;
    scanf("%d %d ",&P,&M);
    DP[0] = 0;
    scanf("%d",&K);
    for(int i=0;i<K;i++){
        scanf("%d",&Coin[i]);
    }
    for(int i=1;i<=M;i++){
        DP[i] = INT_MAX;
        for(int j=0;j<K;j++){
            if(i-Coin[j]>=0 && DP[i]>DP[i-Coin[j]]+1){
                DP[i] = DP[i-Coin[j]]+1;
                DP2[i].val = Coin[j];
                DP2[i].in = j;
            }
        }
    }
    int Anss = INT_MAX;
    int usemin = INT_MAX,use1,use2;
    for(int i=P; i<=M; i++){
        if(DP[i]+DP[i-P]<usemin){
            usemin=DP[i]+DP[i-P];
            use1=i;
            use2=i-P;
        }
    }
    printf("%d %d\n",DP[use1],DP[use2]);
    for (int i=use1; i>0; i-=DP2[i].val) {
        //printf("x");
        ans1[DP2[i].in]++;
    }
    for (int i=0; i<K; i++) {
        printf("%d ",ans1[i]);
    }
    printf("\n");
    for (int i=use2; i>0; i-=DP2[i].val) {
        ans2[DP2[i].in]++;
    }
    for (int i=0; i<K; i++) {
        printf("%d ",ans2[i]);
    }
    printf("\n");
    
    
    
    //printf("%d",Anss);
}
