#include<bits/stdc++.h>

using namespace std;

int N, M, fx, fy, fz;
struct node{
    int tx, ty, tz, a, b, c;
};

vector<node> A;
int idx[21] = {};

double getDist(double x, double y, double z){
    return (((fx-x)*(fx-x)) + ((fy-y)*(fy-y)) + ((fz-z)*(fz-z)));   
}

double travel(int i, int aa, int bb, int cc){
    double MIN = INT_MAX;
    double total = 0;
    for(int i=0; i<M; i++){    
        double d = getDist(A[idx[i]].tx, A[idx[i]].ty, A[idx[i]].tz);
        aa += A[idx[i]].a;
        bb += A[idx[i]].b;
        cc += A[idx[i]].c;
        fx = A[idx[i]].tx;
        fy = A[idx[i]].ty;
        fz = A[idx[i]].tz;
        total += d;
        if (aa >= N and bb >= N and cc >= N){
            return total;
        }
    }
    return INT_MAX;
}
double MIN = INT_MAX;

int main(){
    ios_base::sync_with_stdio(0);
    cin >> N;
    cin >> fx >> fy >> fz;
    cin >> M;
    for(int i=0; i<M; i++){
        node u; cin >> u.tx >> u.ty >> u.tz;
        cin >> u.a >> u.b >> u.c;
        A.push_back(u);
    }
    int x = fx, y = fy, z = fz;
    for(int i=0; i<20; i++)idx[i] = i;
    do{
        fx = x, fy = y, fz = z; 
        MIN = min(travel(0, 0, 0, 0), MIN);      
    }while(next_permutation(idx, idx+M));
    cout << MIN << endl;
}
