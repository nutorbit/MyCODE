#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef struct info{
    int x,y;
}INFO;
typedef vector<INFO> vi;

vi target,drone;
INFO u;
int N,C;
double MIN=DBL_MAX,MAX=DBL_MIN;

double getDis(INFO a,INFO b){
    return sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2));
}

int main(){
    scanf("%d %d",&N,&C);
    FOR(i,0,N)scanf("%d %d",&u.x,&u.y),target.push_back(u);
    FOR(i,0,C)scanf("%d %d",&u.x,&u.y),drone.push_back(u);
    FOR(i,0,N){
        MIN = DBL_MAX;
        FOR(j,0,C){
            if(MIN > getDis(drone[j],target[i]))MIN = getDis(drone[j],target[i]);
        }
        MAX = max(MIN,MAX);
    }
    printf("%.4lf\n",MAX);
}
