#include<cstdio>
#include<stack>
#include<vector>

using namespace std;

vector<int> vec[101];
bool ch[101]={};
bool visited[101]={};
int dp[101]={};
int maxx = -1;
int maxNODE = -1;


void DFS(int i,int val){
    if(val > maxx)maxx = val;
    dp[i] = max(dp[i],val);
    //printf("%d ",i);
    int len = vec[i].size();
    for (int k=0; k<len; k++) {
        if(!ch[vec[i][k]]){
            visited[vec[i][k]] = true;
            ch[vec[i][k]] = true;
            DFS(vec[i][k],val+1);
            ch[vec[i][k]] = false;
        }
    }
}

int main(){
    maxx = -1;
    maxNODE = -1;
    int n,a,b;
    scanf("%d",&n);
    if(n == 0)return 0;
    for (int i=0; i<n; i++) {
        scanf("%d %d",&a,&b);
        vec[a].push_back(b);
        vec[b].push_back(a);
        if(a>b)if(maxNODE < a)maxNODE = a;
        if(a<b)if(maxNODE < b)maxNODE = b;
    }
    for (int i=1; i<=maxNODE; i++) {
        if(!visited[i]){
            visited[i] = true;
            ch[i] = true;
            DFS(i,1);
            ch[i] = false;
        }
    }
    for (int i=1; i<=maxNODE; i++) {
        if(maxx == dp[i]){
            ch[i] = true;
            DFS(i,1);
            ch[i] = false;
            break;
        }
    }
    printf("%d\n",maxx);
    for (int i=1; i<=maxNODE; i++) {
        visited[i] = false;
        dp[i] = 0;
        vec[i].clear();
    }
    main();
    
    
}
