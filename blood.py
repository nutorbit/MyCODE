'''blood'''
def main(number, count):
    '''main'''
    for _ in range(number):
        txt = input()
        count[txt[txt.find(',')+1:]] += 1
    print('\n'.join(str(count[txt]) for txt in ['A', 'B', 'O', 'AB']))

main(int(input()), {txt: 0 for txt in ['A', 'B', 'O', 'AB']})
