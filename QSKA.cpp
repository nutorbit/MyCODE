#include<cstdio>
#include<string.h>

using namespace std;

int arr[100001]={};
int field[100][100]={};

int main(){
    
    int h,l,n;
    scanf("%d %d %d",&h,&l,&n);
    
    for (int i=0; i<n; i++) {
        scanf("%d",&arr[i]);
    }
    
    for (int i=1; i<=h; i++) {
        for (int j=1; j<=l; j++) {
            field[i][j] = 1;
        }
    }
    for (int i=0; i<=h; i++) {
        field[i][0] += field[i-1][0];
    }
    for (int j=0; j<=l; j++) {
        field[0][j] += field[0][j-1];
    }
    for (int i=1; i<=h; i++) {
        for (int j=1; j<=l; j++) {
            field[i][j] = field[i-1][j] + field[i][j-1] + field[i][j] - field[i-1][j-1];
        }
    }
    int a=0;
    /*
    printf("BEFORE\n\n");
    for (int i=1; i<=h; i++) {
        for (int j=1; j<=l; j++) {
            printf("%d  ",field[i][j]);
        }
        printf("\n");
    }
    */
    //printf("\n%d\n",field[h-1][l-1]);
    for (int s=1; s<n; s++) {
        for (int i=arr[s]; i<=h; i++) {
            for (int j=arr[s]; j<=l; j++) {
                a = 1-field[i-arr[s]][j-arr[s]]+field[i-arr[s]][j]+field[i][j-arr[s]];
                //printf("i:%d j:%d = %d ",i,j,a);
                if(field[i][j] > a){
                    field[i][j] = a;
                }
            }
            //printf("\n");
        }
    }
    printf("%d\n",field[h-1][l-1]);
    
    printf("AFTER\n\n");
    for (int i=1; i<=h; i++) {
        for (int j=1; j<=l; j++) {
            printf("%d  ",field[i][j]);
        }
        printf("\n");
    }
    

}
