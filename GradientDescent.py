x = 10 # initial x
n_iter = 1000 # number of iteration
alpha = 0.02
J = []

def compute_cost(x):
    J = x**2 - 4*x
    return J

def compute_grad(x):
    grad = 2*x - 4
    return grad

for i in range(n_iter):
    print (x)
    x = x - alpha*compute_grad(x) # gradient descent
    J.append(compute_cost(x)) # compute cost
print ('final x = %s' % x)
print ('final cost = %s' % J[-1])
