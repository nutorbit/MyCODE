#include<cstdio>
#include<cmath>
#include<cstdlib>

int path[10000001]={};

int main(){
    int N,st;
    int a,b;
    int maxx = -99999999;
    scanf("%d %d",&N,&st);
    for (int i=0; i<N; i++) {
        scanf("%d %d",&a,&b);
        if(a <= b){
            path[a]++;
            path[b+1]--;
        }else{
            path[a]--;
            path[b+1]++;
        }
        if (maxx < b+1) {
            maxx = b+1;
        }
    }
    int sum = 0;
    int ans;
    int minn = 10000001;
    bool ch=false;
    for (int i=0; i<maxx; i++) {
        sum+=path[i];
        if (sum == N) {
            ch = true;
            ans = abs(st-i);
            if(minn > ans){
                minn = ans;
            }
        }
    }
    if (ch) {
        printf("%d",minn);
    }else{
        printf("-1");
    }
}