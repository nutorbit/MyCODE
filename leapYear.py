'''leapYear'''
import calendar
def main():
    '''main'''
    year = int(input())
    if year < 1000:
        print(year, 'is not a valid year.')
    elif calendar.isleap(year):
        print(year, 'is a leap year.')
    else:
        print(year, 'is not a leap year.')
main()
