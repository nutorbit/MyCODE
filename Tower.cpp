#include<cstdio>
#include<vector>
#include<queue>
#include<algorithm>

using namespace std;

typedef struct node{
    int power,i;
}NODE;

vector<int> vec[1000001];
queue<NODE> q;
bool visit[1000001]={};
int maxx = 1;
int power = 0;
int k,n,m,a,b;

int main(){

    scanf("%d %d %d",&k,&n,&m);
    for (int i=0; i<m; i++) {
        scanf("%d %d",&a,&b);
        vec[min(a,b)].push_back(max(a,b));
    }
    NODE u,now,tmp;
    u.i = 1;
    u.power = k;
    q.push(u);
    visit[1] = true;
    
    while(!q.empty()){
        now = q.front();
        q.pop();
        if(now.power == 0)continue;
        for (int i=0; i<vec[now.i].size(); i++) {
            if(!visit[vec[now.i][i]]){
                if(maxx < vec[now.i][i])maxx = vec[now.i][i];
                visit[vec[now.i][i]] = true;
                tmp.power = now.power-1;
                tmp.i = vec[now.i][i];
                q.push(tmp);
            }
        }
    }
    printf("%d\n",maxx);
    

}
