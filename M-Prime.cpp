#include<bits/stdc++.h>

using namespace std;
typedef long double ll;

ll arr[] = {3, 7, 31, 127, 8191, 131071, 524287, 2147483647, 2305843009213693951};
set<ll> vec(arr, arr+9);

int main(){
    
    ios::sync_with_stdio(false);
    ll Q; cin >> Q;
    while(Q){
        ll v; cin >> v;
        if(vec.find(v) != vec.end())
            printf("YES\n");
        else
            printf("NO\n");
        // cout << "sss " << m[v] << endl;
        Q--;
    }
}