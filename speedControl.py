'''speedControl'''
def main():
    '''main'''
    limit = float(input())
    speed = float(input())
    pay = 50.0
    if speed <= limit:
        print('Your speed is legal.')
    else:
        pay += (speed - limit)*5
        if speed > 90:
            pay += 200
        print('The speed is illegal, your fine is ${0:.2f}'.format(pay))

main()
