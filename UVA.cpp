#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

vector<int> arr;
int N,tmp;
int MAX = INT_MIN;
int MIN = INT_MAX;

int main(){
    while(scanf("%d",&N) != EOF){
        arr.clear();
        MAX = INT_MIN,MIN = INT_MAX;
        FOR(i,0,N){
            scanf("%d",&tmp);
            if(!arr.empty() and fabs(arr.back()-(tmp)) > MAX)MAX = fabs(arr.back()-(tmp));
            if(!arr.empty() and fabs(arr.back()-(tmp)) < MIN)MIN = fabs(arr.back()-(tmp));
            arr.push_back(tmp);
        }
        if(MAX >= arr.size() or MIN <= 0)cout << "Not jolly" << endl;
        else cout << "Jolly" << endl;
        
    }
}
