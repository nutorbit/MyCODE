#include<cstdio>
#include<climits>
#include<algorithm>

using namespace std;

typedef long long int ll;

ll GCD(ll a,ll b){
    if(b==0){
        return a;
    }else{
        return GCD(b,a%b);
    }
}

ll LCM(ll a,ll b){
    return (a/GCD(a,b))*b;
}

ll a[1001]={};
ll b[1001]={};
ll maxx = LLONG_MIN;


int main(){
    ll n;
    ll lcm = 1;
    scanf("%lld",&n);
    for (int i=0; i<n; i++) {
        scanf("%lld %lld",&a[i],&b[i]);
        lcm = LCM(a[i],lcm);
        //printf("%d\n",lcm);
    }
    for (int i=0; i<n; i++) {
        ll tmp = lcm/a[i];
        a[i]*=tmp;
        b[i]*=tmp;
    }
    for (int i=0; i<n; i++) {
        printf("%lld %lld\n",a[i],b[i]);
    }
}
