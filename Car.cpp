#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int m,n,t,tmp;

bool road[1001][1001]={};
short step[1001]={};

void travel(int i,int j){
    //forward
    if(i == t-1){
        FOR(k,0,t){
            printf("%d\n",step[k]);
        }
        return ;
    }
    if(i+1 < t && road[i+1][j]){
        step[i+1] = 3;
        travel(i+1,j);
    }
    //right
    if(i+1 < t && j+1 < m && road[i+1][j+1]){
        step[i+1] = 2;
        travel(i+1,j+1);
    }
    //left
    if(i+1 < t && j-1 >= 0 && road[i+1][j-1]){
        step[i+1] = 1;
        travel(i+1,j-1);
    }
}

int main(){

    scanf("%d %d %d",&m,&n,&t);
    FOR(i,0,t)
        FOR(j,0,m)
            scanf("%d",&tmp),tmp==0?road[i][j] = true:road[i][j] = false;
    travel(-1,n-1);

}
