#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>

using namespace std;

int minn =9999999;
int Sa[11],Ba[11];
int N;

void recur(int now,int S,int B){
      if(abs(S-B)<minn && S != 1 && B != 0){
            minn = abs(S-B);
      }
      if (now == N) {
            return ;
      }
      if(now<N){
            recur(now+1,S*Sa[now],B+Ba[now]);
            recur(now+1,S,B);
      }
}

int main(){
      cin >> N;
      for(int i=0;i<N;i++){
            cin >> Sa[i] >> Ba[i];
      }
      recur(0,1,0);
      cout << abs(minn);
}
