#include<bits/stdc++.h>

using namespace std;

int main(){
	int N, M; cin >> N >> M;
	vector<int> adj[100010]{};
	for(int i=0; i<M; i++){
		int a, b; cin >> a >> b;
		adj[a].push_back(b);
		adj[b].push_back(a);
	}
	bool st[100010]={};
	bool ans = false;
	for(int i=0; i<=N; i++){
		if(!st[i]){
			// st[i] = true;
			queue<int> q;
			q.push(i);
			while(!q.empty()){
				int f = q.front(); 
				q.pop();
				for(auto t: adj[f]){
					if(!st[t]){
						st[t] = !st[f];
						q.push(t);
					}
					else if(st[t] == st[f]){

						ans = true;
						break;
					}
				}
			}
			/*
			for(int k=0; k<= N; k++){
				cout << st[k] << " " ;
			}
			cout << endl;
			*/
		}
	}
	cout << (ans?"Gay found!": "Gay not found!") << endl;
}