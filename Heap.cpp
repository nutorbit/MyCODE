#include <iostream>
#include <algorithm>

using namespace std;

int arr[10001]={};
int heap[10001];
int max;

bool compare(const int &a,const int &b){
      return a > b;
}

void MaxHeap(int count){
      if (count == 1) {
            return;
      }
      if (heap[count] < heap[count/2]) {
            int tmp = heap[count/2];
            heap[count/2] = heap[count];
            heap[count] = tmp;
            MaxHeap(count/2);
      }
}

void MinHeap(int count){
      if (count == 1) {
            return;
      }
      if (heap[count] > heap[count/2]) {
            int tmp = heap[count/2];
            heap[count/2] = heap[count];
            heap[count] = tmp;
            MinHeap(count/2);
      }
}

int main(){
      int count;
      cin >> count;
      for (int i = 1; i <= count; i++) {
            cin >> arr[i];
      }
      //more -> less//
      make_heap(arr,arr+count,compare);
      for(int i=0;i<count;i++){
            cout << arr[i] << " ";

      }
      /*int choice;
      cin >> choice;
      if (choice == 1) {
            for (int i=1;i<=count;i++){
                  heap[i] = arr[i];
                  MaxHeap(i);
            }
            for (int i=1;i<=count;i++){
                  cout << heap[i] <<  " ";
            }
      }
      if (choice == 2) {
            for (int i=1;i<=count;i++){
                  heap[i] = arr[i];
                  MinHeap(i);
            }
            for (int i=1;i<=count;i++){
                  cout << heap[i] <<  " ";
            }
      }*/
}
