#include<cstdio>

int row,col;
int sq;
int field[1001][1001]={};
int maxx =0;

void ch(int i,int j){
    int sum=0;
    if (i+sq <= row && j+sq <= col) {
        for (int k=i; k<i+sq; k++) {
            for (int l=j; l<j+sq; l++) {
                sum += field[k][l];
            }
        }
    }
    if (maxx < sum ) {
        maxx = sum;
    }
    
}

int main(){
    
    int x=0;
    scanf("%d %d",&row,&col);
    scanf("%d",&sq);
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            scanf("%d",&field[i][j]);
        }
    }
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            ch(i,j);
        }
    }
    printf("%d\n",maxx);
}
