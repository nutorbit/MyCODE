#include<cstdio>

typedef long long int ll;

using namespace std;

ll minBiteSize = 5;
ll DP[1001][1001]={};

ll findEatingMethodCount(ll plateSize,ll startBiteSize) {
    
    ll biteSize = (startBiteSize > plateSize) ? plateSize : startBiteSize;
    if (plateSize < minBiteSize) return 0;
    if (plateSize == minBiteSize) {
        return 1;
    }
    
    ll count = 0;
    while (biteSize >= minBiteSize) {
        
        ll remaining = plateSize;
        remaining -= biteSize;
        if (remaining == 0) {
            
            count++;
            biteSize--;
            continue;
        }
        if (remaining != 0 && remaining < minBiteSize) {
            biteSize--;
            continue;
        }
        ll c =0;
        
        if(DP[remaining][biteSize] > 0)c = DP[remaining][biteSize];
        else DP[remaining][biteSize] = c = findEatingMethodCount(remaining, biteSize);
        
        count += c;
        biteSize--;
    }
    
    return count;
}

int main(){
    
    ll n;
    scanf("%lld",&n);
    
    ll prob = findEatingMethodCount(n,n);
    printf("%lld\n",prob);
    
}
