#include<cstdio>
#include<algorithm>

using namespace std;

/*
int set[] = {1,2,3,4,5,6};

int main(){
    do{
        for (int i=0; i<6; i++) {
            printf("%d ",set[i]);
        }
        printf("\n");
    }while(next_permutation(set,set+6));
}

*/

int arr[100001]={1,2,3};

void permu(int n){
    if(n == 2){
        for(int i=0;i<3;i++){
            printf("%d",arr[i]);
        }
        printf("\n");
    }else{
        for(int i=n+1;i<3;i++){
            swap(arr[i],arr[n+1]);
            permu(n+1);
            swap(arr[i],arr[n+1]);
        }
    }
}

int main(){
    permu(-1);
}