#include<bits/stdc++.h>

using namespace std;

int main(){
    int T; cin >> T;
    for(int t=1; t<=T; t++){
        int E, N; cin >> E >> N;
        vector<int> A;
        int honor = 0;
        for(int i=0; i<N; i++){
            int in; cin >> in;
            A.push_back(in);
        }
        sort(A.begin(), A.end());
        int ans = 0;
        int l=0, r=N-1;
        while(l <= r){
            if(E > A[l]){
                honor += 1;
                E -= A[l];
                l += 1;
                ans = max(ans, honor);
            }
            else if(honor > 0){
                honor -= 1;
                E += A[r];
                r -= 1;
            }
            else break;
        }
        cout << "Case #" << t << ": "<< ans << endl;
    }
}
