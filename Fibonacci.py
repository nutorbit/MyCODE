'''Fibonacci'''
from functools import reduce
def main(inp):
    '''gogoogoog'''
    print(reduce(lambda x, n: [x[1], x[0]+x[1]], range(int(inp[2])-1), [inp[0], inp[1]])[0])
main(input().split())
