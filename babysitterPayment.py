'''babysitterPayment'''
def tominute(txt):
    '''convert to minute format'''
    hour, minute = txt.split(':')
    hour = float(hour)
    minute = float(minute)
    return hour*60 + minute

def calculate(totalstart, totalend, permin8_21=2.50/60, permin=1.75/60):
    '''calculate'''
    pay = 0
    if totalstart == 480:
        if 480 <= totalend < 1260:
            pay += (totalend-480)*permin8_21
        if totalend >= 1260:
            pay += (1260-480)*permin8_21
            pay += (totalend-1260)*permin
    elif totalstart < 480:
        if 480 <= totalend < 1260:
            pay += (480-totalstart)*permin
            pay += (totalend-480)*permin8_21
        if totalend >= 1260:
            pay += (480-totalstart)*permin
            pay += (1260-480)*permin8_21
            pay += (totalend-1260)*permin
        if totalend < 480:
            pay += (totalend-totalstart)*permin
    elif 480 <= totalstart <= 1260:
        if 480 <= totalend <= 1260:
            pay += (totalend-totalstart)*permin8_21
        if totalend > 1260:
            pay += (1260-totalstart)*permin8_21
            pay += (totalend-1260)*permin
    elif totalstart >= 1260:
        pay += (totalend-totalstart)*permin
    return pay

def main():
    '''main'''
    start = input()
    end = input()
    totalstart = tominute(start)
    totalend = tominute(end)
    pay = 0
    # print(totalstart, totalend)
    if totalstart < totalend:
        pay += calculate(totalstart, totalend)
    else:
        pay += calculate(totalstart, 1440)
        pay += calculate(0, totalend)
    print('$%.2f' % pay)

main()
