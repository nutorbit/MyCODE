#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

const int N = 1000010;

int hire[ N ];

int n, m;

int main( void ) {
    
    scanf( "%d %d", &n, &m );
    
    int arr[n][m];

    for ( int i = 0 ; i < n ; ++i ) {
        for ( int j = 0 ; j < m ; ++j ) {
            scanf( "%d", &arr[i][j] );
        }
    }
    
    for ( int i = 0 ; i < n ; ++i ) {
        sort( arr[i], arr[i] + m );
    }
    
    for ( int j = 0 ; j < m ; ++j ) {
        int mx = 0;
        for ( int i = 1 ; i < n ; ++i ) {
            if ( arr[i][j] > arr[mx][j] ) {
                mx = i;
            }
        }
        ++hire[ mx ];
    }
    int ans = 0;
    for ( int i = 1 ; i < n ; ++i ) {
        if ( hire[i] > hire[ ans ] ) {
            ans = i;
        }
    }
    printf( "%d\n", ans+1 );
    return 0;
    
}
