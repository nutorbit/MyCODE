#include<cstdio>
#include<algorithm>

using namespace std;

int crow,ccol;
int arr[101][101]={};
int t[101][101]={};
int maxx=0;
int minn=0;
int tt=0;

void checkRow(int i,int j){
    tt=0;
    tt+=arr[i][j];
    //printf("%d\n",tt);
    int tmp = maxx;
    if (i+1 < crow) {
        tmp+= t[i+1][j];
    }
    if (i-1 >= 0) {
        tmp+= t[i-1][j];
    }
    for (int k=j+1; k<ccol; k++) {
        tt += arr[i][k];
        
        if (i+1 < crow) {
            tmp += t[i+1][k];
        }
        if (i-1 >= 0) {
            tmp += t[i-1][k];
            //printf("%d\n",t[i-1][k]);
        }
    }
    tmp -= tt;
    if (minn < tmp) {
        minn = tmp;
    }
}
void checkCol(int i,int j){
    tt = 0;
    tt+=arr[i][j];
    int tmp = maxx;
    if (j+1 < ccol) {
        tmp += t[i][j+1];
    }
    if (j-1 >= 0) {
        tmp += t[i][j-1];
    }
    for (int k=i+1; k<crow; k++) {
        tt += arr[k][j];
        if (j+1 < ccol) {
            tmp += t[k][j+1];
        }
        if (j-1 >= 0) {
            tmp += t[k][j-1];
        }
    }
    
    tmp -= tt;
    if (minn < tmp) {
        minn = tmp;
    }
}

int main(){
    
    scanf("%d %d",&crow,&ccol);
    for (int i=0; i<crow; i++) {
        for (int j=0; j<ccol; j++) {
            scanf("%d",&arr[i][j]);
            maxx += arr[i][j];
        }
    }
    for (int i=0; i<crow; i++) {
        for (int j=0; j<ccol; j++) {
            scanf("%d",&t[i][j]);
        }
    }
    //printf("%d\n",maxx);
    /*printf("\n");
    for (int i=0; i<crow; i++) {
        for (int j=0; j<ccol; j++) {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }*/
    for (int i=0; i<crow; i++) {
        checkRow(i,0);
    }
    for (int i=0; i<ccol; i++) {
        checkCol(0,i);
    }
    printf("%d",minn);
}