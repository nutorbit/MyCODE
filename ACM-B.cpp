#include<bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int T; cin >> T;
    while(T--){
        char arr[5]={};
        scanf(" %c %c %c %c %c",&arr[0],&arr[1],&arr[2],&arr[3],&arr[4]);
        int ans = 0;
        int tmp;
        bool ch = false;
        for(int i=0;i<5;i++){
            if(arr[i] == 'J' or arr[i] == 'K' or arr[i] == 'Q')tmp = 10;
            if(isdigit(arr[i])){
                tmp = arr[i]-48;
            }
            if(arr[i] == 'A')tmp = 1;
            if(ans + tmp >= 16 and ans + tmp <= 21){
                cout << ans + tmp << endl;
                ch = true;
                break;
            }
            if(ans < 16 and ans + tmp > 21){
                cout << "busted" << endl;
                ch = true;
                break;
            }
        }
        if(!ch)cout << ans;
    }
    
}
