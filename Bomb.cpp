#include<bits/stdc++.h>

using namespace std;

#define pii pair<int, int>

vector<pii > A, ANS;

bool comp(const pii &a, const pii &b){
    if(a.second == b.second)return a.first < b.first;
    return a.second > b.second;
}

bool cmp(const pii &a, const pii &b){
    if(a.first == b.first)return a.second > b.second;
    return a.first < b.first;
}

int main(){
    ios_base::sync_with_stdio(0);
    int N; cin >> N;
    for(int i=0; i<N; i++){
        int a, b; cin >> a >> b;    
        A.push_back(make_pair(a, b));
    }
    sort(A.begin(), A.end(), comp);
    int H=0;
    int i=1;
    ANS.push_back(make_pair(A[0].first, A[0].second));
    while(i < N){
        if (A[H].first <= A[i].first and A[H].second >= A[i].second){
            ANS.push_back(make_pair(A[i].first, A[i].second));
            H = i;
        }
        i += 1;
    }
    sort(ANS.begin(), ANS.end(), cmp);
    for(int i=0; i<ANS.size(); i++){
        printf("%d %d\n", ANS[i].first, ANS[i].second);
    }
}
