'''encoder'''
def main():
    '''main'''
    asc = [ord(txt)+32 for txt in input()]
    key = int(input())
    for val in asc:
        val += key
        while val > 122 or val < 97:
            if val > 122:
                val = 97 + (val-123)
            if val < 97:
                val = 122 - (96-val)
        print(chr(val), end='')
main()
