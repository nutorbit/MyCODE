#include<cstdio>
#include<iostream>
#include<queue>
#include<stack>

using namespace std;
int graph[6][6]={};
bool visit[6];

void DFS(int now)
{
      printf("%d ",now);
      int i;
      for(i=1;i<6;i++)
      {
            if(graph[now][i]&&!visit[i])
            {
                  visit[i]=true;
                  DFS(i);
            }
      }
}
int main(){

      int i;
      graph[1][2]=1;
      graph[2][1]=1;
      graph[1][3]=1;
      graph[3][1]=1;
      graph[1][4]=1;
      graph[4][1]=1;

      queue<int> q;
      stack<int> s;

      visit[1]=true;
      //DFS(1);
      s.push(1);
      while(s.size())
      {
            int f = s.top();
            printf(" %d ",f );
            s.pop();
            for(i=0;i<6;i++)
            {
                  if(graph[f][i]&&!visit[i])
                  {
                        visit[i]=true;
                        s.push(i);
                  }
            }
      }

}
