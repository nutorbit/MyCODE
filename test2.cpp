#include<bits/stdc++.h>

using namespace std;

vector<int> A, h(1e6+2);
/*
vector<int> tree(1<<21), A;

void build(int node, int l, int r){
    if(l == r){
        tree[node] = A[l];
    }else{
        int mid = (l+r)/2;
        build((node<<1), l, mid);
        build((node<<1)+1, mid+1, r);
        tree[node] = max(tree[(node<<1)], tree[(node<<1)+1]);
    }
}

int query(int node, int from, int to, int l, int r){
    if(l > to or r < from){
        return INT_MIN;
    }else if(l >= from and r <= to){
        return tree[node];
    }else{
        int mid = (l+r)/2;
        int p = query((node<<1), from, to, l, mid);
        int q = query((node<<1)+1, from, to, mid+1, r);
        // cout << p << " " << q << endl;
        if(p == INT_MIN) return q;
        if(q == INT_MIN) return p;
        return max(p, q);
    }
}
*/

int main(){
    int N, Q; cin >> N >> Q;
    for(int i=0; i<N; i++){
        int inp; cin >> inp;
        h[inp].push_back(i);
    }
    for(int i=0; i<1e6; i++)
        sort(h[i].begin(), h[i].end());
    // build(1, 0, A.size());
    /*
    for(int i=0; i<N*4; i++){
        cout << tree[i] << endl;
    }
    */
    for(int i=0; i<Q; i++){
        int from, to; cin >> from >> to;
        cout << query(1, from, to-1, 0, A.size()) << endl;
    }

}
