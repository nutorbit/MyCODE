#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int Q,N,H;
int X[100001]={},Y[100001]={},idx[100001]={};
deque<int> MAX,MIN;

bool comp(const int a,const int b){
    return X[a] < X[b];
}

int main(){
    scanf("%d",&Q);
    while (Q--) {
        scanf("%d %d",&N,&H);
        FOR(i,0,N){
            scanf("%d %d",&X[i],&Y[i]);
        }
        FOR(i,0,N){
            idx[i] = i;
        }
        sort(idx,idx+N,comp);
        MAX.clear(),MIN.clear();
        int left = 0,right = 0,ans = INT_MAX;
        FOR(right,0,N){
            while(!MAX.empty() && Y[MAX.back()] < Y[idx[right]]){
                MAX.pop_back();
            }
            MAX.push_back(idx[right]);
            while(!MIN.empty() && Y[MIN.back()] > Y[idx[right]]){
                MIN.pop_back();
            }
            MIN.push_back(idx[right]);
            while(left <= right && Y[MAX.front()] - Y[MIN.front()] >= H){
                ans = min(ans,X[idx[right]] - X[idx[left]]);
                if(MAX.front() == idx[left]){
                    MAX.pop_front();
                }
                if(MIN.front() == idx[left]){
                    MIN.pop_front();
                }
                left++;
            }
        }
        if(ans == INT_MAX){
            ans = -1;
        }
        printf("%d\n",ans);
    }
}
