nclude<cctype>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<string.h>


using namespace std;

bool ch[51][51]={};
int row,col;
int maxx=0;
int m = 0;

void search(int r,int c){
    
    ch[r][c]=true;
    m++;
    if (r<row-1 && ch[r+1][c]==false) {
        search(r+1,c);
    }
    if (r>0 && ch[r-1][c]==false) {
        search(r-1,c);
    }
    if (c<col-1 && ch[r][c+1]==false) {
        search(r,c+1);
    }
    if (c>0 && ch[r][c-1]==false) {
        search(r,c-1);
    }
    if (maxx < m) {
        maxx = m;
    }
    
}

int main(){
    
    char tmp;
    scanf("%d %d",&row,&col);
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            scanf(" %c",&tmp);
            if (tmp == '#') {
                ch[i][j] = true;
                if (i<row-1) {
                    ch[i+1][j] = true;
                }
                if (i>0) {
                    ch[i-1][j] = true;
                }
                if (j<col-1) {
                    ch[i][j+1] = true;
                }
                if (j>0) {
                    ch[i][j-1] = true;
                }
            }
        }
    }
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            if (ch[i][j]==false) {
                search(i,j);
                m = 0;
            }
        }
    }
    printf("%d\n",maxx);

    return 0;
}


