#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int n,in;
int store[100001]={};

int main(){
    scanf("%d ",&n);
    FOR(i,0,n){
        scanf("%d",&store[i]);
    }
    sort(store,store+n);
    int a=0;
    int MAX = INT_MIN;
    int tmp = store[0];
    FOR(i,0,n){
        if(tmp == store[i])a++;
        if(a > MAX)MAX = a;
        if(tmp != store[i]){
            a = 1;
            tmp = store[i];
        }
    }
    cout << MAX << endl;
    
}
