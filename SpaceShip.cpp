#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

typedef struct node{
    int x,y,z;
    int it1,it2,it3;
}NODE;

vector<NODE> store;
NODE u;
int n,m,stx,sty,stz;
int MIN = INT_MAX;

void recur(int i,int x,int y,int z,int it1,int it2,int it3,int amount){
    int total = ((store[i].x-x)*(store[i].x-x)) + ((store[i].y-y)*(store[i].y-y)) + ((store[i].z-z)*(store[i].z-z));
    if((it1 >= n && it2 >= n && it3 >= n)){
        if(MIN > amount)
            MIN = amount;
        return ;
    }
    if(i == m )return;
    recur(i+1,store[i].x,store[i].y,store[i].z,it1+store[i].it1,it2+store[i].it2,it3+store[i].it3,amount+total);
    recur(i+1,x,y,z,it1,it2,it3,amount);
}

int main(){
    
    scanf("%d",&n);
    scanf("%d %d %d",&stx,&sty,&stz);
    scanf("%d",&m);
    FOR(i,0,m){
        scanf("%d %d %d %d %d %d",&u.x,&u.y,&u.z,&u.it1,&u.it2,&u.it3);
        store.push_back(u);
    }
    permu();
    recur(0,stx,sty,stz,0.0,0.0,0.0,0.0);
    printf("%d\n",MIN);
}
