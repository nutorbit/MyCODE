#include<cstdio>
#include<algorithm>
#include<stack>
#include<iostream>
#include<climits>

using namespace std;

typedef struct node{
    int in;
    int val;
}NODE;

int arr[1000003]={};
int ans[1000003]={};
stack<NODE> st;
NODE u;

int main(){
    
    int n;
    scanf("%d",&n);
    for (int i=1; i<=n; i++) {
        scanf("%d",&arr[i]);
    }
    u.val = INT_MAX;
    u.in = 0;
    st.push(u);
    for (int i=1; i<=n; i++) {
        if(st.top().val > arr[i]){
            u.val = arr[i];
            u.in = i;
            ans[i] = i-st.top().in;
            st.push(u);
        }else{
            while(true){
                if(st.top().val > arr[i])break;
                st.pop();
            }
            ans[i] = i-st.top().in;
            u.val = arr[i];
            u.in = i;
            st.push(u);
        }
    }
    while(!st.empty()){
        st.pop();
    }
    u.val = INT_MAX;
    u.in = n+1;
    st.push(u);
    for (int i=n; i>=0; i--) {
        if(st.top().val > arr[i]){
            u.val = arr[i];
            u.in = i;
            ans[i] += st.top().in-i;
            st.push(u);
        }else{
            while(true){
                if(st.top().val > arr[i])break;
                st.pop();
            }
            ans[i] += st.top().in-i;
            u.val = arr[i];
            u.in = i;
            st.push(u);
        }
    }
    
    for (int i=1; i<=n; i++) {
        cout << ans[i]-1 << " ";
    }
    
    
}
