N,S = map(int,input().split())
arr = list(map(int,input().split()))
i = int(1)
j = int(0)
SUM = arr[0]
ans = float('inf')
while (i != N or SUM >= S):
    SUM += arr[i]
    while(SUM >= S):
        ans = min(ans,i-j+1)
        SUM -= arr[j]
        j += 1
    i += 1
if ans == float('inf'):
    print('0')
else :
    print(ans)
