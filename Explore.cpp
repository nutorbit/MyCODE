#include<cstdio>
#include<algorithm>

using namespace std;

long N,M,K;
int rock[500100]={};
long rockin;
long warp[1001000][2]={};
long start[500100]={};
long maxx=0;
bool succ = false;

void find(long input){
    
    long i,n;
    if(input==N){
        succ=true;
        return ;
    }
    if(!succ){
        if(input>maxx){
            maxx=input;
        }
        if(rock[input]==0){
            find(input+1);
        }
        for(i=1;i<=M;i++){
            if(input == warp[i][0])
                find(warp[i][1]);
        }
    }
}

int main(){
    
    long i,n;
    scanf("%ld%ld%ld",&N,&M,&K);
    for(i=1;i<=M;i++){
        scanf("%ld%ld",&warp[i][0],&warp[i][1]);
        start[warp[i][0]]=i;
    }
    for(i=1;i<=K;i++){
        scanf("%ld",&rockin);
        rock[rockin]=1;
    }
    find(1);
    if(succ)printf("1");
    else printf("0 %ld",maxx);
    printf("\n");
    return 0;
    
}
