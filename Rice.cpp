#include<iostream>
#include<cstdio>
#include<utility>
#include<algorithm>
#include<vector>

using namespace std;

typedef pair<double,int> x;
vector<x> vec;

bool compare(const x &a,const x &b){
    return a.first < b.first;
}

double ans[1000001]={};

int main(){
    int N;
    scanf("%d",&N);
    int a,b;
    for (int i=0; i<N; i++) {
        scanf("%d %d",&a,&b);
        double p = (double)a/(double)b;
        vec.push_back(make_pair(p,b));
    }
    sort(vec.begin(),vec.end(),compare);
    /*
     for (int i=0; i<N; i++) {
     cout << vec[i].first << " ";
     }
     */
    int cs;
    scanf("%d",&cs);
    int se;
    int index = 0;
    
    for (int i=0; i<cs; i++) {
        scanf("%d",&se);
        while(se != 0){
            if(vec[index].second == 0){
                index++;
            }
            if(vec[index].second>=se){
                vec[index].second -= se;
                ans[i]+=vec[index].first*(double)se;
                se = 0;
            }else if(vec[index].second<se){
                int tmp = se-vec[index].second;
                ans[i]+=vec[index].first*vec[index].second;
                vec[index].second = 0;
                se = tmp;
            }
        }
        printf("%.3f\n",ans[i]);
        
        
    }
    /*
     for (int i=0; i<cs; i++) {
     
     }
     */
    
    
}