#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll x[500001]={};
ll y[500001]={};
int n;
ll ans = 0;

int main(){
    scanf("%d",&n);
    FOR(i,0,n)scanf("%lld %lld",&x[i],&y[i]);
    sort(x,x+n);
    sort(y,y+n);
    FOR(i,0,n){
        ans += (x[i]+y[i])*(2*i-n+1);
        //cout << x[i] << " " << y[i] << endl;
    }
    cout << ans << endl;

}
