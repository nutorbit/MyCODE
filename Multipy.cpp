#include<iostream>

using namespace std;

int recur(int a,int b){
    
    if (b==1) {
        return a;
    }else{
        return a+recur(a,b-1);
    }
}

int main(){
    int a,b;
    cin >> a >> b;
    cout << recur(a,b);
}