#include<cstdio>
#include<cstdlib>
#include<algorithm>

using namespace std;

template <class TYPE>
class fw {
    TYPE *arr;
    int n,size;
    
public:
    fw(int n){
        size = n;
        arr = (TYPE*)malloc(sizeof(TYPE)*n);
    }
    ~fw(){
        free(arr);
    }
    
    int bit(int index){
        return index & (-index);
    }
    
    int sum(int b){
        int re = 0;
        while(b > 0){
            re = re + arr[b];
            b -= bit(b);
        }
        return re;
    }
    
    int sum(int a, int b){
        
        return sum(b) - sum(a-1);
        
    }
    
    void update(int i, int k){
        
        while(i<=size){
            arr[i] += k;
            i += bit(i);
        }
        
    }
    
    void pr(){
        for (int i=0; i<=20; i++) {
            printf("%d ",arr[i]);
        }
        printf("\n");
    }
    
};

fw<short> RRR(1000001);
fw<short> GGG(1000001);
fw<short> BBB(1000001);
short oh[1000001]={};
int n,m;

int main(){
    scanf("%d %d",&n,&m);
    for (int i=1; i<=n; i++) {
        if(i%3 == 1){
            RRR.update(i,1);
            oh[i] = 1;
        }
        else if(i%3 == 2){
            GGG.update(i,1);
            oh[i] = 2;
        }
        else{
            BBB.update(i,1);
            oh[i] = 3;
        }
    }
    int choice;
    int a,b;
    int R,G,B;
    char c;
    for (int i=0; i<m; i++) {
        scanf("%d",&choice);
        if(choice == 1){
            scanf("%d %c",&a,&c);
            if(c == 'R'){
                if(oh[a] == 1){
                    continue;
                }
                RRR.update(a,1);
                if(oh[a] == 2){
                    oh[a] = 1;
                    GGG.update(a,-1);
                }else if(oh[a] == 3){
                    oh[a] = 1;
                    BBB.update(a,-1);
                }
            }else if(c == 'G'){
                if(oh[a] == 2){
                    continue;
                }
                GGG.update(a,1);
                if(oh[a] == 1){
                    oh[a] = 2;
                    RRR.update(a,-1);
                }
                if(oh[a] == 3){
                    oh[a] = 2;
                    BBB.update(a,-1);
                }
            }else{
                if(oh[a] == 3){
                    continue;
                }
                BBB.update(a,1);
                if(oh[a] == 1){
                    oh[a] = 3;
                    RRR.update(a,-1);
                }
                else if(oh[a] == 2){
                    oh[a] = 3;
                    GGG.update(a,-1);
                }
            }

        }else{
            scanf("%d %d",&a,&b);
            R = RRR.sum(a,b);
            G = GGG.sum(a,b);
            B = BBB.sum(a,b);
            //printf("R:%d G:%d B:%d\n",R,G,B);
            
            if(R>G && R > B)printf("R\n");
            else if(G>R && G>B)printf("G\n");
            else if(B>R && B>G)printf("B\n");
            else printf("None\n");
        }
    }

}
