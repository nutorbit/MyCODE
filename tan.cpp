#include<cstdio>
#include<iostream>
#include<vector>
#include<algorithm>
#include<climits>

using namespace std;

vector<int> vec;

int main(){
    
	while(true){
		int n,tmp;
		scanf("%d",&n);
		if(n==0)return 0;
		for(int i=0;i<n;i++){
			scanf("%d",&tmp);
			vec.push_back(tmp);
		}
		int sum = 0;
		int maxx = INT_MIN; 
		for(int i=0;i<vec.size();i++){
			sum = max(0,sum+vec[i]);
			maxx = max(maxx,sum);
		}
		printf("%d\n",maxx);
		vec.clear();
	}
}