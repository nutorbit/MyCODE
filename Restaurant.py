'''Restaurant'''
from math import ceil
def main():
    '''main'''
    menu = {1: [69, 0, 'Fish and Chips'], 2: [79, 0, 'Hamburger'], \
             3: [85, 0, 'Spaghetti Carbonara'],\

            4: [70, 0, 'Spaghetti Meatball'], 5: [80, 0, 'Lasagna'],\
             6: [40, 0, 'Garlic Bread'], \
            7: [15, 0, 'Water'], 8: [3, 0, 'Ice']}
    val = int(input())
    total = 0
    while val != 0:
        if 1 <= val <= 8:
            total += menu[val][0]
            menu[val][1] += 1
        val = int(input())
    for i in range(1, 9):
        if menu[i][1] != 0:
            print(menu[i][2] + ' ({})  '.format(menu[i][1]) + str(menu[i][0]*menu[i][1]))
    print('Subtotal', total)
    print('Total (VAT 7%)', ceil(total*1.07))
main()
