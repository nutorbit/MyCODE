#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
#define MOD 909091
typedef long long int ll;
ll memo[10001]={};


//http://techieme.in/count-binary-search-trees/3/
//https://th.wikipedia.org/wiki/จำนวนแคทาแลน
int totalTree(int n){
    if (n == 1 || n == 0)
        return 1;
    else {
        ll left = 0;
        ll right = 0;
        ll sum = 0;
        for (int k = 1; k <= n; k++) {
            if(memo[k-1] == 0){
                memo[k-1] = totalTree(k - 1);
            }
            left = memo[k-1];
            if(memo[n-k] == 0){
                memo[n-k] = totalTree(n - k);
            }
            right = memo[n-k];
            sum = sum + (left * right);
            sum %= MOD;
        }
        return sum;
    }
}


int main(){
    int n;
    scanf("%d",&n);
    printf("%d\n",totalTree(n));
}
