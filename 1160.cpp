#include<cstdio>
#include<queue>

using namespace std;

typedef struct info{
    int i,j,st;
}INFO;

int row,col,sti,stj,endi,endj;
int map[1001][1001]={};
queue<INFO> q;
bool chBranch[1001][1001]={};
int dist[1001][1001];

void branch(int i,int j){
    INFO u,f;
    u.i = i;
    u.j = j;
    u.st = 1;
    q.push(u);
    chBranch[u.i][u.j] = true;
    while(!q.empty()){
        f = q.front();
        q.pop();
        
        if(map[f.i+1][f.j] == 0 && f.i+1 < row)dist[f.i+1][f.j] = min(dist[f.i+1][f.j],dist[f.i][f.j]+1);
        if(map[f.i-1][f.j] == 0 && f.i-1 >= 0)dist[f.i-1][f.j] = min(dist[f.i-1][f.j],dist[f.i][f.j]+1);
        if(map[f.i][f.j+1] == 0 && f.j+1 < col)dist[f.i][f.j+1] = min(dist[f.i][f.j+1],dist[f.i][f.j]+1);
        if(map[f.i][f.j-1] == 0 && f.j-1 >= 0)dist[f.i][f.j-1] = min(dist[f.i][f.j-1],dist[f.i][f.j]+1);
        
        if(map[f.i+1][f.j] != 0 && f.i+1 < row && !chBranch[f.i+1][f.j]){
            u.i = f.i+1;
            u.j = f.j;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i-1][f.j] != 0 && f.i-1 >= 0 && !chBranch[f.i-1][f.j]){
            u.i = f.i-1;
            u.j = f.j;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i][f.j+1] != 0 && f.j+1 < col && !chBranch[f.i][f.j+1]){
            u.i = f.i;
            u.j = f.j+1;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i][f.j-1] != 0 && f.j+1 >= 0 && !chBranch[f.i][f.j-1]){
            u.i = f.i;
            u.j = f.j-1;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
    }
    
}

int minn = 9999999;

void run(int i,int j){
    INFO u,f;
    u.i = i;
    u.j = j;
    u.st = 1;
    q.push(u);
    chBranch[u.i][u.j] = true;
    while(!q.empty()){
        f = q.front();
        q.pop();
        
        if(map[f.i+1][f.j] == 0 && f.i+1 < row)minn = min(minn,dist[f.i+1][f.j]+f.st);
        if(map[f.i-1][f.j] == 0 && f.i-1 >= 0)minn = min(minn,dist[f.i-1][f.j]+f.st);
        if(map[f.i][f.j+1] == 0 && f.j+1 < col)minn = min(minn,dist[f.i][f.j+1]+f.st);
        if(map[f.i][f.j-1] == 0 && f.j-1 >= 0)minn = min(minn,dist[f.i][f.j-1]+f.st);
        
        if(map[f.i+1][f.j] != 0 && f.i+1 < row && !chBranch[f.i+1][f.j]){
            u.i = f.i+1;
            u.j = f.j;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i-1][f.j] != 0 && f.i-1 >= 0 && !chBranch[f.i-1][f.j]){
            u.i = f.i-1;
            u.j = f.j;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i][f.j+1] != 0 && f.j+1 < col && !chBranch[f.i][f.j+1]){
            u.i = f.i;
            u.j = f.j+1;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
        if(map[f.i][f.j-1] != 0 && f.j+1 >= 0 && !chBranch[f.i][f.j-1]){
            u.i = f.i;
            u.j = f.j-1;
            u.st = f.st+1;
            chBranch[u.i][u.j] = true;
            dist[u.i][u.j] = u.st;
            q.push(u);
        }
    }
    
}



int main(){
    
    
    scanf("%d %d",&row,&col);
    scanf("%d %d",&sti,&stj);
    sti -= 1,stj -= 1;
    scanf("%d %d",&endi,&endj);
    endi -= 1,endj -= 1;
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            scanf("%d",&map[i][j]);
            dist[i][j]=9999999;
        }
    }
    branch(endi,endj);
    if(dist[sti][stj] != 9999999)printf("0\n%d",);
    run(sti,stj);
    /*
    printf("\n");
    printf("%d\n",minn);
    for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
            if(dist[i][j] == 9999999)printf("0 ");
            else printf("%d ",dist[i][j]);
        }
        printf("\n");
    }
    */
    
    
    
}
