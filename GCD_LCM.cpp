#include<cstdio>

int GCD(int a,int b){

      if(b!=0){
            return GCD(b,a%b);
      }else{
            return a;
      }
}

int main(){
      int x,y;
      scanf("%d %d",&x,&y);
      printf("%d",GCD(x,y));
}
