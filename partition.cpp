#include<cstdio>
#include<algorithm>

using namespace std;

int arr[1001]={};
int p[1001]={};
int m[1001][1001]={};
int maxx;
int d[1001][1001]={};

int main(){
    int n,k=3;
    scanf("%d",&n);
    for (int i=1; i<=n; i++) {
        scanf("%d",&arr[i]);
    }
    p[0] = 0;
    for (int i=1; i<=n; i++)
        p[i] = p[i-1]+arr[i];
    for (int i=1; i<=n; i++)
        m[i][1] = p[i];
    for (int j=1; j<=k; j++)
        m[1][j] = arr[1];
    for (int i=2; i<=n; i++) {
        for (int j=2; j<=k; j++) {
            m[i][j] = 99999999;
            for (int o=1; o<=i-1; o++) {
                maxx = max(m[o][j-1],p[i]-p[o]);
                if(m[i][j] > maxx){
                    m[i][j] = maxx;
                    d[i][j] = o;
                }
            }
        }
    }
    
    for (int i=1; i<=n; i++) {
        for (int j=1; j<=k; j++) {
            printf("%d ",d[i][j]);
        }
        printf("\n");
    }
    
    printf("==%d %d==\n",d[n][k],d[n][k-1]);

}
