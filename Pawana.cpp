#include<cstdio>

using namespace std;

bool field[1001][100]={};
int N,M;

void travel(int i,int j){
	field[i][j] = false;
	if(field[i][j+1] == true && j+1 < M){
		travel(i,j+1);
	}
	if(field[i+1][j] == true && i+1 < N){
		travel(i+1,j);
	}
	if(field[i+1][j+1] == true && i+1 < N && j+1 < M){
		travel(i+1,j+1);
	}
    if(field[i+1][j-1] == true && i+1 < N && j-1 >= 0){
        travel(i+1,j-1);
    }
    if(field[i-1][j+1] == true && i-1 >= 0 && j+1 < M){
        travel(i-1,j+1);
    }
    if(field[i-1][j-1] == true && i-1 >= 0 && j-1 >= 0){
        travel(i-1,j-1);
    }
    if(field[i-1][j] == true && i-1 >= 0){
        travel(i-1,j);
    }
    if(field[i][j-1] == true && j-1 >= 0){
        travel(i,j-1);
    }

}


int main(){
	
	scanf("%d %d",&N,&M);
	int tmp;
	for(int i=0;i<N;i++){
		for(int j=0;j<M;j++){
			scanf("%1d",&tmp);
			if(tmp == 1) field[i][j] = true;
			if(tmp == 0) field[i][j] = false;
		}
	}
    int nub = 0;
	for(int i=0;i<N;i++){
		for(int j=0;j<M;j++){
			if(field[i][j]){
				travel(i,j);
                nub++;
			}
		}
	}
    printf("%d\n",nub);
}
