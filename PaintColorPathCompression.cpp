#include<cstdio>
#include<vector>
#include<algorithm>
#include<stack>
#include<queue>


using namespace std;

typedef struct node{
    int x1,y1;
    int x2,y2;
}NODE;

typedef struct n{
    int i,j;
}N;

typedef struct info{
    int i,val;
}INFO;

bool comp(const int &a,const int &b){
    return a < b;
}

int w,h,n,tmp1,tmp2,tmp3,tmp4;
vector<int> row;
vector<int> col;
NODE tape[2020]={};
vector<INFO> RROW;
vector<INFO> RCOL;
bool field[2020][2020]={};
int lenRow=0;
int lenCol=0;

queue<N> q;

void BFS(int i,int j){
    N u,now;
    u.i = i;
    u.j = j;
    q.push(u);
    field[i][j] = true;
    while(!q.empty()){
        now = q.front();
        q.pop();
        if(!field[now.i+1][now.j] && now.i+1 < lenCol){
            u.i = now.i+1;
            u.j = now.j;
            field[u.i][u.j] = 1;
            q.push(u);
        }
        if(!field[now.i-1][now.j] && now.i-1 >= 0){
            u.i = now.i-1;
            u.j = now.j;
            field[u.i][u.j] = 1;
            q.push(u);
        }
        if(!field[now.i][now.j+1] && now.j+1 < lenRow){
            u.i = now.i;
            u.j = now.j+1;
            field[u.i][u.j] = 1;
            q.push(u);
        }
        if(!field[now.i][now.j-1] && now.j-1 >= 1){
            u.i = now.i;
            u.j = now.j-1;
            field[u.i][u.j] = 1;
            q.push(u);
        }
    }
}


int main(){
    
    scanf("%d %d",&w,&h);
    if(w==0 && h==0)return 0;
    scanf("%d",&n);
    for (int i=0; i<n; i++) {
        scanf("%d %d %d %d",&tmp1,&tmp2,&tmp3,&tmp4);
        tape[i].x1 = tmp1,tape[i].x2 = tmp3,tape[i].y1 = tmp2,tape[i].y2 = tmp4;
        col.push_back(tmp1);
        col.push_back(tmp3);
        row.push_back(tmp2);
        row.push_back(tmp4);
    }
    sort(row.begin(),row.end(),comp);
    sort(col.begin(),col.end(),comp);
    
    lenRow = row.size();
    lenCol = col.size();
    int p = 1;
    for (int i=0; i<lenRow; i++) {
        INFO u;
        u.i = p;
        u.val = row[i];
        if(RROW.empty())RROW.push_back(u);
        else if(RROW.back().val == row[i]){
            continue;
        }
        else RROW.push_back(u);
        p++;
    }
    p = 1;
    for (int i=0; i<lenCol; i++) {
        INFO u;
        u.i = p;
        u.val = col[i];
        if(RCOL.empty())RCOL.push_back(u);
        else if(RCOL.back().val == col[i]){
            continue;
        }
        else RCOL.push_back(u);
        p++;
    }
    lenRow = RROW.size();
    lenCol = RCOL.size();
    
    for (int i=0; i<n; i++) {
        int x1 = tape[i].x1;
        int x2 = tape[i].x2;
        int l,r;
        int ttt;
        int ddd;
        int lll;
        int rrr;
        l = 0;
        r = RCOL.size();
        while(l < r){
            int mid = (l+r)/2;
            if(RCOL[mid].val == x1){
                lll=RCOL[mid].i;
                break;
            }
            else if(RCOL[mid].val > x1)r = mid;
            else l = mid+1;
        }
        l = 0;
        r = RCOL.size();
        while(l < r){
            int mid = (l+r)/2;
            if(RCOL[mid].val == x2){
                rrr=RCOL[mid].i;
                break;
            }
            else if(RCOL[mid].val > x2)r = mid;
            else l = mid+1;
        }
        int y1 = tape[i].y1;
        int y2 = tape[i].y2;
        l = 0;
        r = RROW.size();
        while(l < r){
            int mid = (l+r)/2;
            if(RROW[mid].val == y1){
                ddd=RROW[mid].i;
                break;
            }
            else if(RROW[mid].val > y1)r = mid;
            else l = mid+1;
        }
        l = 0;
        r = RROW.size();
        while(l < r){
            int mid = (l+r)/2;
            if(RROW[mid].val == y2){
                ttt=RROW[mid].i;
                break;
            }
            else if(RROW[mid].val > y2)r = mid;
            else l = mid+1;
        }
        for (int j=lll; j<rrr; j++) {
            for (int k=ddd; k<ttt; k++) {
                field[j][k] = true;
            }
        }
        /*
        printf("R:%d\nL:%d\nD:%d\nT:%d\n",rrr,lll,ddd,ttt);
        for (int j=0; j<lenCol; j++) {
            for (int k=1; k<lenRow; k++) {
                if(field[j][k])printf("1 ");
                else printf("0 ");
            }
            printf("\n");
        }
        printf("\n");
        */
        
    }
    lenRow = RROW.size();
    lenCol = RCOL.size();
    int count=0;
    for (int i=0; i<lenCol; i++) {
        for (int j=1; j<lenRow; j++) {
            if(!field[i][j])BFS(i,j),count++;
        }
    }
    for (int i=0; i<lenCol; i++) {
        for (int j=1; j<lenRow; j++) {
            field[i][j] = false;
        }
    }
    printf("%d\n",count);
    RCOL.clear();RROW.clear();row.clear();col.clear();
    main();
}
