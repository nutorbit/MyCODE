tree = [-1]*65

lst = [1,4,2,1,5,6,2]

def left(val):
    return (val<<1)+1
def right(val):
    return (val<<1)+2

def find(val,parent):
    #print(tree[parent])
    if tree[parent] == val:
        return 1
    elif tree[parent] == -1:
        return 0
    elif val < tree[parent]:
        return find(val,left(parent))
    else:
        return find(val,right(parent))

def insert(val,parent):
    if tree[parent] == -1:
        tree[parent] = val
    elif val < tree[parent]:
        insert(val,left(parent))
    else:
        insert(val,right(parent))

for val in lst:
    insert(val,0)
#print(tree)
#print(find(5,0))
if find(2,0):
    print('Found')
else :
    print('Not Found')
