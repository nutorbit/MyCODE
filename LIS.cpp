#include<cstdio>

int lis( int arr[], int n ){
    
    int ch[n],i,j,max = 0;
    
    for ( i = 0; i < n; i++ ){
        ch[i] = 1;
    }
    
    for ( i = 1; i < n; i++ ){
        for ( j = 0; j < i; j++ ){
            if ( arr[i] > arr[j] && ch[i] < ch[j] + 1){
                ch[i] = ch[j] + 1;
            }
        }
    }

    for ( i = 0; i < n; i++ ){
        printf("%d ",ch[i]);
        if ( max < ch[i] ){
            max = ch[i];
        }
    }
    printf("\n");
    return max;
}


int main(){
    
    int count;
    scanf("%d",&count);
    int arr[count];
    
    for (int i=0; i<count; i++) {
        scanf("%d",&arr[i]);
    }
    
    printf("%d\n",lis(arr,count));
    return 0;
}
