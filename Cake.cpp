#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

ll GCD(ll a,ll b){
    while((a %= b) && (b %= a));
    return a+b;
}

ll LCM(ll a,ll b){
    return (a/GCD(a,b))*b;
}

int N;
ll a,b,lcm = 1,gcd,sum=0,need = 0;
char tmp;
ll arr[22][3]={};

int main(){

	scanf("%d",&N);
	FOR(i,0,N){
        scanf("%lld%c%lld",&a,&tmp,&b);
        //gcd = GCD(a,b);
        //a /= gcd;b /= gcd;
        lcm = LCM(lcm,b);
        arr[i][0] = a;
        arr[i][1] = b;
	}
    FOR(i,0,N){
        ll x = lcm/arr[i][1];
        arr[i][0] *= x;
        sum += arr[i][0];
    }
    cout << sum  << "/" << lcm << endl;
    gcd = GCD(sum,lcm);
    sum /= gcd;lcm /= gcd;
    if(sum % lcm == 0){
        printf("%lld\n%d",sum/lcm,0);
    }else{
        need = (sum/lcm)+1;
        ll top = lcm * need;
        ll down = lcm;
        top -= sum;
        gcd = GCD(top,down);
        top /= gcd;down /= gcd;
        if(top % down == 0){
            printf("%lld\n%lld",need,top/down);
        }else{
            printf("%lld\n%lld/%lld",need,top,down);
        }
    }

}
