#include<bits/stdc++.h>

using namespace std;

#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int H,W;
int board[201][201]={};
int pMAX[201][201]={};
int pMIN[201][201]={};

void initial(){
    pMAX[H-1][0] = board[H-1][0],pMIN[H-1][0] = board[H-1][0];
    FOR(i,H-2,0)pMAX[i][0] = pMAX[i+1][0]+board[i][0],pMIN[i][0] = pMIN[i+1][0]+board[i][0];
    FOR(i,1,W)pMAX[H-1][i] = pMAX[H-1][i-1]+board[H-1][i],pMIN[H-1][i] = pMIN[H-1][i-1]+board[H-1][i];
}

void QS(){
    FOR(i,H-2,0){
        FOR(j,1,W){
            pMAX[i][j] = max(pMAX[i+1][j],pMAX[i][j-1])+board[i][j];
            pMIN[i][j] = min(pMIN[i+1][j],pMIN[i][j-1])+board[i][j];
        }
    }
    /*
    cout << "MAX" << endl;
    FOR(i,0,H){
        FOR(j,0,W){
            cout << pMAX[i][j] << " ";
        }
        cout << endl;
    }
     */
}

int main(){
    scanf("%d %d",&H,&W);
    FOR(i,0,H)
        FOR(j,0,W)
            scanf("%d",&board[i][j]);
    initial();
    QS();
    cout << pMAX[0][W-1] - pMIN[0][W-1] << endl;
    
}
