#include<stdio.h>
#include<algorithm>

using namespace std;

typedef struct node{
    int val;
    int in;
}NODE;

bool cmp(const NODE &a,const NODE &b)
{
    return a.val<b.val;
}

NODE coin[11]={};
int DP[1<<20]={};
int ans1[11]={};
int ans[11]={};
pair<int,int> DP2[1<<20]={};

int main()
{
    int p,m,n;
    scanf("%d %d %d",&p,&m,&n);
    int i,j;
    for(i=0;i<n;i++)
    {
        scanf("%d",&coin[i].val);
        coin[i].in=i;
    }

    sort(coin,coin+n,cmp);
    for(i=1;i<=(1<<20);i++)
    {
        DP[i]=i;
        DP2[i].first = 1;
        DP2[i].second=coin[0].in;
    }
    for(i=1;i<n;i++)
    {
        DP[coin[i].val]=1;
        DP2[coin[i].val].first=coin[i].val;
        DP2[coin[i].val].second=coin[i].in;
        for(j=coin[i].val+1;j<=(1<<20);j++)
        {
            if(1+DP[j-coin[i].val]<DP[j])
            {
                DP[j] = 1+DP[j-coin[i].val];
                DP2[j].first=coin[i].val;
                DP2[j].second=coin[i].in;
            }
        }
    }
    int usemin=99999999999;
    int use1,use2;
    int usein;
    for(i=p;i<=m;i++)
    {
        if(DP[i]+DP[i-p]<usemin)
        {
            usemin=DP[i]+DP[i-p];
            use1=i;
            use2=i-p;
        }
    }
//    printf("%d\n",DP[2]);
    for(i=0;i<n;i++)
    {
        ans[i]=0;
        ans1[i]=0;
    }
    printf("%d %d\n",DP[use1],DP[use2]);
//    printf("%d %d\n",use1,use2);
    for(j=use1;j>0;j-=DP2[j].first)
    {
        ans1[DP2[j].second]++;
    }
    for(i=0;i<n;i++)
        printf("%d ",ans1[i]);
    printf("\n");

    for(j=use2;j>0;j-=DP2[j].first)
    {
//        printf("==j = %d\n",j);
        ans[DP2[j].second]++;
    }
    for(i=0;i<n;i++)
        printf("%d ",ans[i]);
//    printf("%d\n",DP[10]);
//    for(j=10;j>0;j-=(DP2[j]))
//    {
//        printf("%d ",DP2[j]);
//    }
}
