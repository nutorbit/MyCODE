#include<bits/stdc++.h>

using namespace std;
typedef long double ld;

struct node{
	int i, j;
	node(int i, int j) : i(i), j(j){};
};

vector<node> p;
vector<int> idx;
ld A[5][5]={};
int s[5][5]={};

int dir[8][2]={{0, 1}, {0, -1}, {1, 0}, {-1, 0}, 
			  {1, 1}, {-1, -1}, {-1, 1}, {1, -1}};

inline void Add(ld (&tmp)[5][5], node p, int N, int M){
	for(int i=0; i<8; i++){
		int tox=p.i+dir[i][0], toy=p.j+dir[i][1];
		if(tox >= 0 and toy >= 0 and tox < N and toy < M){
			tmp[tox][toy] += tmp[p.i][p.j]*0.1;
		}
	}
}

int main(){
	int N; int M; cin >> N >> M;
	for(int i=0; i<N; i++){
		for(int j=0; j<M; j++){
			cin >> A[i][j];
			p.push_back({i, j});
			idx.push_back(idx.size());
		}
	}
	ld ans = LLONG_MAX;
	do{
		ld tmp[5][5]={};
		for(int i=0; i<N; i++)
			for(int j=0; j<M; j++)
				tmp[i][j] = A[i][j];
		ld curr = 0;
		for(int i=0; i<p.size(); i++){
			curr += tmp[p[idx[i]].i][p[idx[i]].j];
			Add(tmp, p[idx[i]], N, M);
		}
		ans = min(ans, curr);
	}while(next_permutation(idx.begin(), idx.end()));
	
	printf("%.2Lf\n", ans);

}