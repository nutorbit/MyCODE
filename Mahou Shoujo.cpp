#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

int n,k;
int a[100001]={},dp[100001]={},c[1001]={};
int MIN,tmp;

int main(){
	
	scanf("%d %d",&n,&k);
	FOR(i,1,n+1)
        scanf("%d",&a[i]);
    FOR(i,1,n+1){
		c[a[i]]=i;
		tmp = (i-k+1>0)?i-k+1:1;
        MIN = INT_MAX;
		FOR(j,1000,0){
			if(c[j] >= tmp){
				if(j+dp[tmp-1] < MIN) MIN = j+dp[tmp-1];
				tmp=c[j]+1;
			}
		}
		dp[i]=MIN;
	}
	printf("%d\n",dp[n]);
	return 0;
}
