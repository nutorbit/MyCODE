'''easterPrediction'''
def main():
    '''main'''
    year = int(input())
    exception = [1954, 1981, 2049, 2076]
    vara = year%19
    varb = year%4
    varc = year%7
    vard = (19*vara+24)%30
    vare = (2*varb+4*varc+6*vard+5)%7
    pred = 22 + vard + vare - 31
    if 1900 <= year <= 2099:
        if pred > 0:
            if year in exception:
                pred -= 7
            print('April '+str(pred)+',', year)
        else:
            print('March '+str(pred+31)+',', year)
    else:
        print('Invalid input.')

main()
