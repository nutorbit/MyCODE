//Pyramid program
//c

#include<bits/stdc++.h>

using namespace std;

int main(){
    int row, c, n, temp;
    scanf("%d",&n);
    temp = n;
    for ( row = 1 ; row <= n ; row++ ){
        for ( c = 1 ; c < temp ; c++ ){
            printf(" ");
        }
        temp--;

        for ( c = 1 ; c <= 2*row - 1 ; c++ ){
            printf("*");
        }
        printf("\n");
    }
}
//java
class print{
    public static void main(String []args){
        Scanner scan = new Scanner(System.in);
        p p = new p();
        int n = scan.nextInt();
        p.py(n);
    }
}

class p{
    void py(int n){
        int row,c,temp;
        temp = n;
        for (row = 1;row <= n;row++){
            for (c = 1;c < temp;c++){
                System.out.println(" ");
            }
            temp--;
            for (c = 1;c <= 2*row - 1;c++){
                System.out.println("*");
            }
            System.out.println("\n");
        }
    }
}