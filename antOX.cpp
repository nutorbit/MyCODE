#include<bits/stdc++.h>

using namespace std;

char board[1<<3][1<<3]={};

bool row(int x, int y, char a){
    int c = 0;
    for(int i=0; i<3; i++)
        if(board[i][y] == a) 
            c += 1;
    return c == 3;
}

bool col(int x, int y, char a){
    int c = 0;
    for(int j=0; j<3; j++)
        if(board[x][j] == a)
            c += 1;
    return c == 3;
}

bool diag(int x, int y, char a){
    int c = 0;
    for(int i=0; i<3; i++)
        if(board[i][i] == a)
            c += 1;
    int b = 0;
    for(int i=0; i<3; i++)
        if(board[i][3-i-1] == a)
            b += 1;
    return c == 3 or b == 3;
}

pair<int, int> check(int i, int j, char a){
    board[i][j] = a;
    if(row(i, j, a) or col(i, j, a) or diag(i, j, a))
        return {i, j};
    board[i][j] = '.';
    return {-1, -1};
}

int main(){
    int T; cin >> T;
    while(T--){
        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
                cin >> board[i][j];
        bool can = false;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(board[i][j] == '.'){
                    pair<int, int> po = check(i, j, 'O');
                    if(po.first != -1 and po.second != -1 and !can){
                        cout << po.first+1 << " " << po.second+1 << endl;
                        can = true;
                    }
                }
            }
        }
        if(can)continue;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(board[i][j] == '.'){
                    pair<int, int> po = check(i, j, 'X');
                    if(po.first != -1 and po.second != -1 and !can){
                        cout << po.first+1 << " " << po.second+1 << endl;
                        can = true;
                    }
                }
            }
        }
    }
}
