#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

vector<pair<int,int> > arr;
pair<int,int> u;

int main(){
    ios :: sync_with_stdio(false);
    int N; cin >> N;
    FOR(i,0,N){
        cin >> u.first >> u.second;
        arr.push_back(u);
    }
    int ans = 0;
    for(int i=0;i<N;i++){
        for(int j=i+1;j<N;j++){
            for(int k=j+1;k<N;k++){
                if((arr[j].second - arr[i].second)*(arr[k].first - arr[j].first) !=
                   (arr[k].second - arr[j].second)*(arr[j].first - arr[i].first)){
                    ans ++;
                }
            }
        }
    }
    cout << ans << endl;

}
