#include<bits/stdc++.h>

using namespace std;

int A[300005]={};

int main(){
	int N; cin >> N;
	for(int i=0; i<N; i++)
		cin >> A[i];
	int *bound = A+N;
	int ans = 0;
	for(int i=32; i--;){
		auto b = std::partition(A, bound, [=](int a){return a & (1<<i);});
		if(b-A >= 2){
			ans |= (1<<i);
			bound = b;
		}
	}
	cout << ans << endl;
}