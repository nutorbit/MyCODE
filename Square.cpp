#include<cstdio>
#include<algorithm>

using namespace std;

int arr[101][101]={};

int main(){
    int n,m;
    scanf("%d %d",&n,&m);
    for (int i=0; i<n; i++) {
        for (int j=0; j<m; j++) {
            scanf("%d",&arr[i][j]);
        }
    }
    for (int i=1; i<n; i++) {
        for (int j=1; j<m; j++) {
            if(arr[i][j]==0 || arr[i-1][j-1]==0 || arr[i-1][j]==0 || arr[i][j-1]==0){
                continue;
            }
            else if(arr[i-1][j-1] == arr[i-1][j] && arr[i-1][j-1] == arr[i][j-1]){
                arr[i][j] = arr[i-1][j-1]+1;
            }
            else
                arr[i][j] = max(arr[i-1][j-1],max(arr[i-1][j],arr[i][j-1]));
        }
    }
    printf("\n");
    for (int i=0; i<n; i++) {
        for (int j=0; j<m; j++) {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
}
