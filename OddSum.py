T = int(input())
for i in range(T):
    SUM = 0
    a = int(input())
    b = int(input())
    for j in range(a,b+1):
        if j & 1 :
            SUM += j
    if(a > b):
        print("0")
    else:
        print("Case "+str(i+1)+": "+str(SUM))
