#include<cstdio>
#include<iostream>
#include<queue>
#include<stack>
#include<algorithm>
#include<utility>

using namespace std;

struct info{
    
    int x[10001];
    
};

typedef struct info INFO;
INFO graph[10001]={};

int main(){
    
    int i,j;
    int w,e,r;
    int count;
    int power,maxx;
    scanf("%d %d %d",&power,&maxx,&count);
    bool visit[10001]={};
    for (int i=0; i<count; i++) {
        scanf("%d %d",&w,&e);
        graph[w].x[e] = 1;
        graph[e].x[w] = 1;
        
    }
    
    queue<int> q;
    
    visit[1]=true;
    q.push(1);
    int tmp = 1;
    while(!q.empty()){
        int f = q.front();
        q.pop();
        for (int i = 0; i<maxx; i++) {
            if(graph[f].x[i] != 0 && !visit[i] && power >= 0){
                if (i > tmp) {
                    tmp = i;
                }
                visit[i]=true;
                q.push(i);
                power--;
            }
        }
    }
    printf("%d",tmp);
}