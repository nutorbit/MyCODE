#include<cstdio>
#include<cctype>

void mod3_11() {
    int mod3 = 0;
    int mod11 = 0;
    int ch;
    while (isdigit(ch = fgetc(stdin))) {
        int digit = ch - '0';
        mod3 = (digit + mod3)%3;
        mod11 = (digit - mod11 + 11)%11;
    }
    printf("%d ", mod3);
    printf("%d\n", mod11);
    fflush(stdout);
}

int main() {
    mod3_11();
    return 0;
}
