'''validDate'''
def invalid(day, month, year):
    '''checkInvalid'''
    if month > 12 or month <= 0:
        return False
    listmonth = [-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if year%4 == 0:
        listmonth[2] = 29
    if 1 <= day <= listmonth[month]:
        return True
    return False

def main():
    '''main'''
    date = input()
    month, day, year = date.split('/')
    month = int(month)
    day = int(day)
    year = int(year)
    if invalid(day, month, year):
        print(date, 'is valid.')
    elif not invalid(day, month, year):
        print(date, 'is invalid.')
main()
