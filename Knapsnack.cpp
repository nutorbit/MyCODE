#include<cstdio>
#include<algorithm>
#include<iostream>


int n,w;
int v[101] = {},weight[101]={};
//branch and bound
int predic[101]={};
int nowmax = 0;

using namespace std;

int recur(int now,int noww,int nowv){

    int m = nowv;
    if(now < n && nowv+predic[now]>nowmax){
        if(noww+weight[now]<=w){
            m = max(m,recur(now+1,noww+weight[now],nowv+v[now]));
        }
        m = max(m,recur(now+1,noww,nowv));
    }
    nowmax = max(nowmax,m);
    return m;

}

int main(){

    int i,output;

    cin >> n >> w;

    for(int i=0;i<n;i++){
        cin >> v[i];//price
    }
    for(int i=0;i<n;i++){
        cin >> weight[i];//weight
    }
    predic[n-1] = v[n-1];
    for(int i=n-2;i>=0;i--){
        predic[i] = predic[i+1]+ v[i];
    }

    output = recur(0,0,0);

    cout << output;

}