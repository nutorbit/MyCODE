'''Palindrome'''
def main(number):
    '''main'''
    print(number + ' is Palindrome.' if number == number[::-1] else 'This is not Palindrome')
main(input())
