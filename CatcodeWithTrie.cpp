#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<string.h>

using namespace std;

#define NumBranch 2

int n,m,k,l;
int hashh[1<<17]={};

struct NODE{
    //char value;
    int index;
    NODE *child[NumBranch];
};

NODE *setNode(){
    NODE *temp = (NODE*)malloc(sizeof(NODE));
    //temp->value = NULL;
    for (int i=0; i<NumBranch; i++) {
        temp->child[i] = NULL;
    }
    return temp;
}

NODE *root = setNode();

void insertNode(char *data,int order){
    //int len_data = strlen(data);
    NODE *stNode = root;
    for (int i=0; i<m; i++) {
        int index = data[i]-'0';
        if(!stNode->child[index])
            stNode->child[index] = setNode();
        stNode = stNode->child[index];
        //stNode->value = data[i];
    }
    stNode->index = order;
}

bool searchNode(char *data){
    //int len_data = strlen(data);
    NODE *stNode = root;
    for (int i=0; i<m; i++) {
        int index = data[i]-'0';
        if(!stNode->child[index])
            return false;
        stNode = stNode->child[index];
        //stNode->value = data[i];
    }
    hashh[stNode->index]++;
    return true;
}

char tmp[1<<20]={};

int main(){
    char data[32],fortrie[32];
    scanf("%d%d",&n,&m);
    for (int i=0; i<n; i++) {
        scanf("%s",data);
        insertNode(data,i+1);
    }
    scanf("%d",&k);
    for (int i=0; i<k; i++) {
        scanf("%d",&l);
        scanf("%s",tmp);
        if(l<m)printf("OK\n");
        else{
            for (int j=1; j<=n; j++) {
                hashh[j] = 0;
            }
            bool ch;
            for (int j=m; j<l; j++) {
                strncpy(fortrie,tmp+(j-m),m);
                ch|=searchNode(fortrie);
            }
            if(ch){
                for (int j=1; j<=n; j++) {
                    if(hashh[j] > 0)
                        printf("%d ",j);
                    printf("\n");
                }
            }else{
                printf("OK\n");
            }
            
        }
    }
    /*
    insertNode("ant");
    insertNode("anatomy");
    insertNode("answer");
    insertNode("beta");
    insertNode("boot");
    
    if(searchNode("anaa"))printf("find anaa\n");
    if(searchNode("anatomy"))printf("find anatomy\n");
    */
}
