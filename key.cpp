#include<cstdio>
#include<iostream>

using namespace std;

#define FOR(i,j,k) for(int i=j;(j<k)?i<k:i>=k;(j<k)?i=i+1:i=i-1)
#define MAX 1001

int field[MAX+1][MAX+1]={};
int qs[MAX+1][MAX+1]={};

int main(){
    int n,m,x,y,r;
    scanf("%d %d",&n,&m);
    FOR(i,0,n){
        scanf("%d %d",&x,&y);
        field[x][y]++;
    }
    //create quick sum
    qs[0][0]=field[0][0];       // ADD
    FOR(i,1,MAX+1){
        qs[i][0] = qs[i-1][0]+field[i][0];
    }
    FOR(j,1,MAX+1){
        qs[0][j] = qs[0][j-1]+field[0][j];
    }
    FOR(i,1,MAX+1){
        FOR(j,1,MAX+1){
            qs[i][j] = field[i][j]+qs[i-1][j]+qs[i][j-1]-qs[i-1][j-1];
        }
    }
    //    FOR(i,0,15+1){
    //        FOR(j,0,15+1){
    //            cout << qs[i][j] << "  ";
    //        }
    //        cout << endl;
    //    }
//    for(int i=0;i<=10;i++)
//    {
//        for(int j=0;j<=10;j++)
//        {
//            printf("%d ",qs[i][j]);
//        }
//        printf("\n");
//    }
    FOR(i,0,m){
        scanf("%d %d %d",&x,&y,&r);
        int maxX = x+r;
        int maxY = y+r;
        int minX = x-r;
        int minY = y-r;
        if(maxX > MAX)maxX = MAX;
        if(maxY > MAX)maxY = MAX;
        if(minX < 0)minX = 0;
        if(minY < 0)minY = 0;
//        printf("Mx%d My%d mx%d my%d\n",maxX,maxY,minX,minY);
        int t1,t2,t3,t4;
        t1 = qs[maxX][maxY];
        t2 = minX-1 < 0?0:qs[minX-1][maxY];
        t3 = minY-1 < 0?0:qs[maxX][minY-1];
        t4 = minX-1 < 0 || minY-1 < 0?0:qs[minX-1][minY-1];
//        printf("%d %d %d %d====\n",t1,t2,t3,t4);
        printf("%d\n",t1-t2-t3+t4);
    }
}
