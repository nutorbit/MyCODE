#include<iostream>
#include<cstring>

using namespace std;

char infix[100],output[100],stack[100];
char tmp;
int j=0,cs=0;

char pop();
void push(char c);
int compare(int sym);

int main(){
    
    cin >> infix;
    int n = strlen(infix);
    for (int i=0; i<n; i++) {
        if (infix[i] != '(' && infix[i] != ')' && infix[i] != '+' && infix[i] != '-' && infix[i] != '*' && infix[i] != '/' ) {
            output[j++] = infix[i];
        }else{
            if (cs==0) {
                push(infix[i]);
            }else{
                if (infix[i] != '(' || infix[i] != ')') {
                    if (compare(infix[i]) <= compare(stack[cs-1])) {
                        tmp = pop();
                        output[j++] = tmp;
                        push(infix[i]);
                    }else{
                        push(infix[i]);
                    }
                }else{
                    if (infix[i] == '(') {
                        push(infix[i]);
                    }
                    if (infix[i] == ')') {
                        tmp = pop();
                        while (tmp != '(') {
                            output[j++] = tmp;
                            tmp = pop();
                        }
                    }
                }
            }
        }
    }
    while (cs != 0) {
        output[j++]=pop();
    }
    
    cout << output;
    
}
void push(char c){
    stack[cs++] = c;
}

char pop(){
    cs--;
    return stack[cs];
}


int compare(int sym){
    if (sym == '(' || sym == ')') {
        return 0;
    }
    if (sym == '+' || sym == '-') {
        return 1;
    }
    if (sym == '*' || sym == '/') {
        return 2;
    }
    return 0;
}


