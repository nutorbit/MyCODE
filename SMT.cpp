#include<bits/stdc++.h>
//zone
using namespace std;
int RMQ(int segtree[],int query_l,int query_h,int low,int high,int pos)
{
    if(query_l <= low && query_h >= high)
    {
        return segtree[pos];
    }
    if(query_l > high || query_h < low)
    {
        return INT_MAX;
    }
    int mid = (low + high)/2;
    return min(RMQ(segtree,query_l,query_h,low,mid,2*pos + 1),RMQ(segtree,query_l,query_h,mid + 1,high,2*pos + 2));
}
void buildtree(int a[],int segtree[],int pos, int low, int high)
{
    if(low == high)
    {
        segtree[pos] = a[low];
        return ;
    }
    
    int mid = (low + high)/2;
    buildtree(a,segtree,2*pos + 1,low,mid);
    buildtree(a,segtree,2*pos + 2,mid + 1,high);
    segtree[pos] = min(segtree[2*pos + 1],segtree[2*pos + 2]);
}
int main()
{
    int n;
    cout<<"Enter number of elements\n";
    cin >> n;
    int a[n + 1];
    int i;
    cout<<"Enter Input array \n";
    for(i = 0 ; i < n ; i++)
    {
        cin >> a[i];
    }
    int x = (int)(ceil(log2(n)));
    int siz = 2*(int)pow(2,x) - 1;//height of balanced binary tree 2^h+1 - 1 here h = log(k) k ->leaves here k = n :D
    int segtree[siz];
    int pos = 0;
    int low = 0;
    int high = n - 1;
    buildtree(a,segtree,pos,low,high);
    cout<<"Enter no of queries\n";
    int q;
    cin >> q;
    int l,r;
    while(q--)
    {
        cout<<"\n enter range from which you want to find minimum 0 - indexed\n";
        cin >> l >> r;
        cout<<"Minimum between " << l  << " and " <<r << " ";
        cout<<RMQ(segtree,l,r,0,n-1,0)<<endl;
    }
    return 0;
}
