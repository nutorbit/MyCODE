#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>

using namespace std;

int n,m;
int choice;
int a,b;
int R,G,B;
char c;

template <class TYPE>
class ST {
    TYPE *arr;
    int size;
    
public:
    ST(int n){
        size = n;
        arr = (TYPE*)malloc(sizeof(TYPE)*size);
    }
    ~ST(){
        free(arr);
    }
    void updateAll(int node,int s,int e,int idx,int val){
        if(idx < s || idx > e)return;
        arr[node] += val;
        if(s != e){
            int mid = (s+e)/2;
            updateAll(2*node+1,s,mid,idx,val);
            updateAll(2*node+2,mid+1,e,idx,val);
        }
    }
    
    void update(int i,int k){
        updateAll(0,0,n,i,k);
    }
    
    int sumAll(int node,int s,int e,int l,int r){
        if(r < s || e < l){
            return 0;
        }
        if(l <= s && e <= r){
            return arr[node];
        }
        
        int mid = (s+e)/2;
        int p1 = sumAll(2*node+1,s,mid,l,r);
        int p2 = sumAll(2*node+2,mid+1,e,l,r);
        return (p1+p2);
    }
    
    int sum(int l,int r){
        return sumAll(0,0,n,l-1,r-1);
    }
};

short oh[2000001]={};
ST<int> RRR(1000001);
ST<int> GGG(1000001);
ST<int> BBB(1000001);

int main(){
    scanf("%d %d",&n,&m);
    
    for (int i=0; i<n; i++) {
        if(i%3 == 0){
            RRR.update(i,1);
            oh[i+1] = 1;
        }
        else if(i%3 == 1){
            GGG.update(i,1);
            oh[i+1] = 2;
        }
        else if(i%3 == 2){
            BBB.update(i,1);
            oh[i+1] = 3;
        }
    }
    //printf("%d\n",RRR.sum(1,10));
    
    for (int i=0; i<m; i++) {
        scanf("%d",&choice);
        if(choice == 1){
            scanf("%d %c",&a,&c);
            if(c == 'R'){
                if(oh[a] == 1){
                    continue;
                }
                RRR.update(a-1,1);
                if(oh[a] == 2){
                    oh[a] = 1;
                    GGG.update(a-1,-1);
                }else if(oh[a] == 3){
                    oh[a] = 1;
                    BBB.update(a-1,-1);
                }
            }else if(c == 'G'){
                if(oh[a] == 2){
                    continue;
                }
                GGG.update(a-1,1);
                if(oh[a] == 1){
                    oh[a] = 2;
                    RRR.update(a-1,-1);
                }
                if(oh[a] == 3){
                    oh[a] = 2;
                    BBB.update(a-1,-1);
                }
            }else{
                if(oh[a] == 3){
                    continue;
                }
                BBB.update(a-1,1);
                if(oh[a] == 1){
                    oh[a-1] = 3;
                    RRR.update(a-1,-1);
                }
                else if(oh[a] == 2){
                    oh[a] = 3;
                    GGG.update(a-1,-1);
                }
            }
        }else{
            scanf("%d %d",&a,&b);
            R = RRR.sum(a,b);
            G = GGG.sum(a,b);
            B = BBB.sum(a,b);
            //printf("R:%d G:%d B:%d\n",R,G,B);
            
            if(R>G && R > B)printf("R\n");
            else if(G>R && G>B)printf("G\n");
            else if(B>R && B>G)printf("B\n");
            else printf("None\n");
        }
    }

}
