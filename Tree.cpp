#include<bits/stdc++.h>

using namespace std;

int m[200][200]={};
int dp2[200][200]={};
int dp[200][200]={};
int dir[2][2] ={{1, 0}, {0, 1}};
int ans1 = -1;
int ans2 = -1;

void run(int i, int j){
    //cout << i << j << endl;
    for(int k=0; k<2; k++){
        int tox = i+dir[k][0], toy = j+dir[k][1]; 
        if(m[tox][toy] != -1 and dp[tox][toy] < dp[i][j] + m[tox][toy]){
            dp[tox][toy] = dp[i][j] + m[tox][toy];
            dp2[tox][toy] = dp2[i][j] + m[tox][toy]*m[tox][toy];
            ans2 = max(dp[tox][toy], ans2);
            ans1 = max(dp2[tox][toy], ans1);
            run(tox, toy);
        }
    }
}

int main(){
    int H; cin >> H;
    vector<int> A;
    int len = (H*(H+1))/2;
    for(int i=0; i<len; i++){
        int inp; cin >> inp;
        A.push_back(inp);
    }
    for(int i=0; i<100; i++){
        for(int j=0; j<100; j++){
            m[i][j] = -1;
        }
    }
    int idx = 1, r=1;
    m[0][0] = A[0];
    while(idx < len){
        int tr = r, tj=0;
        while(idx < len and tr >= 0){
            m[tr][tj] = A[idx];
            tr -= 1;
            tj += 1;
            idx += 1;
        }
        r += 1;
    }
    dp2[0][0] = A[0]*A[0];
    dp[0][0] = A[0];
    run(0, 0);
    cout << ans1 << " " << ans2 << endl;
    printf("%c%c\n", ans1%26+97, ans2%26+97);
}
