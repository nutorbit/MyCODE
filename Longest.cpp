#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)

short field[1001][1001]={};
int n,r,c,tmp;
int MAX = -1;
//Left to Right
void travel1(int i,int j,int dis,int tid,bool ch){
    if(dis > MAX)MAX = dis;
    //printf("%d %d\n",i,j);
    if(!ch){
        if(i+1 < r&&field[i+1][j]==1){
            travel1(i+1,j,dis+1,3,true);
        }
        if(i-1 >= 0 && field[i-1][j] == 1){
            travel1(i-1,j,dis+1,1,true);
        }
        if(j+1 < c && field[i][j+1] == 1){
            travel1(i,j+1,dis+1,2,false);
        }
    }else{
        if(tid == 3 &&i+1 < r&&field[i+1][j]==1){
            travel1(i+1,j,dis+1,3,true);
        }
        if(tid == 1 &&i-1 >= 0 && field[i-1][j] == 1){
            travel1(i-1,j,dis+1,1,true);
        }
        if(tid == 2 &&j+1 < c && field[i][j+1] == 1){
            travel1(i,j+1,dis+1,2,false);
        }
    }
}
//Top to Bottom
void travel2(int i,int j,int dis,int tid,bool ch){
    if(dis > MAX)MAX = dis;
    //printf("%d %d\n",i,j);
    if(!ch){
        if(i+1 < r&&field[i+1][j]==1){
            travel2(i+1,j,dis+1,3,false);
        }
        if(j-1 >= 0 && field[i][j-1] == 1){
            travel2(i,j-1,dis+1,1,true);
        }
        if(j+1 < c && field[i][j+1] == 1){
            travel2(i,j+1,dis+1,2,true);
        }
    }else{
        if(tid == 3 &&i+1 < r&&field[i+1][j]==1){
            travel2(i+1,j,dis+1,3,false);
        }
        if(tid == 1 &&j-1 >= 0 && field[i][j-1] == 1){
            travel2(i,j-1,dis+1,1,true);
        }
        if(tid == 2 &&j+1 < c && field[i][j+1] == 1){
            travel2(i,j+1,dis+1,2,true);
        }
    }
}
//Right to Left
void travel3(int i,int j,int dis,int tid,bool ch){
    if(dis > MAX)MAX = dis;
    //printf("%d %d\n",i,j);
    if(!ch){
        if(i+1 < r&&field[i+1][j]==1){
            travel3(i+1,j,dis+1,3,true);
        }
        if(j-1 >= 0 && field[i][j-1] == 1){
            travel3(i,j-1,dis+1,1,false);
        }
        if(i-1 >= 0 && field[i-1][j] == 1){
            travel3(i-1,j,dis+1,2,true);
        }
    }else{
        if(tid == 3 &&i+1 < r&&field[i+1][j]==1){
            travel3(i+1,j,dis+1,3,true);
        }
        if(tid == 1 &&j-1 >= 0 && field[i][j-1] == 1){
            travel3(i,j-1,dis+1,1,false);
        }
        if(tid == 2 &&i-1 >= 0 && field[i-1][j] == 1){
            travel3(i-1,j,dis+1,2,true);
        }
    }
}
//Bottom to Top
void travel4(int i,int j,int dis,int tid,bool ch){
    if(dis > MAX)MAX = dis;
    //printf("%d %d\n",i,j);
    if(!ch){
        if(j-1 >= 0&&field[i][j-1]==1){
            travel4(i,j-1,dis+1,3,true);
        }
        if(i-1 >= 0 && field[i-1][j] == 1){
            travel4(i-1,j,dis+1,1,false);
        }
        if(j+1 < c && field[i][j+1] == 1){
            travel4(i,j+1,dis+1,2,true);
        }
    }else{
        if(tid == 3 &&j-1 >= 0&&field[i][j-1]==1){
            travel4(i,j-1,dis+1,3,true);
        }
        if(tid == 1 &&i-1 >= 0 && field[i-1][j] == 1){
            travel4(i-1,j,dis+1,1,false);
        }
        if(tid == 2 &&j+1 < c && field[i][j+1] == 1){
            travel4(i,j+1,dis+1,2,true);
        }
    }
}


int main(){
    
    scanf("%d ",&n);
    FOR(i,0,n){
        MAX = -1;
        scanf("%d %d",&r,&c);
        FOR(j,0,r){
            FOR(k,0,c){
                scanf("%1hd",&field[j][k]);
            }
        }
        /*
        cout << endl;
        FOR(j,0,r){
            FOR(k,0,c){
                printf("%1hd",field[i][j]);
            }
            cout << endl;
        }
         */
        FOR(j,0,r)
            if(field[j][0] == 1)travel1(j,0,1,2,false);
        FOR(k,0,c)
            if(field[0][k] == 1)travel2(0,k,1,3,false);
        FOR(j,0,r)
            if(field[j][c-1] == 1)travel3(j,c-1,1,1,false);
        FOR(k,0,c)
            if(field[r-1][k] == 1)travel4(r-1,k,1,1,false);
        
        printf("%d \n",MAX);
    }
}
