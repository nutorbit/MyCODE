#include <cstdio>

#define ALL 1100

int arr[100001]={};
int field[ALL][ALL]={0};

int main(){
    
    int i,n,k;
    int N,M;
    int x,y,r,sy,sx,w,h,bomb;
    
    scanf("%d %d",&N,&M);
    
    for(i=0;i<N;i++){
        scanf("%d %d",&x,&y);
        field[x][y]=1;
    }
    
    for(i=0,bomb=0;i<M;i++,bomb=0){
        scanf("%d %d %d",&x,&y,&r);
        
        sx=x-r;
        if (sx < 0) {
            sx = 0;
        }
        sy=y-r;
        if (sy < 0) {
            sy = 0;
        }
        w=x+r;
        h=y+r;
        
        for(k=sy;k<=h;k++){
            for(n=sx;n<=w;n++){
                if(field[n][k]==1){
                    bomb++;
                    field[n][k]=0;
                }
            }
        }
        
        arr[i] = bomb;
        
    }
    for (int i=0; i<M; i++) {
        printf("%d\n",arr[i]);
    }
}