#include<bits/stdc++.h>

using namespace std;
int field[1<<10][1<<10]={};
int t[1<<10][1<<10]={};

#define piii pair<int, pair<int, int> >

vector<piii>  A;

bool cmp(const piii &a, const piii &b){
    return a.second.second > b.second.second;
}
vector<int > H;
int ans = 0;

int main(){
    int N; scanf("%d ", &N);
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            scanf("%d", &field[i][j]);
        }
    }
    t[0][0] = 0;
    A.push_back(make_pair(-1, make_pair(-1, 0)));
    for(int i=1; i<N; i++){
        t[i][0] = t[i-1][0] + 1;
        A.push_back(make_pair(i, make_pair(0, t[i][0])));
        t[0][i] = t[0][i-1] + 1;
        A.push_back(make_pair(0, make_pair(i, t[0][i])));
    }
    for(int i=1; i<N; i++){
        for(int j=1; j<N; j++){
            t[i][j] = min(t[i-1][j], t[i][j-1])+1;
            A.push_back(make_pair(i, make_pair(j, t[i][j])));
        }
    }
    sort(A.begin(), A.end(), cmp);
    /*
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            cout << t[i][j] << " " ;
        }
        cout << endl;
    } 
    */
    int now = A[0].second.second;
    int s = A.size();
    for(int i=0; i<s; i++){
        if(A[i].second.second == now){
            //if(A[i].first == -1)H.push_back(0);
            /*else*/ H.push_back(field[A[i].first][A[i].second.first]);
            push_heap(H.begin(), H.end());
        }else{
            ans += H[0];
            pop_heap(H.begin(), H.end());
            H.pop_back();
           // if(A[i].first == -1)H.push_back(0);
            /*else*/ H.push_back(field[A[i].first][A[i].second.first]);
            push_heap(H.begin(), H.end());
            now = A[i].second.second;
        }
    }
    cout << ans << endl;
}
