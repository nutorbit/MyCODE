#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;

int N,K,W;
ll H[100001]={};
ll sum[100001]={};
ll MAX = LLONG_MIN;

bool check(ll limit){
    fill(sum,sum+N,0);
    ll total = 0,REQ = 0;
    FOR(i,0,N){
        sum[i] += sum[i-1];
        if(H[i] - sum[i] > limit){
            REQ = H[i] - sum[i] - limit;
            sum[i] += REQ;
            total += REQ;
            if(i+W < N)sum[i+W] -= REQ;
        }
    }
    return total <= K;
}

void BS(ll l,ll r){
    ll mid = (l+r)/2;
    if(l < r){
        if(check(mid)){
            //ans = r;
            BS(l,mid);
        }else{
            BS(mid+1,r);
        }
    }
}

int main(){
    
    scanf("%d %d %d",&N,&W,&K);
    FOR(i,0,N){
        scanf("%lld",&H[i]);
        if(MAX < H[i])MAX = H[i];
    }
    //BS(0,MAX);
    ll  l=0,r=MAX;
    while(l < r){
        ll mid = (l+r)/2;
        if(check(mid)){
            r = mid;
        }else{
            l = mid+1;
        }
    }
    printf("%lld\n",l);
}


