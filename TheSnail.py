import math

while True:
    H,U,D,F = map(int,input().split())
    if H == 0:
        break
    now = 0
    day = 1
    tmp = U*(F/100)
    while True:
        now += U
        if U > 0:
            U -= tmp
        if now > H:
            break
        now -= D
        if now < 0:
            break
        day += 1
    if now >= 0:
        print("success on day "+str(day))
    else :
        print("failure on day "+str(day))
   
