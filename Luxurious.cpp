#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

typedef struct node{
    int a,b;
    int val;
}NODE;

bool comp(const NODE &a,const NODE &b){
    return a.val < b.val;
}

int qs[100001]={};
vector<NODE> heap;
bool h[30001]={};

int main(){

    int n,k,p,tmp;
    scanf("%d %d %d",&n,&k,&p);
    for (int i=0; i<n; i++) {
        scanf("%d",&tmp);
        qs[i] = qs[i-1]+tmp;
    }
    if(k*p >= n)printf("%d\n",qs[n-1]);
    else{
        for (int i=0; i<=n-p; i++) {
            tmp = qs[i+p-1]-qs[i-1];
            NODE u;
            u.a = i+1;
            u.b = i+p;
            u.val = tmp;
            heap.push_back(u);
            push_heap(heap.begin(),heap.end(),comp);
            printf("%d-%d -> %d\n",i+1,i+p-1+1,tmp);
        }
        int count = 0,c = 0;
        for (int i=0; i<heap.size(); i++) {
            if(c == k)break;
            if(!h[heap[i].a] && !h[heap[i].b]){
                for (int j=heap[i].a; j<=heap[i].b; j++) {
                    h[j] = true;
                }
                count+= heap[i].val;
                c++;
            }
            //printf("%d-%d -> %d\n",heap[i].a,heap[i].b,heap[i].val);
        }
        printf("%d\n",count);
    }
}

