#include<bits/stdc++.h>

using namespace std;
int c=0;

vector<int> Qsort(vector<int> A){
    if(A.size() < 2) return A;
    vector<int> L, R;
    int pivot = A[(A.size()-1)/2];
    c += 1;
    for(int i=0; i<A.size(); i++){
        if(i != (A.size()-1)/2){
            if(A[i] > pivot) R.push_back(A[i]);
            if(A[i] <= pivot) L.push_back(A[i]);
        }
    }
    vector<int> tmp1, tmp2, ans;
    tmp1 = Qsort(L);
    tmp2 = Qsort(R);
    ans.insert(ans.end(), tmp1.begin(), tmp1.end());
    ans.push_back(pivot);
    ans.insert(ans.end(), tmp2.begin(), tmp2.end());
    return ans;
}

int main(){
    int T; cin >> T;
    for(int t=1; t<=T; t++){
        int N; cin >> N;
        c = 0;
        vector<int> A;
        for(int i=0; i<N; i++){
            int in; cin >> in;
            A.push_back(in);
        }
        vector<int> dis = Qsort(A);
        /*
        for(auto v: dis){
            cout << v << " ";
        }
        cout << endl << c << endl;
        */
        cout << "Case #" << t << ": ";
        if(c >= dis.size()-1){
            cout << "YES" << endl;
        }else{
            cout << "NO" << endl;
        }
    }
}
