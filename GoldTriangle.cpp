#include<bits/stdc++.h>

using namespace std;

typedef long long ll;
ll x[100100]={}, y[100100]={};
vector<pair<int, int> > A;

int main(){
    int N; cin >> N;
    for(int i=0; i<N; i++){
        ll a, b; cin >> a >> b;
        A.push_back({a, b});
        x[a] += 1;
        y[b] += 1;
    }
    ll ans = 0;
    for(auto i: A){
        ans += ((x[i.first]-1)*(y[i.second]-1));
    }
    cout << ans;
}
