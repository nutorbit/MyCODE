#include<bits/stdc++.h>

using namespace std;
#define FOR(i,j,k) for(int i=j;j<k?i<k:i>=k;j<k?i++:i--)
typedef long long int ll;
typedef struct node{
    int x,y,w;
}NODE;
struct comp{
    bool operator()(const NODE &a,const NODE &b){
        return a.w < b.w;
    }
}COMP;

int a[1<<21]={};
int n;
NODE edge[1<<21],u;
vector<NODE> store;

void initial(int n){
    FOR(i,0,n)
        a[i] = i;
}

int root(int x){
    while(a[x] != x){
        a[x] = a[a[x]];
        x = a[x];
    }
    return x;
}

void unionset(int x,int y){
    int A = root(x),B = root(y);
    a[A] = a[B];
}

int kruskal(){
    int sum = 0;
    initial(n);
    FOR(i,0,n){
        int x = edge[i].x,y = edge[i].y;
        if(root(x) != root(y)){
            sum += edge[i].w;
            u.x = edge[i].x,u.y = edge[i].y,u.w = edge[i].w;
            store.push_back(u);
            unionset(x,y);
        }
    }
    return sum;
}

int main(){
    
    scanf("%d",&n);
    FOR(i,0,n)
        scanf("%d %d %d",&edge[i].x,&edge[i].y,&edge[i].w);
    sort(edge,edge+n,COMP);
    int ans = kruskal();
    printf("MINIMUM SPANNING TREE : %d\n",ans);
    FOR(i,0,store.size())
        printf("%d -> %d : %d\n",store[i].x,store[i].y,store[i].w);
}
