//
//  Magic Square.cpp
//  Programming
//
//  Created by Nut chukamphaeng on 3/28/16.
//  Copyright © 2016 Nut chukamphaeng. All rights reserved.
//

#include<cctype>
#include<cstdio>
#include<cstring>
#include<cstdlib>


int main(){
    int arr[101][101],ch[101];
    int col[101]={},row[101]={},cheang1=0,cheang2=0;
    bool check=false;
    int size,n=0;
    scanf("%d",&size);
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            scanf("%d",&arr[i][j]);
            ch[n] = arr[i][j];
            if (ch[n] > size*size) {
                check = true;
            }
            if (ch[n] < 1) {
                check = true;
            }
            n++;
        }
    }
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            row[i] += arr[i][j];
            col[i] += arr[j][i];
        }
        cheang1 += arr[i][i];
        cheang2 += arr[i][size-i-1];
    }
    if (cheang1 != cheang2) {
        check = true;
    }
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            if (row[i] != col[j] || cheang1 != col[j] || cheang1 != row[i]) {
                check = true;
                break;
            }
        }
    }
    for (int i=0; i<n+1; i++) {
        for (int j=i+1; j<n+1; j++) {
            if (ch[i] == ch[j]) {
                check = true;
                break;
            }
        }
    }
    
    
    if (!check) {
        printf("Yes\n");
    }else{
        printf("No\n");
    }
    
    
}